#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 10:26:59 2017

@author: sobhan
"""

import dicom
from os import listdir
from os.path import isfile, join
import datetime
import math

default_dicomDir = "/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/El_Khawankey_N/PET/DICOM/PA1/ST1/SE1/"
default_dicomDirCT = "/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/El_Khawankey_N/CT/DICOM/PA1/ST1/SE1/"

# lists all files in the given dicom directory
def get_list_of_dicom_files(dicomDir=default_dicomDir):
    return [f for f in listdir(dicomDir) if isfile(join(dicomDir, f))]

# returns the dicome header of a file in the given directory as text
def get_dicom_header(i, dicomDir=default_dicomDir):
    dcm_files = get_list_of_dicom_files(dicomDir)
    dcm_file = dicom.read_file(dicomDir + str(dcm_files[i]), force=True)
    return dcm_file

def get_patient_name(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.PatientName.replace('^', ' ')
    
def get_patient_weight(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.PatientWeight

def get_injected_dose(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1074].value

def get_half_life(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1075].value

def get_scan_start_time(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    start_time = dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1072].value
    return datetime.datetime.strptime(start_time, "%H%M%S.%f")

def get_acquisition_time(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    acquisition_time = dcm_file[0x08, 0x32].value
    return datetime.datetime.strptime(acquisition_time, "%H%M%S.%f")

def get_instance_number(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x20, 0x13].value

# calculates and returns the SUV: standardized uptake value
def get_suv_bw(dicomDir=default_dicomDir):
    injected_dose = get_injected_dose(dicomDir) / 1000000
    half_life = get_half_life(dicomDir)
    delta_t = (get_acquisition_time(dicomDir) - get_scan_start_time(dicomDir)).seconds # ??? is it in seconds?
    weight = get_patient_weight(dicomDir)    
    corrected_dose = float (injected_dose * math.exp( - float(math.log(2.0) / half_life * delta_t)))
    return float(weight / corrected_dose)

def get_rescale_intercept(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RescaleIntercept

def get_rescale_slope(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RescaleSlope
#for i in range(421):
#print get_dicom_header()
#    print "i=%d" % i
#    print get_instance_number(i, default_dicomDirCT)

#print get_injected_dose(dicomDir)    
#print get_scan_start_time(dicomDir)
#print get_acquisition_time(dicomDir)
#print get_suv_bw(dicomDir)
#print get_rescale_intercept(default_dicomDirCT)
#print get_rescale_slope(default_dicomDirCT)