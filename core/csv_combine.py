#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 15:26:49 2020

@author: sobhan
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 13:08:37 2018

@author: sobhan
"""

import os
import numpy as np
import csv
#import SimpleITK 
import sys
from PyQt4 import QtGui, QtCore
from sklearn.svm import SVC as svc
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn import preprocessing
from matplotlib import pyplot as plt


WINDOWS = 0
zain = 0

class Window(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.form_widget = FormWidget(self)
        _widget = QtGui.QWidget()
        _layout = QtGui.QVBoxLayout(_widget)
        _layout.addWidget(self.form_widget)
        self.setCentralWidget(_widget)

        self.setGeometry(300, 200, 800, 500)
        self.setWindowTitle("PET-CT Hotspot Label Prediction GUI")
        
        """ controls the initiation of datasets"""
        self.first_run = True
        
        if WINDOWS:
            self.WORKING_DIR = 'D:/Sobhan/dicom/outpt/testnov/'
        else:
            self.WORKING_DIR = '/home/sobhan/gt_oct17'
            self.WORKING_DIR = '/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/oncotarget'
            self.TEST_DIR = '/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2021'
            self.OUTPUT_DIR = '/home/sobhan/data/2021/res'
        
#        openHeaderFile = QtGui.QAction("Open &header file", self)
#        openHeaderFile.setShortcut("Ctrl+H")
#        openHeaderFile.setStatusTip('Specify the path')
#        openHeaderFile.triggered.connect(self.open_header_file)
#
#        openFile = QtGui.QAction("&Open training data file", self)
#        openFile.setShortcut("Ctrl+O")
#        openFile.setStatusTip('Specify the path')
#        openFile.triggered.connect(self.open_csv_file)
        
        openTestFile = QtGui.QAction("Op&en test data file", self)
        openTestFile.setShortcut("Ctrl+E")
        openTestFile.setStatusTip('Specify the path')
        openTestFile.triggered.connect(self.open_test_csv_file)
        
        saveLastPrediction = QtGui.QAction("&Save last prediction", self)
        saveLastPrediction.setShortcut("Ctrl+S")
        saveLastPrediction.setStatusTip('Specify the output directory')
        saveLastPrediction.triggered.connect(self.save_last_study)
        
        exitApp = QtGui.QAction("&Exit", self)
        exitApp.setShortcut("Ctrl+Q")
        exitApp.setStatusTip('Leave The App')
        exitApp.triggered.connect(self.close_application)
        
        trainDataSet = QtGui.QAction("&Train data", self)
        trainDataSet.setShortcut("Ctrl+T")
        trainDataSet.setStatusTip('Train using input data')
        trainDataSet.triggered.connect(self.train_data_set)
        
        testDataSet = QtGui.QAction("&Predict labels", self)
        testDataSet.setShortcut("Ctrl+P")
        testDataSet.setStatusTip('Predict labels for input data')
        testDataSet.triggered.connect(self.predict_labels)
        
        drawFeatureImportances = QtGui.QAction("&Draw feature importance diagram", self)
        drawFeatureImportances.setShortcut("Ctrl+D")
        drawFeatureImportances.setStatusTip('Draw feature importance diagram')
        drawFeatureImportances.triggered.connect(self.draw_feature_importance_diagram)

        self.statusBar()
        
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
#        fileMenu.addAction(openHeaderFile)
#        fileMenu.addAction(openFile)
        fileMenu.addAction(openTestFile)
        fileMenu.addAction(saveLastPrediction)
        fileMenu.addAction(exitApp)
        
        learnMenu = mainMenu.addMenu('&Learn')
        learnMenu.addAction(trainDataSet)
        learnMenu.addAction(testDataSet)
#        learnMenu.addAction(drawFeatureImportances)
        self.open_header_file_mediso_pet()
        self.open_header_file_mediso_ct()
        self.open_header_file()
        self.open_csv_file()
        self.train_ds = []
        self.test_data = []
        self.test_data_pet = []
        self.test_data_ct = []
        self.hotspot_names = []
        self.patientId = 71
        
    def close_application(self):
        print "exit app"
        sys.exit()

    
    def open_header_file_mediso_pet(self):
        self.header_med_pet = []
#	self.header_file_path = str(QtGui.QFileDialog.getOpenFileName(self, "Select the csv header file", self.WORKING_DIR, "Comma separated files (*.csv *. xls *.xlsx)"))
#        self.header_file_path_med = self.WORKING_DIR + "/head_med_pet.csv"
        self.header_file_path_med = self.WORKING_DIR + "/head_med_pet.csv"
        #head_med_new
        with open(self.header_file_path_med, 'rb') as header_file:
            reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            self.header_med_pet = reader.next()
#            for column in self.header_med:
#                print column
        print len(self.header_med_pet)
        self.form_widget.refresh_layout("The header file is loaded: " + str(len(self.header_med_pet)) + " features are activated" + "\r\nNow you can load the training data file")
    
    
    def open_header_file_mediso_ct(self):
        self.header_med_ct = []
#	self.header_file_path = str(QtGui.QFileDialog.getOpenFileName(self, "Select the csv header file", self.WORKING_DIR, "Comma separated files (*.csv *. xls *.xlsx)"))
        self.header_file_path_med = self.WORKING_DIR + "/head_med_ct.csv"
        with open(self.header_file_path_med, 'rb') as header_file:
            reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            self.header_med_ct = reader.next()
#            for column in self.header_med:
#                print column
        print len(self.header_med_ct)
        self.form_widget.refresh_layout("The header file is loaded: " + str(len(self.header_med_ct)) + " features are activated" + "\r\nNow you can load the training data file")
    
    
    def open_header_file(self):
        self.header = []
#	self.header_file_path = str(QtGui.QFileDialog.getOpenFileName(self, "Select the csv header file", self.WORKING_DIR, "Comma separated files (*.csv *. xls *.xlsx)"))
        self.header_file_path = self.WORKING_DIR + "/head_pet_ct.csv"
        with open(self.header_file_path, 'rb') as header_file:
            reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            self.header = reader.next()
#            for column in self.header:
#                print column
#        print self.header
        self.form_widget.refresh_layout("The header file is loaded: " + str(len(self.header)) + " features are activated" + "\r\nNow you can load the training data file")
            
    def open_csv_file(self):
        # opens the base training file
        self.data = []
        self.labels = []
#        self.csv_file_path = str(QtGui.QFileDialog.getOpenFileName(self, "Select the input csv file", self.WORKING_DIR, "Comma separated files (*.csv *. xls *.xlsx)"))
#        self.csv_file_path = self.WORKING_DIR + "/train_pet_ct.csv"
        self.csv_file_path = self.WORKING_DIR + "/train83.csv"
        with open(self.csv_file_path, 'rb') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:                
                _tuple = [row[column] for column in self.header]
                _tuple = np.append(_tuple, row['patient_id'])
                self.data.append(_tuple)
                self.labels.append(int(row['label']))
        self.form_widget.refresh_layout("The training data file is loaded, %d hotspots data loaded for training\r\nNow you can load the test data file" % len(self.data))        
    
    def open_test_csv_file(self):
#        if not self.first_run:
#            self.reset_datasets()
        self.patientId += 1
        self.test_data = []
        self.test_data_pet = []
        self.test_data_ct = []
        self.test_labels = []

        self.hotspot_names = []
#        self.test_csv_file_path = str(QtGui.QFileDialog.getOpenFileName(self, "Select the test csv file", self.TEST_DIR, "Comma separated files (*.csv *. xls *.xlsx)"))
        for path,dirs,files in os.walk(self.TEST_DIR):
            print (dirs)
            for filename in files:
                print (os.path.join(path,filename))
#                with open(path+'/'+filename, 'rt') as file:
#                    row = json.load(file)
#                data_patient.append(row)
                print "BEFORE TEST"
#                print len(self.data)
#                print self.labels
                data = [row for row in self.data]
                labels = [label for label in self.labels]
                try:            
                    with open(path+'/'+filename, 'rb') as csvfile:
                        csvfile.next()
                        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
                        _tuple = []
                        print "BEFORE TEST0"
                        
                        pet = []
                        pett = []
                        ct = []
                    
                        for column in self.header_med_pet:
                            if column == "Max. Diameter" and not zain:
                                pet.append("Max. Diameter ( mm )")
                            else:
                                pet.append(column)
                        for column in pet:
                            if column == "TLG" and not zain:
                                pett.append("TLG ( g )")
                            else:
                                pett.append(column)            
                        
                        for column in self.header_med_ct:
                            if column == "Max. Diameter" and not zain:
                                ct.append("Max. Diameter ( mm )")
                            else:
                                ct.append(column)
                        self.header_med_pet = pett
                        self.header_med_ct = ct
                        
                        
                        for row in reader:
        #                    print row
                            if not zain:
                                if row['Image name'] == "PET( g/ml )":
                                    _tuple_pet = [row[column] for column in self.header_med_pet]
                                    print "PET"
                                    print len(_tuple_pet)
                                    self.test_data_pet.append(_tuple_pet)
                                else:
                                    _tuple_ct = [row[column] for column in self.header_med_ct]
                                    print "CT"
                                    print len(_tuple_ct)
                                    self.test_data_ct.append(_tuple_ct)
                            else:
                                if row['Image name'] == "PET( g/ml)":
                                    _tuple_pet = [row[column] for column in self.header_med_pet]
                                    print "PET"
                                    print len(_tuple_pet)
                                    self.test_data_pet.append(_tuple_pet)
                                else:
                                    _tuple_ct = [row[column] for column in self.header_med_ct]
                                    print "CT"
                                    print len(_tuple_ct)
                                    self.test_data_ct.append(_tuple_ct)
        #                print "BEFORE TEST1"
                        for row_pet in self.test_data_pet:
                            for row_ct in self.test_data_ct:
                                if row_pet[0] == row_ct[0]:
        #                            print row_pet[0]
                                    name_lbl = row_pet[0]
        #                            print "BEFORE TEST2"
                                    self.hotspot_names.append(name_lbl)
                                    _tuple = np.array(np.concatenate((row_pet[1:], row_ct[1:])))
#                                    print "Concat"
#                                    print len(_tuple)
                                    for i in range(len(_tuple)):
        #                                print i, _tuple[i]
                                        _tuple[i] = float(_tuple[i])
                #                    print "BEFORE TEST3"
                                    _tuple = np.append(_tuple, self.patientId)
                                    self.test_data.append(_tuple)
                            
                                    # update the training data set
                    #                    print "BEFORE TEST4"                    
                                    data.append(_tuple)
                    #                    print "BEFORE TEST5"
                    #                            self.labels = np.array(self.labels)
                                    if 'met' in str(name_lbl).lower(): 
                                        labels.append(int(1))
                                        self.test_labels.append(int(1))
                                    else:
                                        labels.append(int(0))    
                                        self.test_labels.append(int(0))
                            
                        self.data = [row for row in data]
                        self.labels = [label for label in labels]
                            
                        print "AFTER TEST"
                        print len(self.test_data)
                        print len(self.test_labels)
                        print len(self.data)
                        print len(self.labels)
                except Exception as e:
                    print "Something went wron while processing test file"
                    print e.message
                    
#        print self.test_data
#        print self.test_data_ct
        
#        print "ERRRRRRRRRR0"
#        print self.labels
#        self.train_data_set()
#        self.predict_labels()
#        print "ERRRRRRRRRR1"
#        print self.labels
        self.first_run = False
        self.update_train_ds()
        self.form_widget.refresh_layout("The test data file is loaded, %d hotspots data loaded for test:\r\nNow you can train data from learn menu" % len(self.test_data))
                    
    def train_data_set(self):
        self.data = np.array(self.data)
        self.labels = np.array(self.labels)
        
        self.form_widget.refresh_layout("Training started, it may take a while...")
        print "TEST0"        
        try:
            # single label svm
#            clf = svc(kernel='linear')
#            scalerObj = preprocessing.MinMaxScaler()
#            scaler = scalerObj.fit(self.data)
#            self.data = scaler.transform(self.data)
#            self.test_data = scaler.transform(self.test_data)   
            clf = ExtraTreesClassifier(n_estimators=250, max_depth=20, min_samples_leaf=1, random_state=0)
            clf.fit(self.data, self.labels)
            self.clf = clf
            self.form_widget.refresh_layout("Training finished, now you can predict test labels")
        except Exception as e:
            print "Something went wron while training"
            print e.message
    
    def predict_labels(self):
        self.test_data = np.array(self.test_data)
        print self.test_data
        try:
            # single label svm
            predictions = np.array(self.clf.predict(self.test_data))
            str_predict = "\r\n"
            for i in range(len(predictions)):
                str_predict += self.hotspot_names[i] + ":\r\t" + str(predictions[i]) + "\r\n"
            self.prediction = "Extra Trees Classifier Prediction: %s" % str_predict
            print self.prediction
            self.form_widget.refresh_layout(self.prediction)            
            self.update_train_ds()
            if self.first_run:
                self.first_run = False
        except Exception as e:
            print "Something went wron while training"
            print e.message
            

    def draw_feature_importance_diagram(self):
	print 'INSIDE draw_feature_importance_diagram', self.header
	self.form_widget.feauture_importances(self.clf.coef_, self.header)

    def save_last_study(self):
        self.output_path = str(QtGui.QFileDialog.getExistingDirectory(self, "Select prediction output directory"))
        
        # save tha overal info
#        with open(self.output_path + '/predictions.csv', 'w') as csvfile:
#    	    out_header = ['Header_file_patrh', 'Training_file_path', 'Test_file_path', 'predioctions']
#            writer = csv.DictWriter(csvfile, out_header)
#            writer.writeheader()
#            writer.writerow({'Header_file_patrh': str(self.header_file_path), 'Training_file_path': str(self.csv_file_path), 'Test_file_path': str(self.test_csv_file_path), 'predioctions': str(self.prediction) })
            
        # save the newest training data
        with open(self.output_path + '/train_out.csv', 'w') as csvfile_train:
            print "OUT0"
            train_ds = np.array(self.train_ds)
            print len(train_ds)
            writer = csv.writer(csvfile_train, delimiter=';',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            try:
                for row in train_ds:
                    writer.writerow(row)
            except Exception as e:
                print "Something went wron while creating train csv output"
                print e.message
                    
    def update_train_ds(self):
        
        train_ds = [rowt for rowt in self.train_ds]
        print "ERRRRRRRRRR2"
#        print labels
        if self.first_run:
            labels = [label for label in self.labels]
            print "DEBUG0"
            for i in range(len(self.data)):
                row = self.data[i]
                row = np.array(row)
#                print "KHARRRR0"
#                print row
                row = np.append(row, labels[i])
#                print row
                train_ds.append(row)
            print "DEBUG1"
        else:
            labels = [label for label in self.test_labels]
            print "DEBUG2"
            print len(self.test_data)
            print len(labels)
            for i in range(len(self.test_data)):
                row = self.test_data[i]
                row = np.array(row)
#                print "KHARRRR1"
#                print row
                row = np.append(row, labels[i])
#                print row
                train_ds.append(row)
            print "DEBUG3"
            
        print "ERRRRRRRRRR3"
        self.train_ds = [rowt for rowt in train_ds]
#            print self.train_ds


class FormWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(FormWidget, self).__init__(parent)

        self.home()

    def home(self):
        self.infoLabel = QtGui.QLabel("Info:")
        self.figure = plt.figure()
        self.layout = QtGui.QGridLayout(self)
        self.layout.addWidget(self.infoLabel, 2, 2)
        self.setLayout(self.layout)
	#Window.setLayout(self.layout)
        self.show()

    def refresh_layout(self, label="Info:", color="black"):
        self.infoLabel.setText(label)
        str_color = 'color: ' + color
        self.infoLabel.setStyleSheet(str_color)
        self.show()

    def feauture_importances(self, coef, names):
	names = np.array(names)
	coef = np.array(coef[0])
	imp = coef
	imp,names = zip(*sorted(zip(imp,names)))
	print 'INSIDE feauture_importances', coef, names, len(coef), len(names)
	plt.barh(range(0,len(names)), imp, align='center')
	plt.yticks(range(0,len(names)), names)
	#plt.plot(x, coef)
	#plt.xlabel()
	#plt.ylabel(
	plt.show()

    
    
def main():
    app = QtGui.QApplication(sys.argv)
    win = Window()
    win.show()
    app.exec_()

if __name__ == '__main__':
    sys.exit(main()) 
