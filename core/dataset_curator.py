#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:36:38 2020

@author: sobhan
"""

import os
import dicom
import numpy as np
import DicomHeaderUtils as dhu
import cv2
import matplotlib.pyplot as plt
import scipy
from scipy import ndimage

log_dir = "/home/moazemi/data/dataset/"

def write_log(log_line):
    """writes one single line into the log file located in log_dir/metadata.txt
        Parameters
        ----------
        log_line : string
            The log entry as a single line string.
        """
    writer = open(os.path.join(log_dir, "ds_curator_log.txt"), 'a+')
    writer.write(log_line)
    writer.write('\r\n')
    writer.close()

#ds_path = "/home/sobhan/data/dataset/"
ds_path = "/home/moazemi/data/dataset/dataset/"
#ds_path = "/home/sobhan/data/2021/"
#ds_path = "/home/sobhan/data/test_old/non_responders/"

# sampling resolution
sampling_resolution = [2.5,2.5,2.5]
sampling_resolution = [2.5,2.5,2.] #[5.46,5.46,3.27]
PET_RESOLUTION = 128
CT_RESOLUTION = 512
# pet expansion parameters
PET_HIGHEST_RESOLUTION = 700
PET_EXPAND_OFFSET = 98


def load_scan(path):
    slices = [dicom.read_file(path + '/' + s, force=True) for s in os.listdir(path)]
    slices.sort(key = lambda x: int(x.InstanceNumber))
    try:
        slice_thickness = np.abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2])
    except:
        slice_thickness = np.abs(slices[0].SliceLocation - slices[1].SliceLocation)
        
    for s in slices:
        s.SliceThickness = slice_thickness
    
    print "INSIDE load_scan"
    print "loaded %d slices" % len(slices)
    return slices
    
def get_pixels(scans):
    image = np.stack([s.pixel_array for s in scans])
    image = image.astype(np.int16)
    print "INSIDE get_pixels"
    print image.shape
    return np.array(image, dtype=np.int16)

def get_pixels_huCT(scans):
    image = np.stack([s.pixel_array for s in scans])
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0
    
    # Convert to Hounsfield units (HU)
    intercept = scans[0].RescaleIntercept
    slope = scans[0].RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    
    print "INSIDE get_pixels_huCT"
    print image.shape
    
    return np.array(image, dtype=np.int16)

def resample(image, scan, new_spacing=[1,1,1]):
    # Determine current pixel spacing
    spacing = map(float, ([scan[0].SliceThickness] + scan[0].PixelSpacing))
    spacing = np.array(list(spacing))

    resize_factor = spacing / new_spacing
    new_real_shape = image.shape * resize_factor
    new_shape = np.round(new_real_shape)
    real_resize_factor = new_shape / image.shape
    new_spacing = spacing / real_resize_factor
    
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    
    return image, new_spacing

def save_resampled_pet_slices(i, patient_id, pets):
    print "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
    print "SAVING RESAMPLED PET SLICES"
    
    img_path = os.path.join(ds_path, patient_id,"dicom","pet","images")
    thres_path = os.path.join(ds_path, patient_id,"dicom", "pet","thres")
    if not os.path.isdir(img_path):
        os.makedirs(img_path)
    if not os.path.isdir(thres_path):
        os.makedirs(thres_path)
    for z, pet_slice in enumerate(pets):
#        pet_slice /= 255.0
        
        img = np.copy(pet_slice)
        vmin = np.min(img)
        vmax = np.max(img)
        if vmin < 0:
            img += abs(vmin)
            vmin += abs(vmin)
            vmax += abs(vmin)
        thres = 0.4 * vmax
#            Zm = np.ma.masked_where(img > thres, img)
#            Zm = img > thres
        Zm = np.where(img > thres, 1, 0)
        img_path_out = os.path.join(img_path,"%d.png" % z)
        thres_path_out = os.path.join(thres_path,"%d.png" % z)
#        print img_path_out
#        print thres_path_out
        
        plt.imsave(img_path_out, pet_slice, cmap="gray")
        plt.imsave(thres_path_out, Zm, cmap="gray")
        
def save_resampled_ct_slices(i, patient_id, cts):
    print "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
    print "SAVING RESAMPLED CT SLICES"
    
    img_path = os.path.join(ds_path, patient_id,"dicom/ct/images/")
    if not os.path.isdir(img_path):
        os.makedirs(img_path)        
#    thres_path = os.path.join(ds_path, patient_id,"dicom/ct/thres/")
    for z, ct_slice in enumerate(cts):
#        ct_slice /= 255.0
        
        img = np.copy(ct_slice)
        vmin = np.min(img)
        vmax = np.max(img)
        if vmin < 0:
            img += abs(vmin)
            vmin += abs(vmin)
            vmax += abs(vmin)
        thres = 0.4 * vmax
#            Zm = np.ma.masked_where(img > thres, img)
#            Zm = img > thres
#        Zm = np.where(img > thres, 1, 0)
        img_path_out = os.path.join(img_path,"%d.png" % z)
#        thres_path_out = os.path.join(thres_path,"%d.png" % z)
#        print img_path_out
#        print thres_path_out
        
        plt.imsave(img_path_out, img, cmap="gray")
#        plt.imsave(thres_path_out, Zm, cmap="gray")

def save_resampled_bone_slices(i, patient_id, cts):
    print "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
    print "SAVING RESAMPLED Bone SLICES"
    
    img_path = os.path.join(ds_path, patient_id,"dicom/ct/bone_masks/")
    if not os.path.isdir(img_path):
        os.makedirs(img_path)        
#    thres_path = os.path.join(ds_path, patient_id,"dicom/ct/thres/")
    for z, ct_slice in enumerate(cts):
#        ct_slice /= 255.0
        
        
        img = np.copy(ct_slice)
        
        
        
        
        vmin = np.min(img)
        vmax = np.max(img)
        if vmin < 0:
            img += abs(vmin)
            vmin += abs(vmin)
            vmax += abs(vmin)
        min_threshold = 1024. + float(2. * float(vmax - vmin) / 10.)
        img = img > min_threshold
#            Zm = np.ma.masked_where(img > thres, img)
#            Zm = img > thres
#        Zm = np.where(img > thres, 1, 0)
        img_path_out = os.path.join(img_path,"%d.png" % z)
#        thres_path_out = os.path.join(thres_path,"%d.png" % z)
#        print img_path_out
#        print thres_path_out
        
        plt.imsave(img_path_out, img, cmap="gray")
#        plt.imsave(thres_path_out, Zm, cmap="gray")

def do_resampling(i, patient_id, pet_imgs, pet_scans, sampling_resolution):
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "before resampling pets"

    print "patient No: %d" % i
    img_after_resamp, spacing = resample(pet_imgs, pet_scans, sampling_resolution)
    print img_after_resamp.shape
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "ater resampling pets"
    
    tmpImg = np.zeros([len(img_after_resamp), CT_RESOLUTION, CT_RESOLUTION])
    for z in range(len(img_after_resamp)):
        tmpImg[z,:,:] = np.copy(cv2.resize(img_after_resamp[z], (PET_HIGHEST_RESOLUTION, PET_HIGHEST_RESOLUTION)))[PET_EXPAND_OFFSET:610,PET_EXPAND_OFFSET:610]
    save_resampled_pet_slices(i, patient_id, tmpImg)
    return tmpImg

def do_resamplingCT(i, patient_id, bone_imgs, ct_imgs, ct_scans, sampling_resolution):
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "before resampling cts"
#    img_after_resamp, spacing = resample(ct_imgs, ct_scans, sampling_resolution)
    bones_after_resamp, spacing = resample(bone_imgs, ct_scans, sampling_resolution)
#    print img_after_resamp.shape

    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "ater resampling cts"

#    tmpImg = np.zeros([len(img_after_resamp), CT_RESOLUTION, CT_RESOLUTION])        
    tmpBone = np.zeros([len(bones_after_resamp), CT_RESOLUTION, CT_RESOLUTION])   
    for z in range(len(bones_after_resamp)):
#        tmpImg[z,:,:] = np.copy(cv2.resize(img_after_resamp[z], (CT_RESOLUTION, CT_RESOLUTION)))
        tmpBone[z,:,:] = np.copy(cv2.resize(bones_after_resamp[z], (CT_RESOLUTION, CT_RESOLUTION)))
#    save_resampled_ct_slices(i, patient_id, tmpImg)
    save_resampled_bone_slices(i, patient_id, tmpBone)
    return tmpBone
    
#loading pets
#pet_scans = []
#suv_bw = []
#pet_imgs = []
#transformed_pet = []
#patientNames = []
#pets_after_resamp = []
#idx=0
#for i, patient_id in enumerate(sorted(os.listdir(ds_path))):
##    if i in range(86,99):
#    if patient_id == '99':
#        print (i, patient_id)
#        path = os.path.join(ds_path, patient_id,"dicom/pet/orig/")
#        pet_scans.append(load_scan(path))
##        suv_bw.append(dhu.get_suv_bw(path))
#        suv_bw.append(1)
#        pet_imgs.append(get_pixels(pet_scans[idx]))            
#        transformed_pet.append(pet_imgs[idx] * suv_bw[idx])
#        patientNames.append(dhu.get_patient_name(path))
#        pets_after_resamp.append(do_resampling(idx,patient_id, pet_imgs[idx], pet_scans[idx], sampling_resolution))
#        idx += 1
    
# loading cts
#ct_scans = []
#ct_imgs = []
#cts_before_resampling = []
#cts_after_resamp = [] 
#compressed_cts = []


for i, patient_id in enumerate(sorted(os.listdir(ds_path))):
    if i in range(0,100):
#    if patient_id == '99':
        print (i, patient_id)
        write_log(patient_id)
        path = os.path.join(ds_path, patient_id,"dicom/ct/orig/")
        ct_scan = load_scan(path)
#        ct_imgs.append(get_pixels_huCT(ct_scans[idx]))
#        ct_imgs = get_pixels(ct_scan)
        bone_imgs = get_pixels_huCT(ct_scan)
#        cts_before_resampling =ct_imgs
#        compressed_cts =ct_imgs
#        print "Number of ct slices: %d" % len(cts_before_resampling)    
        cts_after_resamp = do_resamplingCT(i,patient_id, bone_imgs, bone_imgs, ct_scan, sampling_resolution)
        
