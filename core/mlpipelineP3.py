#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 2 13:23:13 2018

@author: sobhan

Dear Sobhan,

unfortunatelly it takes more time to extract the data than I exspected. I already sent you the table with the first 24 patients completed, so that you can perhaps start with this data.
Please include in the clinical data the numerical data:

age (D), gelason score (G), ECOG1 (V), ALP1 (X), Alkaline phosphatease bone (Z), PSA1 (AB), time between first diagnosis (AF) and the PET (H) - this has to be calculated, weight (AG), height (AH), crea1 (AI), GGT1 (AJ), CRP1 (AK), Hb1 (AL), Erys1 (AM), Thrombos1 (AN), Leukos1 (AO)

as well as the binary data (1=yes, 2=no):

previous prostatectomy (O), previous or ongoing hormonal therapy (P), previous chemotherapy (Q), previous or ongoing bisphosphonate therapy (R), previous radiation of the prostate (S), previous radiation of bone metastases (T), previous radiation of lymph node metastases (U),

I am working to fill the table as soon as possible and provide you with the full data.

Best regards,

Ralph
"""

# imports
import matplotlib.pyplot as plt
from sklearn.svm import SVC, SVR
from sklearn.model_selection import StratifiedKFold, LeaveOneOut, KFold
from sklearn.feature_selection import RFECV
from sklearn.datasets import make_classification
import csv
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import scale
from sklearn.metrics import roc_curve, auc
from scipy import interp
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from itertools import cycle
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
import itertools
import pandas as pd
from scipy import stats
from sklearn import linear_model
# from shogun import *
from sklearn import preprocessing

# pipeline control inputs
do_loo = False
do_kfold = False
do_loo_subject = False
do_loo_subjects_new = False
do_loo_ten_subjects = False
do_loo_five_subjects = False
do_loo_three_subjects = False
do_loo_two_subjects = False
show_interim_rocs = False
do_multilabel_classification = False
do_draw_feature_importances = False
do_grid_search = False
do_draw_label_rocs = False
do_regresson = False
do_psa_classification = False
do_dgn2020 = False
do_clinical = False
balanced = False
minmaxed_psa = False


PET_CT_MODE = 22 # 0: pet, 1: ct, 2: pet/ct, 20: pet/ct with clinical
# input paths
if PET_CT_MODE == 0:
    header_path = "./csv/head.csv"
if PET_CT_MODE == 1:
    header_path = "./csv/head_ct.csv"
if PET_CT_MODE == 2:
    header_path = "./csv/head_pet_ct.csv"
if PET_CT_MODE == 3:
    header_path = "./csv/head_pet_stat.csv"
if PET_CT_MODE == 4:
    header_path = "./csv/head_pet_size.csv"
if PET_CT_MODE == 5:
    header_path = "./csv/head_pet_tex.csv"
if PET_CT_MODE == 6:
    header_path = "./csv/head_pet_zone.csv"
if PET_CT_MODE == 7:
    header_path = "./csv/head_pet_run.csv"
if PET_CT_MODE == 8:
    header_path = "./csv/head_ct_stat.csv"
if PET_CT_MODE == 9:
    header_path = "./csv/head_ct_size.csv"
if PET_CT_MODE == 10:
    header_path = "./csv/head_ct_tex.csv"
if PET_CT_MODE == 11:
    header_path = "./csv/head_ct_zone.csv"
if PET_CT_MODE == 12:
    header_path = "./csv/head_ct_run.csv"
if PET_CT_MODE == 13:
    header_path = "./csv/head_pet_ct_stat.csv"
if PET_CT_MODE == 14:
    header_path = "./csv/head_pet_ct_size.csv"
if PET_CT_MODE == 15:
    header_path = "./csv/head_pet_ct_tex.csv"
if PET_CT_MODE == 16:
    header_path = "./csv/head_pet_ct_zone.csv"
if PET_CT_MODE == 17:
    header_path = "./csv/head_pet_ct_run.csv"
if PET_CT_MODE == 20:
    header_path = "./csv/head_pet_ct_mix.csv"
if PET_CT_MODE == 21:
    header_path = "./csv/2020/head83.csv"    
if PET_CT_MODE == 22:
    header_path = "./csv/2020/orig/head_med_new.csv"

# only do regression when all best features are included in the header, i. e., entropy, homogeneity, and size variation
if do_psa_classification and not (PET_CT_MODE in [2, 15, 20, 21, 22]):
    do_regresson = False
else:
    do_regresson = True
 
num_pet_features = 39
num_ct_features = 38
num_all_features = 77
if PET_CT_MODE == 13:
    num_pet_features = 7
    num_ct_features = 6
    num_all_features = 13
elif PET_CT_MODE == 14:
    num_pet_features = 2
    num_ct_features = 2
    num_all_features = 4
elif PET_CT_MODE == 15:
    num_pet_features = 9
    num_ct_features = 10
    num_all_features = 19
elif PET_CT_MODE == 16:
    num_pet_features = 11
    num_ct_features = 11
    num_all_features = 22
elif PET_CT_MODE == 17:
    num_pet_features = 11
    num_ct_features = 11
    num_all_features = 22
    
# classifier for psa clasification: 0: rbf svc, 1: poly svc, 2: extra trees, 3: linear svc
classifier = 2

# whether to average the pet and ct probas to calculate combined proba
do_proba_average = False


# params for linear and rbf classifier, ...
dep_dec = 10
est_dec = 250

dep_dec_rf = 20
est_dec_rf = 250

c_lin = 1 # best: 2**11 
gamma_lin = 2**-13

c_rbf = 2**-5
gamma_rbf = 2**-15

train_path = "./csv/oncotarget/train99new.csv"
#train_path = "./csv/train_pet_ct_72mix.csv"
#train_path = "./csv/train_pet_ct.csv"
#train_path = "./csv/patients_cv.csv"
multi_path = "./csv/multi.csv"
train_clinical24_path = "./csv/train_clinical24.csv"
head_clinical_path = "./csv/head_clinical.csv"

# setting graph & font style
font = {'family' : 'normal',
#        'weight' : 'bold',
        'size'   : 10}

plt.rc('font', **font)

# import feature names
header = []
with open(header_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header = next(reader)
    for column in header:
        print (column)



header_pet = header[:39]
#print ("HEADERS")
#print (header_pet)
header_ct = header[39:77]
#print (header_ct)


header_clinical = []
with open(head_clinical_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header_clinical = next(reader)
    for column in header_clinical:
        print (column)

# import trainig data
data = []
labels = []
delta_psa = []
with open(train_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        data.append([row[column] for column in header])
        labels.append(int(row['label']))
        delta_psa.append(float(row['delta_psa']))
        

fi = []
fi_volume = []
fi_scaled = []
mean_fi = dict()
df = pd.read_csv(train_path, sep=';')
print ("DF")
n_features = len(header)
n_subjects = 99 # 99    83    72    #48      #30     #39     #24     #72     #30
n_hotspots = 3542 # 3542    2860     2419    #1631     #940    #1239   #797    #2419   #958


# map the patient ids
pid_range = range(n_subjects)
#pid_range = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17,21,23,24,26,32,44,63,35,27,25,28,29,40,16,38,52,51,37,18,48,20,22]
pid_map = dict()
pid_reverse_map = dict()
ii = 0
for pid in pid_range:
    pid_map[ii] = pid
    pid_reverse_map[pid] = ii
    ii += 1

header.append('patient_id')
header.append('delta_psa')

# create array of pathological hotspots
for sid in range(n_hotspots):    
    if df['label'][sid] == 1:
        fi.append([df[column][sid]for column in header])
#        fi_volume.append([df[column][sid]for column in ['Volume', 'CT_Volume']])        
#        if "CT" in column:
        
#        fi_scaled.append([df[column][sid]*df['Volume'][sid]for column in header])
#        fi_scaled.append([df[column][sid]*df['CT_Volume'][sid]for column in header_ct])
#        fi_scaled.append([df[column][sid]for column in header_clinical])
#        else:
        
fi = np.array(fi)
#fi_volume = np.array(fi_volume)
#fi_scaled = np.array(fi_scaled)

print ("FI")
print (fi.shape)
print (fi[0][6])
#print (fi_volume.shape)
#print (fi_volume)
#print (fi_scaled.shape)
#print (fi_scaled)
# create dict of pathological hotspots with patient ids as keys
patients = dict()
delta_psa_patients = []
for pid in pid_range:
    fi_ = []
#    print ("DEBUG", pid)
    for sid in range(len(fi)):
#        print (fi[sid])
        if pid == int(fi[sid][n_features]):
#            print (fi[sid])
            fi_.append(fi[sid])
    fi_ = np.array(fi_)
#    print (fi_)
#    print (fi_.shape)
    patients[pid] = fi_    
#patients = np.array(patients)
#print ("patients")
#print (patients)

out_pat = np.asarray(patients)
#print ("OUT_PAT")
#print (out_pat.shape)
#print (patients[25][10])
#print (patients[25][10][n_features+1])

#with open("./csv/out_pat.csv", 'w') as writer_file:
#    writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
#    for iii in range(len(out_pat)):
#        writer.writerow(out_pat[iii])

print ("PATIENTS")
print (patients[0])
#print (patients[0][0][81])
for pid in pid_range:
    mean_ = []
    for col in range(n_features+2):
#        print ("COL")
#        print (col)
        arr = np.array(patients[pid]).astype(np.float)
#        print ("ARR_SHAPE")
#        print (arr.shape)
#        print (pid, col)
        mean_ = np.mean(arr, axis=0)
        mean_ = np.array(mean_)
    mean_fi[pid] = mean_
#    print ("DEBUG")
#    print (n_features, pid)
#    print (mean_fi)
#    print (fi)
#    print (patients[pid])
    delta_psa_patients.append(mean_[n_features+1])
print ("DEBUG_DELTA_PSA")
print (delta_psa_patients)
#mean_fi = np.array(mean_fi)
#print (mean_fi.shape)
#print (delta_psa_patients)

print ("TEST")
single_features = []
for fid in range(n_features):
    sf = []
    for pid in pid_range:
        sf.append(mean_fi[pid][fid])
    single_features.append(sf)
#single_features = np.array(single_features)
#print (single_features.shape)
#print (len(delta_psa_patients))

print ("MEAN_FI")
#print (mean_fi)
out_mean_fi = np.array(mean_fi)
print (out_mean_fi.shape)
#with open("./csv/mean_fi.csv", 'w') as writer_file:
#    writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
#    for row in out_mean_fi:
#        writer.writerow(row)


# for clinical data
#single_features_clinical = np.array(single_features_clinical)
        
if do_clinical:
    header_clinical = []
    with open(head_clinical_path, 'rt') as header_file:
        reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        header_clinical = next(reader)
        for column in header_clinical:
            print (column)
    
    data_clinical = []
    delta_psa_clinical = []
    with open(train_clinical24_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:                
            data_clinical.append([row[column] for column in header_clinical])
            delta_psa_clinical.append(float(row['change']))
            
    single_features_clinical = dict()
    for fid in range(len(header_clinical)):
        sf = []
        for pid in pid_range:
#            print ("DEBUG PID")
#            print (pid)
#            print (pid_reverse_map[pid])
            sf.append(data_clinical[pid_reverse_map[pid]][fid])
        single_features_clinical[pid] = sf

    best_clinical_features = []
    for fid in range(len(header_clinical)):
        X = np.array([single_features_clinical[fid][pid] for pid in pid_range])
        y = np.array(delta_psa_clinical)
        X = scale(X)     
        slope, intercept, r_value, p_value, std_err = stats.linregress(X,y)
        if p_value < 0.05:
            best_clinical_features.append(header_clinical[fid])
            lw = 2
            plt.scatter(X, y, color='cyan', label='original data')
            plt.plot(X, intercept + slope * X, 'r', label='fitted line')
            plt.xlabel('%s (r_value: %0.4f, p_value: %0.4f)' % (header_clinical[fid], r_value, p_value))
            plt.ylabel('Delta PSA')
            plt.title('Regression test for clinical data' )
            plt.legend()
#            manager = plt.get_current_fig_manager()
#            manager.window.showMaximized()
            plt.savefig("./res/NEW_reg_%s.png" % header[fid], dpi=300)
            plt.show()
    fi = []

mean_fi_reg = []
if do_regresson:
    best_features = []
    for fid in range(n_features):
        X = np.array([single_features[fid][pid_reverse_map[pid]] for pid in pid_range])
        y = np.array(delta_psa_patients)
        X = scale(X) 
        slope, intercept, r_value, p_value, std_err = stats.linregress(X,y)
        if p_value < 0.05:
            best_features.append(header[fid])
            lw = 2
            plt.scatter(X, y, color='cyan', label='original data')
            plt.plot(X, intercept + slope * X, 'r', label='fitted line')
            plt.xlabel('%s (r_value: %0.4f, p_value: %0.4f)' % (header[fid], r_value, p_value))
            plt.ylabel('Delta PSA')
            plt.title('Regression test' )
            plt.legend()
#            manager = plt.get_current_fig_manager()
#            manager.window.showMaximized()
            plt.savefig("./res/NEW_reg_%s.png" % header[fid], dpi=300)
            plt.show()
    fi = []
    
    best_features.append('patient_id')
    best_features.append('delta_psa')
    for sid in range(n_hotspots):    
        if df['label'][sid] == 1:
            fi.append([df[column][sid]for column in best_features])
    fi = np.array(fi)
    print (fi.shape)
            
    patients = dict()
    delta_psa_patients = []
    for pid in pid_range:
        fi_ = []
        for sid in range(len(fi)):
            if pid == fi[sid][len(best_features)-2]:
                fi_.append(fi[sid])
        fi_ = np.array(fi_)
        patients[pid] = fi_    
    
    for pid in pid_range:
        mean_ = []
        for col in range(len(best_features)):
                mean_ = np.mean(patients[pid], axis=0)
                mean_ = np.array(mean_)
        mean_fi_reg.append(mean_)
        delta_psa_patients.append(mean_[len(best_features)-1])
    mean_fi_reg = np.array(mean_fi_reg)

    print ("MEAN_FI_REG")
    print (mean_fi_reg)


out_patients_mean = []
for pid in pid_range:
    out_patients_mean.append(mean_fi[pid])
out_patients_mean = np.array(out_patients_mean)
print (out_patients_mean.shape)
with open("./csv/patients_mean.csv", 'w') as writer_file:
    writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    writer.writerows(out_patients_mean)



X, y = np.array(data), np.array(labels)
#X = scale(X)
scalerObj = preprocessing.MinMaxScaler()
scaler = scalerObj.fit(X)
X = scaler.transform(X)




# initialize cross validation kernels
loo = LeaveOneOut()
print (loo.get_n_splits(X))
kf = KFold(n_splits=20, shuffle=True)
print (kf.get_n_splits(X))
n_classes = 2
#n_samples, n_features = X.shape




######################################
# helper functions
######################################    
# draw the ROC curves
def plot_roc_legend():
    classification_task = "hotspot"
    if do_psa_classification:
        classification_task = "PSA"
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.rc('xtick', labelsize=11)
    plt.rc('ytick', labelsize=11)
    plt.title('Receiver operating characteristic for %s classification' % classification_task)
    plt.legend(loc="lower right")
#        fig = plt.gcf()
#        fig.set_size_inches((10, 6), forward=False)
#    manager = plt.get_current_fig_manager()
#    manager.window.showMaximized()
#        fig.savefig("./res/NEW_ROC.png", dpi=300)
    plt.savefig("./res/NEW_ROC.png", dpi=300)
    plt.show()
        
        
        
# draw feature importances diagrams          
def feauture_importances(coef, std, names, title):
    names = np.array(names)
    coef = np.array(coef)
    imp = np.absolute(coef)
    imp, std, names = zip(*sorted(zip(imp, std, names)))
#    print ('INSIDE feauture_importances', coef, names)
    _names = []
    for i in range(len(names)):
        name = str(names[i])
        print ('INSIDE feauture_importances', name)
        name = name.replace("Gray-LevelNon-Uniformity", "GLNU")
        name = name.replace("BoneMineralDensity", "BMD")
        if not ('CT' in name):
            name = name.replace(name, 'PET_' + name)
        print ('INSIDE feauture_importances', name)
        _names.append(name)
#    print ('INSIDE feauture_importances', _names)        
    plt.rc('xtick', labelsize=22)
    plt.rc('ytick', labelsize=22)
    plt.title("Feature importances for %s classifier based on PET and CT features" % title)
    plt.barh(range(15), imp[-15:], xerr=std[-15:], align='center')
    plt.yticks(range(15), _names[-15:])
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig("./res/NEW_FI.png", dpi=300)
    plt.show()


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')



######################################
# start leave one out 
######################################    
if do_loo:
    scores_svc = [] # linear svc
    scores_tree = [] # extra trees
    scores_rf = [] # random forest
    
    predict_svc = [] # linear svc
    predict_tree = [] # extra trees
    predict_rf = [] # random forest
    
    
    for train_index, test_index in loo.split(X):    
        #print("TEST sample index:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
        # linear svc classifier
        clf = SVC(kernel='linear')
        clf.fit(X_train, y_train)
        scores_svc.append(clf.score(X_test, y_test))
        predict_svc.append(clf.decision_function(X_test))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(n_estimators=250, random_state=0)
        trees.fit(X_train, y_train)
        scores_tree.append(trees.score(X_test, y_test))
        predict_tree.append(trees.predict_proba(X_test))
    
    	# random forest classifierfrom sklearn.tree import DecisionTreeClassifier
        forest = RandomForestClassifier(n_estimators=250, random_state=0)
        forest.fit(X_train, y_train)
        scores_rf.append(forest.score(X_test, y_test))
        predict_rf.append(forest.predict_proba(X_test))
    	
    
    scores_svc = np.array(scores_svc)
    scores_tree = np.array(scores_tree)
    scores_rf = np.array(scores_rf)
    predict_svc = np.array(predict_svc)
    predict_tree = np.array(predict_tree)
    predict_rf = np.array(predict_rf)
    
    print (predict_svc.shape, predict_tree.shape, predict_rf.shape)
    
    #print (scores_svc, y)
    print (scores_svc.mean(), scores_tree.mean(), scores_rf.mean())
    #print (predict_svc.mean(), predict_tree.mean(), predict_rf.mean())
    
    # Compute ROC curve and area the curve for linear svc
    fpr, tpr, thresholds = roc_curve(y, predict_svc)
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Linear SVC, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	
    
    
    
    # Compute ROC curve and area the curve for extra tree
    fpr, tpr, thresholds = roc_curve(y, predict_tree[:, :, 1]) 
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Extra Trees, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	
    
    
    # Compute ROC curve and area the curve for extra tree
    fpr, tpr, thresholds = roc_curve(y, predict_rf[:, :, 1])
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Random Forest, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	



#######################################from sklearn.tree import DecisionTreeClassifier
# start kfold
#######################################
if do_kfold:
    # init roc variables
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    
    tprs_rf = []
    aucs_rf = []
    mean_rf = np.linspace(0, 1, 100)
    
    i = 0
    for train_index, test_index in kf.split(X):
        #print("TEST sample index:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
        # linear svc classifier
        clf = SVC(kernel='linear')
        probas_ = clf.fit(X_train, y_train).decision_function(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='SVC, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(n_estimators=250, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(n_estimators=250, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
    
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC: Linear SVC, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)

######################################
# start leave one subject out 
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_subject:
    # init subject based variables
    n_subjects = 30    
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    for i in pid_range:
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if i != int(row['patient_id']):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
            print ("TEST")
            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
                if pid != i:
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
#            std = np.std(train_psa_all, axis=0)
#            test_psa_all = np.subtract(test_psa_all, mean)
#            test_psa_all = np.divide(test_psa_all, std)
                        
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid != i:
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
#            train_psa_pet = scale(train_psa_pet)
#            mean = np.mean(train_psa_pet, axis=0)        
#            std = np.std(train_psa_pet, axis=0)
#            test_psa_pet = np.subtract(test_psa_pet, mean)
#            test_psa_pet = np.divide(test_psa_pet, std)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid != i:
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)
#            train_psa_ct = scale(train_psa_ct)
#            mean = np.mean(train_psa_ct, axis=0)        
#            std = np.std(train_psa_ct, axis=0)
#            test_psa_ct = np.subtract(test_psa_ct, mean)
#            test_psa_ct = np.divide(test_psa_ct, std)

            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            print ("TEST SCALEROBJ")
            print (train_psa_ct)
            print (test_psa_ct)
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid != i:
                        train_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)
#                train_psa = scale(train_psa)
#                mean = np.mean(train_psa, axis=0)        
#                std = np.std(train_psa, axis=0)
#                test_psa = np.subtract(test_psa, mean)
#                test_psa = np.divide(test_psa, std)

                scalerObj = preprocessing.MinMaxScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            
#            # extra trees classifier
#            trees = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
#            probas_ = trees.predict_proba(test_psa)
#            trees_probas.append(probas_[:,1])
#            y_pred_psa = trees.predict(test_psa)
#            y_pred_psa_trees.append(y_pred_psa[0])
#            y_score_psa = trees.score(test_psa, y_test_psa)
#            print ("PREDICTION ET")
#            print (y_pred_psa, y_test_psa, y_score_psa)
#            scores_psa.append(y_score_psa)
##            cnfs_psa.append(confusion_matrix(y_test_psa, y_pred_psa))
            
#            # random forest classifier
#            clf = RandomForestClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
#            probas_ = clf.predict_proba(test_psa)
#            rf_probas.append(probas_[:,1])
#            y_pred_psa = clf.predict(test_psa)
#            y_score_psa = clf.score(test_psa, y_test_psa)
#            print ("PREDICTION RF")
#            print (y_pred_psa, y_test_psa, y_score_psa)
#            scores_psa_rf.append(y_score_psa)
##            cnfs_psa.append(confusion_matrix(y_test_psa, y_pred_psa))
            
#            # linear svc classifier
#            clf = SVC(kernel='linear').fit(train_psa, y_train_psa)
#            probas_ = clf.decision_function(test_psa)
#            lsvc_probas.append(probas_)
#            y_pred_psa = clf.predict(test_psa)
#            y_score_psa = clf.score(test_psa, y_test_psa)
#            print ("PREDICTION linear")
#            print (y_pred_psa, y_test_psa, y_score_psa)
#            scores_psa_lsvc.append(y_score_psa)

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
            c_rbf = 2
            gamma_rbf = 0.0001
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                print ("TEST probas_pet")
                print (probas_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=2, C=1, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
            c_rbf = 2**-5
            gamma_rbf = 0.00048828125
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
            c_rbf = 2
            c_lin = 2**-5
            gamma_rbf = 0.00048828125
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 0.007812
                c_lin = 2**-5
                if classifier == 0:
                    # rbf svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=2, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
            
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("!!!!!!!appending true psa label!!!!!!!!!")
            print (y_test_psa)
            y_true_psa.append(y_test_psa[0])
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid != i:
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
#            train_reg = scale(train_reg)
#            mean = np.mean(train_reg, axis=0)        
#            std = np.std(train_reg, axis=0)
#            test_reg = np.subtract(test_reg, mean)
#            test_reg = np.divide(test_reg, std * std)
            
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            scaler = scalerObj.fit(y_train)
#            y_train = scaler.transform(y_train)
#            y_test = scaler.transform(y_test)
            
#            y_all = []
#            for sid in range(n_subjects-1):
#                y_all.append(y_train[sid])
#            y_all.append(y_test[0])
#            y_all = scale(y_all)
#            y_train = y_all[:n_subjects-1]
#            y_test = []
#            y_test.append( y_all[n_subjects-1])
#            mean_y = np.mean(y_train)
#            std_y = np.std(y_train)
#            y_test = np.subtract(y_test, mean_y)
#            y_test = np.divide(y_test, std_y * std_y)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
#    print ("SENSITIVITIES/SPECIFICITIES:")
#    print ("Linear SVC, RBF SVC, Polynomial SVC, Extra Trees, Random Forest")
#    print (lin_tpr, lin_tnr)
#    print (rbf_tpr, rbf_tnr)
#    print (poly_tpr, poly_tnr)
#    print (tree_tpr, tree_tnr)
#    print (rf_tpr, rf_tnr)
    
    
    if do_psa_classification:
#        scores_psa = np.array(scores_psa)
#        mean_score = np.mean(scores_psa, axis=0)
#        std_score = np.std(scores_psa, axis=0)
#        print("PSA average score for extra trees, mean & std:", mean_score, std_score)
#        scores_psa_rf = np.array(scores_psa_rf)
#        mean_score = np.mean(scores_psa_rf, axis=0)
#        std_score = np.std(scores_psa_rf, axis=0)
#        print("PSA average score for random forest, mean & std:", mean_score, std_score)
#        scores_psa_lsvc = np.array(scores_psa_lsvc)
#        mean_score = np.mean(scores_psa_lsvc, axis=0)
#        std_score = np.std(scores_psa_lsvc, axis=0)
#        print("PSA average score for linear svc, mean & std:", mean_score, std_score)
#        scores_psa_poly = np.array(scores_psa_poly)
#        mean_score = np.mean(scores_psa_poly, axis=0)
#        std_score = np.std(scores_psa_poly, axis=0)
#        print("PSA average score for polynomial svc, mean & std:", mean_score, std_score)
        
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            poly_probas = np.array(poly_probas)
            y_true_psa = np.array(y_true_psa)
            fpr, tpr, _ = roc_curve(y_true_psa, poly_probas)
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        # poly svc classifier all
        print ("!!!!!!!!! poly svc classifier all !!!!!!")
        poly_probas_all = np.array(poly_probas_all)
        print (poly_probas_all)
        y_true_psa = np.array(y_true_psa)
        print (y_true_psa)
        fpr, tpr, _ = roc_curve(y_true_psa, poly_probas_all)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        poly_probas_pet = np.array(poly_probas_pet)
        y_true_psa = np.array(y_true_psa)
        fpr, tpr, _ = roc_curve(y_true_psa, poly_probas_pet)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        poly_probas_ct = np.array(poly_probas_ct)
        y_true_psa = np.array(y_true_psa)
        fpr, tpr, _ = roc_curve(y_true_psa, poly_probas_ct)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
#        # bagging classifier ct
#        bag_probas_all = np.array(bag_probas_all)
#        y_true_psa = np.array(y_true_psa)
#        fpr, tpr, _ = roc_curve(y_true_psa, bag_probas_all)
#        psa_auc = auc(fpr, tpr)
#        plt.plot(fpr, tpr, color='g',
#                 label=r'ROC: Bagging ensemble, all features (AUC = %0.2f)' % (psa_auc),
#                 ls='dashed', lw=2, alpha=.8)
        
#        # linear svc classifier
#        lsvc_probas = np.array(lsvc_probas)
#        fpr, tpr, _ = roc_curve(y_true_psa, lsvc_probas)
#        psa_auc = auc(fpr, tpr)
#        plt.plot(fpr, tpr, color='g',
#                 label=r'Mean ROC of PSA: Linear SVC (AUC = %0.2f)' % (psa_auc),
#                 ls='dashed', lw=2, alpha=.8)
#        
#        # extra trees classifier
#        trees_probas = np.array(trees_probas)
#        fpr, tpr, _ = roc_curve(y_true_psa, trees_probas)
#        psa_auc = auc(fpr, tpr)
#        plt.plot(fpr, tpr, color='b',
#                 label=r'Mean ROC of PSA: Extra Trees (AUC = %0.2f)' % (psa_auc),
#                 ls='dashdot', lw=2, alpha=.8)
#        
#        # random forest classifier
#        rf_probas = np.array(rf_probas)
#        fpr, tpr, _ = roc_curve(y_true_psa, rf_probas)
#        psa_auc = auc(fpr, tpr)
#        plt.plot(fpr, tpr, color='y',
#                 label=r'Mean ROC of PSA: Random Forest (AUC = %0.2f)' % (psa_auc),
#                 ls='dotted', lw=2, alpha=.8)
#        
#        # rbf svc classifier
#        rbf_probas = np.array(rbf_probas)
#        fpr, tpr, _ = roc_curve(y_true_psa, rbf_probas)
#        psa_auc = auc(fpr, tpr)
#        plt.plot(fpr, tpr, color='cyan',
#                 label=r'Mean ROC of PSA: RBF SVC (AUC = %0.2f)' % (psa_auc),
#                 ls='dotted', lw=3, alpha=.8)
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
#        print (classification_report(y_true_psa, y_pred_psa_trees))
#        print (classification_report(y_true_psa, y_pred_psa_poly))        
    
    
    # draw feature importances?    
    if do_draw_feature_importances:
#        mean_fi_lsvc = np.mean(feature_importances_lsvc, axis=0)
#        std_fi_lsvc = np.std(feature_importances_lsvc, axis=0)        
##        feauture_importances(mean_fi_lsvc, std_fi_lsvc, header, "linear SVC")
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
#        mean_fi_rf = np.mean(feature_importances_rf, axis=0)
#        std_fi_rf = np.std(feature_importances_rf, axis=0)
#        feauture_importances(mean_fi_rf, std_fi_rf, header, "random forest")
#        
#        mean_fi_tree_r = np.mean(feature_importances_tree_r, axis=0)
#        std_fi_tree_r = np.std(feature_importances_tree_r, axis=0)
#        feauture_importances(mean_fi_tree_r, std_fi_tree_r, header, "extra trees regressor")
#        
#        mean_fi_rfr = np.mean(feature_importances_rf_r, axis=0)
#        std_fi_rfr = np.std(feature_importances_rf_r, axis=0)
#        feauture_importances(mean_fi_rfr, std_fi_rfr, header, "extra trees regressor")

    # draw confusion matrix 
    # for hotspot classifiers
    x = np.zeros_like(cnfs[0])
    x_lin = np.zeros_like(cnfs_lin[0])
    x_rbf = np.zeros_like(cnfs_rbf[0])
    x_poly = np.zeros_like(cnfs_poly[0])
    x_rf = np.zeros_like(cnfs_rf[0])
    # for psa classifiers
    print ("TEST PSA CONF")
    cnfs_psa_all = np.array(cnfs_psa_all)
    cnfs_psa_pet = np.array(cnfs_psa_pet)
    cnfs_psa_ct = np.array(cnfs_psa_ct)
    cnfs_psa_best = np.array(cnfs_psa_best)
    cnfs_lin = np.array(cnfs_lin)
    print (cnfs_lin.shape)
    print (cnfs_psa_all)
    print (cnfs_psa_pet.shape)
    print (cnfs_psa_ct.shape)
    print (cnfs_psa_best.shape)
    
    if do_psa_classification:
        xx_all = np.zeros_like(cnfs_psa_all[0])
        xx_pet = np.zeros_like(cnfs_psa_pet[0])
        xx_ct = np.zeros_like(cnfs_psa_ct[0])
        xx_best = np.zeros_like(cnfs_psa_best[0])
    for i in pid_range:
        x += cnfs[i]
        x_lin += cnfs_lin[i]
        x_rbf += cnfs_rbf[i]
        x_poly += cnfs_poly[i]
        x_rf += cnfs_rf[i]
        if do_psa_classification:
            xx_all += cnfs_psa_all[i]
            xx_pet += cnfs_psa_pet[i]
            xx_ct += cnfs_psa_ct[i]
            xx_best += cnfs_psa_best[i]

    # calclate sensitivity/specificity
    
    # for hotspot classifiers
    # Extea trees
    print ("Sensitivity/Specificity")
    fpr = float(x[0][1]/(x[0][1] + x[0][0]))
    tpr = float(x[1][1]/(x[1][1] + x[1][0]))
    tnr = float(x[0][0]/(x[0][0] + x[0][1]))
    fnr = float(x[1][0]/(x[1][0] + x[1][1]))
    print ("Extra Trees: ",tpr, tnr)       
    
    # linear SVC
    fpr = float(x_lin[0][1]/(x_lin[0][1] + x_lin[0][0]))
    tpr = float(x_lin[1][1]/(x_lin[1][1] + x_lin[1][0]))
    tnr = float(x_lin[0][0]/(x_lin[0][0] + x_lin[0][1]))
    fnr = float(x_lin[1][0]/(x_lin[1][0] + x_lin[1][1]))
    print ("Linear SVC: ",tpr, tnr) 

    # rbf
    fpr = float(x_rbf[0][1]/(x_rbf[0][1] + x_rbf[0][0]))
    tpr = float(x_rbf[1][1]/(x_rbf[1][1] + x_rbf[1][0]))
    tnr = float(x_rbf[0][0]/(x_rbf[0][0] + x_rbf[0][1]))
    fnr = float(x_rbf[1][0]/(x_rbf[1][0] + x_rbf[1][1]))
    print ("RBF SVC: ",tpr, tnr)             
    
    # poly
    fpr = float(x_poly[0][1]/(x_poly[0][1] + x_poly[0][0]))
    tpr = float(x_poly[1][1]/(x_poly[1][1] + x_poly[1][0]))
    tnr = float(x_poly[0][0]/(x_poly[0][0] + x_poly[0][1]))
    fnr = float(x_poly[1][0]/(x_poly[1][0] + x_poly[1][1]))
    print ("Poly SVC: ",tpr, tnr)       
    
    # rf
    fpr = float(x_rf[0][1]/(x_rf[0][1] + x_rf[0][0]))
    tpr = float(x_rf[1][1]/(x_rf[1][1] + x_rf[1][0]))
    tnr = float(x_rf[0][0]/(x_rf[0][0] + x_rf[0][1]))
    fnr = float(x_rf[1][0]/(x_rf[1][0] + x_rf[1][1]))
    print ("Random Forest: ",tpr, tnr)       
    
    
    if do_psa_classification:
        # for psa classifiers 
        print ("Sensitivity/Specificity for PSA analysis")
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        
        # for all
        fpr = float(xx_all[0][1]/(xx_all[0][1] + xx_all[0][0]))
        tpr = float(xx_all[1][1]/(xx_all[1][1] + xx_all[1][0]))
        tnr = float(xx_all[0][0]/(xx_all[0][0] + xx_all[0][1]))
        fnr = float(xx_all[1][0]/(xx_all[1][0] + xx_all[1][1]))
        print ("%s for PET/CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for pet
        fpr = float(xx_pet[0][1]/(xx_pet[0][1] + xx_pet[0][0]))
        tpr = float(xx_pet[1][1]/(xx_pet[1][1] + xx_pet[1][0]))
        tnr = float(xx_pet[0][0]/(xx_pet[0][0] + xx_pet[0][1]))
        fnr = float(xx_pet[1][0]/(xx_pet[1][0] + xx_pet[1][1]))
        print ("%s for PET: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for ct
        fpr = float(xx_ct[0][1]/(xx_ct[0][1] + xx_ct[0][0]))
        tpr = float(xx_ct[1][1]/(xx_ct[1][1] + xx_ct[1][0]))
        tnr = float(xx_ct[0][0]/(xx_ct[0][0] + xx_ct[0][1]))
        fnr = float(xx_ct[1][0]/(xx_ct[1][0] + xx_ct[1][1]))
        print ("%s for CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for best
        fpr = float(xx_best[0][1]/(xx_best[0][1] + xx_best[0][0]))
        tpr = float(xx_best[1][1]/(xx_best[1][1] + xx_best[1][0]))
        tnr = float(xx_best[0][0]/(xx_best[0][0] + xx_best[0][1]))
        fnr = float(xx_best[1][0]/(xx_best[1][0] + xx_best[1][1]))
        print ("%s for BEST: %f, %f" % (clf_txt, tpr, tnr))       
        
    class_names = ["physiological", "pathological" ]
    class_names_psa = ["non_responder", "responder" ]
    plot_confusion_matrix(x, classes=class_names, normalize=False,
                              title='Confusion matrix')        
    plt.show()
    
#    plot_confusion_matrix(x_lin, classes=class_names, normalize=False,
#                              title='Confusion matrix')        
#    plt.show()
    
    if do_psa_classification:
        
        plot_confusion_matrix(xx_all, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_pet, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_ct, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_best, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
    
    # psa regressoion scores
    if do_regresson:
        # RRRRRRRRR
        y_true_etr = np.array(y_true_etr)    
        mean_y_true = np.mean(y_true_etr, axis=0)
        y_pred_etr = np.array(y_pred_etr)
        y_pred_rbf = np.array(y_pred_rbf)
        y_pred_rfr = np.array(y_pred_rfr)
        y_pred_poly = np.array(y_pred_poly)
        y_pred_lin = np.array(y_pred_lin)
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("ETR")
        print (np.mean(etr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))        
          
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RBF")
        print (np.mean(rbfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("POLY")
        print (np.mean(polyr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("LIN")
        print (np.mean(lnr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RFR")
        print (np.mean(rfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        
######################################
# mulitable classification
######################################    

def get_label(id):
        if id == 0: return "Bone"
        elif id == 1: return "Lymph node"
        elif id == 2: return "Kidney"
        elif id == 3: return "Liver"
        elif id == 4: return "Prostate"
        elif id == 5: return "Bladder"
        elif id == 6: return "Lung"
        elif id == 7: return "Spleen"
        elif id == 8: return "Sal. gland"
        elif id == 9: return "Other tissue"


# init feature importance matrices
feature_importances_lsvc = []
feature_importances_ada = []
if do_multilabel_classification:
    cnfs = []
    precisions = []
    recalls = []
    fbetas = []
    supports = []
    n_classes = 10
    n_subjects = 30
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    tprs_labels_svc = []
    fprs_labels_svc = []
    aucs_labels_svc = []
    mean_fpr_labels_svc = np.linspace(0, 1, 100)
    mean_fpr = np.linspace(0, 1, 100)
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    # adaboost
    tprs_ada = []
    aucs_ada = []
    mean_fpr_ada = np.linspace(0, 1, 100)
    # rf
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100)
    # et
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    
    clfs = []
    y_scores = []
    clf_names = ['linear SVC', 'poly SVC', 'AdaBoost']
    clfs.append(OneVsRestClassifier(SVC(kernel='linear', probability=True)))
    clfs.append(OneVsRestClassifier(SVC(kernel='poly', degree=2, C=1.0)))
    clfs.append(OneVsRestClassifier(AdaBoostClassifier()))
    
    for pid in pid_range:
        print (pid)
    
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        # import data
        with open(multi_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if pid != int(row['patient_id']):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['category']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['category']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        y_test_b = label_binarize(y_test, classes=range(n_classes))
        y_train_b = label_binarize(y_train, classes=range(n_classes))
        
        
        print ("X_test.shape, y_test.shape", X_test.shape, y_test.shape)
        # normalize test data
        X_test = np.subtract(X_test, mean)
        X_test = np.divide(X_test, std * std)
        
        # linear svc classifier
        clf = OneVsRestClassifier(SVC(kernel='linear', probability=True)).fit(X_train, y_train_b)
        probas_ = clf.decision_function(X_test)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):            
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_svc.append(interp(mean_fpr, fpr["micro"], tpr["micro"]))
        tprs_svc[-1][0] = 0.0
        aucs_svc.append(roc_auc["micro"])
       
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (pid, roc_auc))
            
            
        # polynomial svc classifier
        clf = OneVsRestClassifier(SVC(kernel='poly', degree=2, C=1.0)).fit(X_train, y_train_b)
        probas_ = clf.decision_function(X_test)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_poly.append(interp(mean_fpr_poly, fpr["micro"], tpr["micro"]))
        tprs_poly[-1][0] = 0.0
        aucs_poly.append(roc_auc["micro"])


        # AdaBoostClassifier
        clf = AdaBoostClassifier().fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        feature_importances_ada.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        fprs = []
        tprs = []
        aucs = []
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test, probas_[:, i], pos_label=i)
            tprs.append(tpr[i])
            fprs.append(fpr[i])
            roc_auc[i] = auc(fpr[i], tpr[i])
            aucs.append(roc_auc[i]) 
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_ada.append(interp(mean_fpr_ada, fpr["micro"], tpr["micro"]))
        tprs_ada[-1][0] = 0.0
        aucs_ada.append(roc_auc["micro"])
        
        
        # RandomForestClassifier        
        clf = RandomForestClassifier(n_estimators=250, criterion = 'entropy', random_state=0).fit(X_train, y_train)
#        clf = RandomForestClassifier(max_depth=20, min_samples_leaf=1, n_estimators=45).fit(X_train, y_train)
        probas_ = clf.predict_proba(X_test)
        
        
        
        feature_importances_rf.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])  
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_rf.append(interp(mean_fpr_rf, fpr["micro"], tpr["micro"]))
        tprs_rf[-1][0] = 0.0
        aucs_rf.append(roc_auc["micro"])
        
        
        # ExtraTreesClassifier        
#        clf = ExtraTreesClassifier(n_estimators=250, criterion = 'entropy', random_state=0).fit(X_train, y_train)
        clf = ExtraTreesClassifier(max_depth=20, min_samples_leaf=1, n_estimators=250, criterion='entropy', random_state=0).fit(X_train, y_train)
        probas_ = clf.predict_proba(X_test)
        
        # feeding confusion matrix
        y_pred = clf.predict(X_test)
        y_pred = np.array(y_pred)
        class_names = [cat for cat in range(10)]
#        print ("y_test, y_pred", y_test, y_pred)
        cnf = np.zeros((10,10))
#        for i in range(n_classes):
        cnf = confusion_matrix(y_test, y_pred, labels=class_names) # , labels=class_names
        p, r, f, s = precision_recall_fscore_support(y_test, y_pred, labels=class_names)
        print ("CNF", cnf.shape)
        print (cnf)
#        prfs = np.array(prfs)
        print ("PRFS")
        print (p)
        print (r)
        print (f)
        print (s)
#        print (prfs)
        cnfs.append(cnf)
        precisions.append(p)
        recalls.append(r)
        fbetas.append(f)
        supports.append(s)
        
        feature_importances_tree.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        
        colors = ['r', 'g', 'b', 'y', 'cyan', 'gray', 'brown', 'aqua', 'darkorange', 'cornflowerblue']
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])            
        
        tprs_labels_svc.append(tprs)
        fprs_labels_svc.append(fprs)
        aucs_labels_svc.append(aucs)
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_tree.append(interp(mean_fpr_tree, fpr["micro"], tpr["micro"]))
        tprs_tree[-1][0] = 0.0
        aucs_tree.append(roc_auc["micro"])

            
#    # linear svc classifier
    mean_fpr = np.array(mean_fpr)
    tprs_svc = np.array(tprs_svc)
    print ("tprs_svc", tprs_svc.shape)
    print ("mean_fpr", mean_fpr.shape)
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='r',
             label=r'Mean ROC: %s (AUC = %0.2f $\pm$ %0.2f)' % (clf_names[0], mean_auc, std_auc),
             lw=2, alpha=.8)
    fprs = []
    tprs = []
    aucs = []
    colors = ['r', 'g', 'b', 'y', 'cyan', 'gray', 'brown', 'aqua', 'darkorange', 'cornflowerblue']
    tprs_labels_svc = np.array(tprs_labels_svc)
    fprs_labels_svc = np.array(fprs_labels_svc)
    aucs_labels_svc = np.array(aucs_labels_svc)
    mean_fpr_labels_svc = np.array(mean_fpr_labels_svc)
    print ("tprs_labels_svc", tprs_labels_svc.shape)
    print ("fprs_labels_svc", fprs_labels_svc.shape)
    print ("aucs_labels_svc", aucs_labels_svc.shape)
    print ("mean_fpr_labels_svc", mean_fpr_labels_svc.shape)
    for i in range(n_classes):
        fprs_ = []
        tprs_ = []
        aucs_ = []
        for pid in pid_range:
            tprs_.append(tprs_labels_svc[pid][i])
            fprs_.append(fprs_labels_svc[pid][i])
            aucs_.append(aucs_labels_svc[pid][i])
        fprs.append(fprs_)
        tprs.append(tprs_)
        aucs.append(aucs_)
    fprs = np.array(fprs)
    tprs = np.array(tprs)    
    aucs = np.array(aucs)
    
    for i in range(n_classes):
#        fprs_ = []
#        tprs_ = []
        fprs_ = np.unique(np.concatenate([fprs[i][pid] for pid in pid_range]))        
        tprs_ = np.zeros_like(fprs_)
        for pid in pid_range:
            tprs_ += interp(fprs_, fprs[i][pid], tprs[i][pid])     
        tprs_ /= n_subjects
        fprs_ = np.nan_to_num(fprs_)
        tprs_ = np.nan_to_num(tprs_)
        aucs[i] = np.nan_to_num(aucs[i])
        mean_auc = np.mean(aucs[i], axis=0)        
        std_auc = np.std(aucs[i], axis=0)        
        print ("mean_auc",i , mean_auc)        
        print ("fprs_", fprs_)
        print ("tprs_", tprs_)
#        plt.plot(fprs_, tprs_, color=colors[i],
#            label=r'Mean ROC for class %s, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (get_label(i), mean_auc, std_auc),
#            lw=1, alpha=.8)
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='g',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)


    # adaboost classifier
    mean_tpr = np.mean(tprs_ada, axis=0)
    mean_auc = auc(mean_fpr_ada, mean_tpr)
    std_auc = np.std(aucs_ada)
    plt.plot(mean_fpr_ada, mean_tpr, color='b',
             label=r'Mean ROC: AdaBoost (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls=':', lw=2, alpha=.8)
    
    # rf classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='y',
             label=r'Mean ROC: Random forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    
    
    # et classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='cyan',
             label=r'Mean ROC: Extra trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=3, alpha=.8)
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()
    
        # draw feature importances?    
    if do_draw_feature_importances:
#        mean_fi_lsvc = np.mean(feature_importances_lsvc, axis=0)
#        std_fi_lsvc = np.std(feature_importances_lsvc, axis=0)
#        feauture_importances(mean_fi_lsvc, std_fi_lsvc, header, "linear SVC")
#        mean_fi_ada = np.mean(feature_importances_ada, axis=0)
#        std_fi_ada = np.std(feature_importances_ada, axis=0)
#        feauture_importances(mean_fi_ada, std_fi_ada, header, "AdaBoost")
#        mean_fi_rf = np.mean(feature_importances_rf, axis=0)
#        std_fi_rf = np.std(feature_importances_rf, axis=0)
#        feauture_importances(mean_fi_rf, std_fi_rf, header, "random forest")
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
        
    plt.figure()
    class_names = ["bone", "lymph node", "kidney", "liver", "prostate", "bladder", "lung", "spleen", "sal. gland", "other"]
    x = np.zeros_like(cnfs[0])
    p = np.zeros_like(precisions[0])
    r = np.zeros_like(recalls[0])
    f = np.zeros_like(fbetas[0])
    s = np.zeros_like(supports[0])
    print ("XXXXXXXXXX", x.shape)
    for i in pid_range:
        x += cnfs[i]
        p += precisions[i]
        r += recalls[i]
        f += fbetas[i]
        s += supports[i]        
    precision = p
    recall = r
    fbeta = f
    support = s
    print ("AVG PRFS")
    print (precision)
    print (recall)
    print (fbeta)
    print (support)
    plot_confusion_matrix(x, classes=class_names, normalize=False,
                              title='Confusion matrix')        
    plt.show()
   
if do_grid_search:
    from sklearn.svm import SVR
    # Split the dataset in two equal parts
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.5, random_state=0)
    
    
    
############################################
######## legend for tuned params   #########
############################################
#tuned_parameters = [{'kernel': ['rbf'], 
#                             'C': [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5],
#                             'gamma': [1e-3, 1e-4, 2**-15, 2**-13, 2**-11, 2**-9, 2**-7, 2**-5, 2**-3, 2**-1, 2, 2**3]},
#                            {'kernel': ['linear'],
#                             'C': [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5]},
#                            {'kernel': ['poly'], 'degree': [2, 3]}]
############################################



#    # Set the parameters by cross-validation
#    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
#                         'C': [1, 10, 100, 1000]},
#                        {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
#                        {'kernel': ['poly'], 'degree': [2, 3]}]
#    
#    scores = ['precision', 'recall']
#    
#    for score in scores:
#        print("# Tuning hyper-parameters for %s" % score)
#        print()
#    
#        clf = GridSearchCV(SVC(), tuned_parameters, cv=5,
#                           scoring='%s_macro' % score)
#        clf.fit(X_train, y_train)
#    
#        print("Best parameters set found on development set:")
#        print()
#        print(clf.best_params_)
#        print()
#        print("Grid scores on development set:")
#        print()
#        means = clf.cv_results_['mean_test_score']
#        stds = clf.cv_results_['std_test_score']
#        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
#            print("%0.3f (+/-%0.03f) for %r"
#                  % (mean, std * 2, params))
#        print()
#    
#        print("Detailed classification report:")
#        print()
#        print("The model is trained on the full development set.")
#        print("The scores are computed on the full evaluation set.")
#        print()
#        y_true, y_pred = y_test, clf.predict(X_test)
#        print(classification_report(y_true, y_pred))
#        print()
#        
        
        
#    rfc = RandomForestClassifier(n_jobs=-1, max_features='sqrt', oob_score = True) 
# 
#    # Use a grid over parameters of interest
#    param_grid = { 
#               "n_estimators" : [10, 20, 50, 100, 150, 250],
#               "max_depth" : [1, 5, 10, 15, 20, 25, 30],
#               "min_samples_leaf" : [1, 2, 4, 6, 8, 10]}
#     
#    CV_rfc = GridSearchCV(estimator=rfc, param_grid=param_grid, cv= 10)
#    CV_rfc.fit(X_train, y_train)
#    print (CV_rfc.best_params_)
    print ("GRID_SEARCH")
    print (np.array(mean_fi).shape)
    


    best_scores = []
    best_clf_scores = []
    cv_steps = 1
    for i in range(0, 30, cv_steps):
        
        train_psa = []
        test_psa = []
        y_train_psa = []
        y_test_psa = []
        for pid in pid_range:
            if pid not in range(i, i+cv_steps):
                train_psa.append([mean_fi[pid][fid] for fid in range(n_features)])
                if mean_fi[pid][n_features+1] >= 0:
                    y_train_psa.append(1)
                else:
                    y_train_psa.append(0)
            else:
                test_psa.append([mean_fi[pid][fid] for fid in range(n_features)])
                if mean_fi[pid][n_features+1] >= 0:
                    y_test_psa.append(1)
                else:
                    y_test_psa.append(0)        

        # normalize data                    
        train_psa = np.array(train_psa)
#        train_psa = scale(train_psa)
#        mean = np.mean(train_psa, axis=0)        
#        std = np.std(train_psa, axis=0)
#        test_psa = np.subtract(test_psa, mean)
#        test_psa = np.divide(test_psa, std * std)        
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(train_psa)
        train_psa = scaler.transform(train_psa)
        test_psa = scaler.transform(test_psa)
        
        
        y_train_psa = np.array(y_train_psa)
        y_test_psa = np.array(y_test_psa)
        
        
############################################        
##########   uncomment for SVC #############
############################################        
        # set parameters
        tuned_parameters = [{'kernel': ['rbf'], 
                             'C': [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5],
                             'gamma': [1e-3, 1e-4, 2**-15, 2**-13, 2**-11, 2**-9, 2**-7, 2**-5, 2**-3, 2**-1, 2, 2**3],
                             'class_weight': ['balanced', None]}]
        
        scores = ['precision', 'recall']   
        print("GridSearch No. ", i)
        for score in scores:
            clf = GridSearchCV(SVC(), tuned_parameters, cv=5,
                               scoring='%s_macro' % score)
            clf.fit(train_psa, y_train_psa)
        
            
#            print("Best parameters set found on development set:")
            c = gamma = kernel = degree = class_weight = -1 
            if clf.best_params_['kernel'] == 'rbf':
                c, gamma, class_weight, kernel = clf.best_params_['C'], clf.best_params_['gamma'], clf.best_params_['class_weight'], clf.best_params_['kernel']
            elif clf.best_params_['kernel'] == 'linear':
                c, kernel = clf.best_params_['C'],  clf.best_params_['kernel']
            elif clf.best_params_['kernel'] == 'poly':
                degree, kernel = clf.best_params_['degree'], clf.best_params_['kernel']
            best_clf_scores.append(clf.best_score_)
            best_scores.append([clf.best_score_, c, degree, gamma, class_weight, kernel, score, i])
            means = clf.cv_results_['mean_test_score']
            stds = clf.cv_results_['std_test_score']
            y_true, y_pred = y_test_psa, clf.predict(test_psa)
    print ("BEST RESULTS")
    for score_line in sorted(best_scores):
        print (score_line)
    


#############################################        
###########   uncomment for RFC #############
#############################################


#        scores = ['precision', 'recall']   
#        print("GridSearch No. ", i)
#        for score in scores:
#            rfc = RandomForestClassifier(n_jobs=-1, max_features='sqrt', oob_score = True) 
# 
#            # Use a grid over parameters of interest
#            param_grid = { 
#                       "n_estimators" : [10, 100, 250, 1000],
#                       "max_depth" : [1, 10, 20, 30],
#                       "min_samples_leaf" : [1, 10, 100]}
#             
#            CV_rfc = GridSearchCV(estimator=rfc, param_grid=param_grid, cv= 3)
#            CV_rfc.fit(X_train, y_train)
#            print (CV_rfc.best_params_)


#############################################        
###########   uncomment for SVR #############
#############################################
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid != i:
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
#            train_reg = scale(train_reg)
#            mean = np.mean(train_reg, axis=0)        
#            std = np.std(train_reg, axis=0)
#            test_reg = np.subtract(test_reg, mean)
#            test_reg = np.divide(test_reg, std * std)
            
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            np.random.seed(0)
            parameters = {'kernel': ('linear', 'rbf','poly'), 'C':[1.5, 10],'gamma': [1e-7, 1e-4],'epsilon':[0.1,0.2,0.5,0.3]}
            svr = SVR()
            clf = GridSearchCV(svr, parameters)
            clf.fit(train_reg,y_train)
            clf.best_params_
    print ("Average Score")
    print (np.mean(best_clf_scores))

if do_draw_label_rocs:
    # init data containers
    n_classes = 10
    data = []
    labels = []
    # import data
    with open(multi_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:  
            data.append([row[column] for column in header])
            labels.append(int(row['category']))
                
    X, y = np.array(data), np.array(labels)   
    print("X, y", X.shape, y.shape)
    X = scale(X)
    
#    y = label_binarize(y, classes=range(n_classes))

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.5,
                                                        random_state=0)
    
    y_train_b = label_binarize(y_train, classes=range(n_classes))
    y_test_b = label_binarize(y_test, classes=range(n_classes))
    print("y_train_b, y_test_b", y_train_b.shape, y_test_b.shape)
    print("y_train, y_test", y_train.shape, y_test.shape)
    # Learn to predict each class against the other
#    classifier = OneVsRestClassifier(SVC(kernel='linear', probability=True,
#                                     random_state=0))
#    y_score = classifier.fit(X_train, y_train).decision_function(X_test)
    
    clf = ExtraTreesClassifier(n_estimators=250, criterion = 'entropy', random_state=0).fit(X_train, y_train)
    probas_ = clf.predict_proba(X_test)
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
#        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], probas_[:, i])
        fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
        plt.plot(fpr[i], tpr[i], color='darkorange',
         lw=1, label='ROC curve (area = %0.2f)' % roc_auc[i])
        
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend() 
    
    
######################################
# start leave ten subjects out 
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_ten_subjects:
    cv_steps = 10
    # init subject based variables
    n_subjects = 83    
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    for i in [0,10,20]:
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) not in range(i, i+cv_steps):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
            print ("TEST")
#            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
#                if pid != i:
                if pid not in range(i, i+cv_steps):
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)

            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid not in range(i, i+cv_steps):
                        train_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)

                scalerObj = preprocessing.MinMaxScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
            c_rbf = 2
            gamma_rbf = 0.0001
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=2, C=1, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
            c_rbf = 2**-5
            gamma_rbf = 0.00048828125
            c_lin = 2**-5
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
            c_rbf = 2
            c_lin = 2**-5
            gamma_rbf = 0.00048828125
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 0.007812
                c_lin = 2**-5
                if classifier == 0:
                    # linear svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=2, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                        
                print ("POBAS_TEST")
                print (probas_)
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("Y_PSA_TEST")
            print (y_test_psa)
            y_true_psa.append(y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
    
    
    if do_psa_classification:
        if i == 10:
            i = 1
        elif i == 20:
            i = 2
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            poly_probas[i] = np.array(poly_probas[i])
            y_true_psa[i] = np.array(y_true_psa[i])
            fpr, tpr, _ = roc_curve(y_true_psa[i], poly_probas[i])
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        # poly svc classifier all
        poly_probas_all[i] = np.array(poly_probas_all[i])
        y_true_psa[i] = np.array(y_true_psa[i])
        fpr, tpr, _ = roc_curve(y_true_psa[i], poly_probas_all[i])
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        poly_probas_pet[i] = np.array(poly_probas_pet[i])
        y_true_psa[i] = np.array(y_true_psa[i])
        fpr, tpr, _ = roc_curve(y_true_psa[i], poly_probas_pet[i])
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        poly_probas_ct[i] = np.array(poly_probas_ct[i])
        y_true_psa[i] = np.array(y_true_psa[i])
        fpr, tpr, _ = roc_curve(y_true_psa[i], poly_probas_ct[i])
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
    
    # draw feature importances?    
    if do_draw_feature_importances:
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
    # draw confusion matrix 
    # for hotspot classifiers
    x = np.zeros_like(cnfs[0])
    x_lin = np.zeros_like(cnfs_lin[0])
    x_rbf = np.zeros_like(cnfs_rbf[0])
    x_poly = np.zeros_like(cnfs_poly[0])
    x_rf = np.zeros_like(cnfs_rf[0])
    # for psa classifiers
    print ("TEST PSA CONF")
    cnfs_psa_all = np.array(cnfs_psa_all)
    cnfs_psa_pet = np.array(cnfs_psa_pet)
    cnfs_psa_ct = np.array(cnfs_psa_ct)
    cnfs_psa_best = np.array(cnfs_psa_best)
    cnfs_lin = np.array(cnfs_lin)
    print (cnfs_lin.shape)
    print (cnfs_psa_all)
    print (cnfs_psa_pet.shape)
    print (cnfs_psa_ct.shape)
    print (cnfs_psa_best.shape)
    
    if do_psa_classification:
        xx_all = np.zeros_like(cnfs_psa_all[0])
        xx_pet = np.zeros_like(cnfs_psa_pet[0])
        xx_ct = np.zeros_like(cnfs_psa_ct[0])
        xx_best = np.zeros_like(cnfs_psa_best[0])
    for i in pid_range:
        x += cnfs[i]
        x_lin += cnfs_lin[i]
        x_rbf += cnfs_rbf[i]
        x_poly += cnfs_poly[i]
        x_rf += cnfs_rf[i]
        if do_psa_classification:
            xx_all += cnfs_psa_all[i]
            xx_pet += cnfs_psa_pet[i]
            xx_ct += cnfs_psa_ct[i]
            xx_best += cnfs_psa_best[i]

    # calclate sensitivity/specificity
    
    # for hotspot classifiers
    # Extea trees
    print ("Sensitivity/Specificity")
    fpr = float(x[0][1]/(x[0][1] + x[0][0]))
    tpr = float(x[1][1]/(x[1][1] + x[1][0]))
    tnr = float(x[0][0]/(x[0][0] + x[0][1]))
    fnr = float(x[1][0]/(x[1][0] + x[1][1]))
    print ("Extra Trees: ",tpr, tnr)       
    
    # linear SVC
    fpr = float(x_lin[0][1]/(x_lin[0][1] + x_lin[0][0]))
    tpr = float(x_lin[1][1]/(x_lin[1][1] + x_lin[1][0]))
    tnr = float(x_lin[0][0]/(x_lin[0][0] + x_lin[0][1]))
    fnr = float(x_lin[1][0]/(x_lin[1][0] + x_lin[1][1]))
    print ("Linear SVC: ",tpr, tnr) 

    # rbf
    fpr = float(x_rbf[0][1]/(x_rbf[0][1] + x_rbf[0][0]))
    tpr = float(x_rbf[1][1]/(x_rbf[1][1] + x_rbf[1][0]))
    tnr = float(x_rbf[0][0]/(x_rbf[0][0] + x_rbf[0][1]))
    fnr = float(x_rbf[1][0]/(x_rbf[1][0] + x_rbf[1][1]))
    print ("RBF SVC: ",tpr, tnr)             
    
    # poly
    fpr = float(x_poly[0][1]/(x_poly[0][1] + x_poly[0][0]))
    tpr = float(x_poly[1][1]/(x_poly[1][1] + x_poly[1][0]))
    tnr = float(x_poly[0][0]/(x_poly[0][0] + x_poly[0][1]))
    fnr = float(x_poly[1][0]/(x_poly[1][0] + x_poly[1][1]))
    print ("Poly SVC: ",tpr, tnr)       
    
    # rf
    fpr = float(x_rf[0][1]/(x_rf[0][1] + x_rf[0][0]))
    tpr = float(x_rf[1][1]/(x_rf[1][1] + x_rf[1][0]))
    tnr = float(x_rf[0][0]/(x_rf[0][0] + x_rf[0][1]))
    fnr = float(x_rf[1][0]/(x_rf[1][0] + x_rf[1][1]))
    print ("Random Forest: ",tpr, tnr)       
    
    
    if do_psa_classification:
        # for psa classifiers 
        print ("Sensitivity/Specificity for PSA analysis")
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        
        # for all
        fpr = float(xx_all[0][1]/(xx_all[0][1] + xx_all[0][0]))
        tpr = float(xx_all[1][1]/(xx_all[1][1] + xx_all[1][0]))
        tnr = float(xx_all[0][0]/(xx_all[0][0] + xx_all[0][1]))
        fnr = float(xx_all[1][0]/(xx_all[1][0] + xx_all[1][1]))
        print ("%s for PET/CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for pet
        fpr = float(xx_pet[0][1]/(xx_pet[0][1] + xx_pet[0][0]))
        tpr = float(xx_pet[1][1]/(xx_pet[1][1] + xx_pet[1][0]))
        tnr = float(xx_pet[0][0]/(xx_pet[0][0] + xx_pet[0][1]))
        fnr = float(xx_pet[1][0]/(xx_pet[1][0] + xx_pet[1][1]))
        print ("%s for PET: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for ct
        fpr = float(xx_ct[0][1]/(xx_ct[0][1] + xx_ct[0][0]))
        tpr = float(xx_ct[1][1]/(xx_ct[1][1] + xx_ct[1][0]))
        tnr = float(xx_ct[0][0]/(xx_ct[0][0] + xx_ct[0][1]))
        fnr = float(xx_ct[1][0]/(xx_ct[1][0] + xx_ct[1][1]))
        print ("%s for CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for best
        fpr = float(xx_best[0][1]/(xx_best[0][1] + xx_best[0][0]))
        tpr = float(xx_best[1][1]/(xx_best[1][1] + xx_best[1][0]))
        tnr = float(xx_best[0][0]/(xx_best[0][0] + xx_best[0][1]))
        fnr = float(xx_best[1][0]/(xx_best[1][0] + xx_best[1][1]))
        print ("%s for BEST: %f, %f" % (clf_txt, tpr, tnr))       
        
    class_names = ["physiological", "pathological" ]
    class_names_psa = ["non_responder", "responder" ]
    plot_confusion_matrix(x, classes=class_names, normalize=False,
                              title='Confusion matrix')        
    plt.show()
    
#    plot_confusion_matrix(x_lin, classes=class_names, normalize=False,
#                              title='Confusion matrix')        
#    plt.show()
    
    if do_psa_classification:
        
        plot_confusion_matrix(xx_all, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_pet, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_ct, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_best, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
    
    # regressoion scores
    if do_regresson:
        # RRRRRRRRR
        y_true_etr = np.array(y_true_etr)    
        mean_y_true = np.mean(y_true_etr, axis=0)
        y_pred_etr = np.array(y_pred_etr)
        y_pred_rbf = np.array(y_pred_rbf)
        y_pred_rfr = np.array(y_pred_rfr)
        y_pred_poly = np.array(y_pred_poly)
        y_pred_lin = np.array(y_pred_lin)
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("ETR")
        print (np.mean(etr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))        
          
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RBF")
        print (np.mean(rbfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("POLY")
        print (np.mean(polyr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("LIN")
        print (np.mean(lnr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RFR")
        print (np.mean(rfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        
######################################
# start leave five subjects out 
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_five_subjects:
    cv_steps = 5
    # init subject based variables
    n_subjects = 72    
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    ##################
    # appending all  #
    ##################
    probas_best = []
    probas_all = []
    probas_pet_ = []
    probas_ct_ = []
    y_true_psa_ = []
    
    for i in range(0,30,cv_steps):
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) not in range(i, i+cv_steps):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
            print ("TEST")
            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
#                if pid != i:
                if pid not in range(i, i+cv_steps):
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MaxAbsScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)

            scalerObj = preprocessing.MaxAbsScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid not in range(i, i+cv_steps):
                        train_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)

                scalerObj = preprocessing.MaxAbsScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
            c_rbf = 2**15
            gamma_rbf = 0.1
#            c_rbf = 2**-5
#            gamma_rbf = 0.00048828125
            c_lin = 2**15
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=3, C=1).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (probas_pet)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
            c_rbf = 2**-5
            gamma_rbf = 0.00048828125
            c_lin = 2**-5
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (probas_ct)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
#            c_rbf = 2
#            gamma_rbf = 0.00048828125
            c_rbf = 2**15
            gamma_rbf = 2**-15                                  
            
            c_lin = 2**15            
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (probas_)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 0.007812
                c_lin = 2**-5
                if classifier == 0:
                    # linear svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=3, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                        
                print ("POBAS_TEST")
                print (probas_)
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (probas_)
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("Y_PSA_TEST")
            print (y_test_psa)
            y_true_psa.append(y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
    
    for i in range(0,30,cv_steps):
        if do_psa_classification:
            
            i= int(i/cv_steps)
            
            for ii in range(0, cv_steps):
                print (poly_probas[i])
                probas_best.append(poly_probas[i][ii])
                probas_all.append(poly_probas_all[i][ii])
                probas_pet_.append(poly_probas_pet[i][ii])
                probas_ct_.append(poly_probas_ct[i][ii])
                y_true_psa_.append(y_true_psa[i][ii])
            
            print ("THE GREAT TEST!!!")
            print (probas_all)
            print (probas_pet_)
            print (probas_ct_)
            print (probas_best)
            
    if do_psa_classification:            
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            probas_best = np.array(probas_best)
            y_true_psa_ = np.array(y_true_psa_)
            print (y_true_psa_.shape, probas_best.shape)
            fpr, tpr, _ = roc_curve(y_true_psa_, probas_best)
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        
        
        
        # poly svc classifier all
        probas_all = np.array(probas_all)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_all)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        probas_pet_ = np.array(probas_pet_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_pet_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        probas_ct_ = np.array(probas_ct_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_ct_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
    
    # draw feature importances?    
    if do_draw_feature_importances:
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
    # draw confusion matrix 
    # for hotspot classifiers
    x = np.zeros_like(cnfs[0])
    x_lin = np.zeros_like(cnfs_lin[0])
    x_rbf = np.zeros_like(cnfs_rbf[0])
    x_poly = np.zeros_like(cnfs_poly[0])
    x_rf = np.zeros_like(cnfs_rf[0])
    # for psa classifiers
    print ("TEST PSA CONF")
    cnfs_psa_all = np.array(cnfs_psa_all)
    cnfs_psa_pet = np.array(cnfs_psa_pet)
    cnfs_psa_ct = np.array(cnfs_psa_ct)
    cnfs_psa_best = np.array(cnfs_psa_best)
    cnfs_lin = np.array(cnfs_lin)
    print (cnfs_lin.shape)
    print (cnfs_psa_all)
    print (cnfs_psa_pet.shape)
    print (cnfs_psa_ct.shape)
    print (cnfs_psa_best.shape)
    
    if do_psa_classification:
        xx_all = np.zeros_like(cnfs_psa_all[0])
        xx_pet = np.zeros_like(cnfs_psa_pet[0])
        xx_ct = np.zeros_like(cnfs_psa_ct[0])
        xx_best = np.zeros_like(cnfs_psa_best[0])
    for i in pid_range:
        x += cnfs[i]
        x_lin += cnfs_lin[i]
        x_rbf += cnfs_rbf[i]
        x_poly += cnfs_poly[i]
        x_rf += cnfs_rf[i]
        if do_psa_classification:
            xx_all += cnfs_psa_all[i]
            xx_pet += cnfs_psa_pet[i]
            xx_ct += cnfs_psa_ct[i]
            xx_best += cnfs_psa_best[i]

    # calclate sensitivity/specificity
    
    # for hotspot classifiers
    # Extea trees
    print ("Sensitivity/Specificity")
    fpr = float(x[0][1]/(x[0][1] + x[0][0]))
    tpr = float(x[1][1]/(x[1][1] + x[1][0]))
    tnr = float(x[0][0]/(x[0][0] + x[0][1]))
    fnr = float(x[1][0]/(x[1][0] + x[1][1]))
    print ("Extra Trees: ",tpr, tnr)       
    
    # linear SVC
    fpr = float(x_lin[0][1]/(x_lin[0][1] + x_lin[0][0]))
    tpr = float(x_lin[1][1]/(x_lin[1][1] + x_lin[1][0]))
    tnr = float(x_lin[0][0]/(x_lin[0][0] + x_lin[0][1]))
    fnr = float(x_lin[1][0]/(x_lin[1][0] + x_lin[1][1]))
    print ("Linear SVC: ",tpr, tnr) 

    # rbf
    fpr = float(x_rbf[0][1]/(x_rbf[0][1] + x_rbf[0][0]))
    tpr = float(x_rbf[1][1]/(x_rbf[1][1] + x_rbf[1][0]))
    tnr = float(x_rbf[0][0]/(x_rbf[0][0] + x_rbf[0][1]))
    fnr = float(x_rbf[1][0]/(x_rbf[1][0] + x_rbf[1][1]))
    print ("RBF SVC: ",tpr, tnr)             
    
    # poly
    fpr = float(x_poly[0][1]/(x_poly[0][1] + x_poly[0][0]))
    tpr = float(x_poly[1][1]/(x_poly[1][1] + x_poly[1][0]))
    tnr = float(x_poly[0][0]/(x_poly[0][0] + x_poly[0][1]))
    fnr = float(x_poly[1][0]/(x_poly[1][0] + x_poly[1][1]))
    print ("Poly SVC: ",tpr, tnr)       
    
    # rf
    fpr = float(x_rf[0][1]/(x_rf[0][1] + x_rf[0][0]))
    tpr = float(x_rf[1][1]/(x_rf[1][1] + x_rf[1][0]))
    tnr = float(x_rf[0][0]/(x_rf[0][0] + x_rf[0][1]))
    fnr = float(x_rf[1][0]/(x_rf[1][0] + x_rf[1][1]))
    print ("Random Forest: ",tpr, tnr)       
    
    
    if do_psa_classification:
        # for psa classifiers 
        print ("Sensitivity/Specificity for PSA analysis")
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        
        # for all
        fpr = float(xx_all[0][1]/(xx_all[0][1] + xx_all[0][0]))
        tpr = float(xx_all[1][1]/(xx_all[1][1] + xx_all[1][0]))
        tnr = float(xx_all[0][0]/(xx_all[0][0] + xx_all[0][1]))
        fnr = float(xx_all[1][0]/(xx_all[1][0] + xx_all[1][1]))
        print ("%s for PET/CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for pet
        fpr = float(xx_pet[0][1]/(xx_pet[0][1] + xx_pet[0][0]))
        tpr = float(xx_pet[1][1]/(xx_pet[1][1] + xx_pet[1][0]))
        tnr = float(xx_pet[0][0]/(xx_pet[0][0] + xx_pet[0][1]))
        fnr = float(xx_pet[1][0]/(xx_pet[1][0] + xx_pet[1][1]))
        print ("%s for PET: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for ct
        fpr = float(xx_ct[0][1]/(xx_ct[0][1] + xx_ct[0][0]))
        tpr = float(xx_ct[1][1]/(xx_ct[1][1] + xx_ct[1][0]))
        tnr = float(xx_ct[0][0]/(xx_ct[0][0] + xx_ct[0][1]))
        fnr = float(xx_ct[1][0]/(xx_ct[1][0] + xx_ct[1][1]))
        print ("%s for CT: %f, %f" % (clf_txt, tpr, tnr))       
        
        # for best
        fpr = float(xx_best[0][1]/(xx_best[0][1] + xx_best[0][0]))
        tpr = float(xx_best[1][1]/(xx_best[1][1] + xx_best[1][0]))
        tnr = float(xx_best[0][0]/(xx_best[0][0] + xx_best[0][1]))
        fnr = float(xx_best[1][0]/(xx_best[1][0] + xx_best[1][1]))
        print ("%s for BEST: %f, %f" % (clf_txt, tpr, tnr))       
        
    class_names = ["physiological", "pathological" ]
    class_names_psa = ["non_responder", "responder" ]
    plot_confusion_matrix(x, classes=class_names, normalize=False,
                              title='Confusion matrix')        
    plt.show()
    
#    plot_confusion_matrix(x_lin, classes=class_names, normalize=False,
#                              title='Confusion matrix')        
#    plt.show()
    
    if do_psa_classification:
        
        plot_confusion_matrix(xx_all, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_pet, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_ct, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_best, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
    
    # regressoion scores
    if do_regresson:
        # RRRRRRRRR
        y_true_etr = np.array(y_true_etr)    
        mean_y_true = np.mean(y_true_etr, axis=0)
        y_pred_etr = np.array(y_pred_etr)
        y_pred_rbf = np.array(y_pred_rbf)
        y_pred_rfr = np.array(y_pred_rfr)
        y_pred_poly = np.array(y_pred_poly)
        y_pred_lin = np.array(y_pred_lin)
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("ETR")
        print (np.mean(etr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))        
          
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RBF")
        print (np.mean(rbfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("POLY")
        print (np.mean(polyr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("LIN")
        print (np.mean(lnr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RFR")
        print (np.mean(rfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        
######################################
# start leave three subjects out 
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_three_subjects:
    cv_steps = 3
    # init subject based variables
    n_subjects = 72    
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    ##################
    # appending all  #
    ##################
    probas_best = []
    probas_all = []
    probas_pet_ = []
    probas_ct_ = []
    y_true_psa_ = []
    
    for i in range(0,30,cv_steps):
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) not in range(i, i+cv_steps):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
            print ("TEST")
            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
#                if pid != i:
                if pid not in range(i, i+cv_steps):
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MaxAbsScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)

            scalerObj = preprocessing.MaxAbsScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid not in range(i, i+cv_steps):
                        train_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)

                scalerObj = preprocessing.MaxAbsScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
            c_rbf = 2**15
            gamma_rbf = 0.1
#            c_rbf = 2**-5
#            gamma_rbf = 0.00048828125
            c_lin = 2**15
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=3, C=1).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (probas_pet)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
            c_rbf = 2**-5
            gamma_rbf = 0.00048828125
            c_lin = 2**-5
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (probas_ct)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
#            c_rbf = 2
#            gamma_rbf = 0.00048828125
            c_rbf = 2**15
            gamma_rbf = 2**-15                                  
            
            c_lin = 2**15            
            if classifier == 0:
                # linear svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (probas_)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 0.007812
                c_lin = 2**-5
                if classifier == 0:
                    # linear svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=3, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                        
                print ("POBAS_TEST")
                print (probas_)
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (probas_)
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("Y_PSA_TEST")
            print (y_test_psa)
            y_true_psa.append(y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
    
    for i in range(0,30,cv_steps):
        if do_psa_classification:
            
            i = int(i/cv_steps)
            
            for ii in range(0, cv_steps):
                print (poly_probas[i])
                probas_best.append(poly_probas[i][ii])
                probas_all.append(poly_probas_all[i][ii])
                probas_pet_.append(poly_probas_pet[i][ii])
                probas_ct_.append(poly_probas_ct[i][ii])
                y_true_psa_.append(y_true_psa[i][ii])
            
    print ("THE GREAT TEST!!!")
    print (probas_all)
    print (probas_pet_)
    print (probas_ct_)
    print (probas_best)
            
    if do_psa_classification:            
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            probas_best = np.array(probas_best)
            y_true_psa_ = np.array(y_true_psa_)
            print (y_true_psa_.shape, probas_best.shape)
            fpr, tpr, _ = roc_curve(y_true_psa_, probas_best)
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        
        
        
        # poly svc classifier all
        probas_all = np.array(probas_all)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_all)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        probas_pet_ = np.array(probas_pet_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_pet_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        probas_ct_ = np.array(probas_ct_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_ct_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
    
    # draw feature importances?    
    if do_draw_feature_importances:
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
    # draw confusion matrix 
    # for hotspot classifiers
    x = np.zeros_like(cnfs[0])
    x_lin = np.zeros_like(cnfs_lin[0])
    x_rbf = np.zeros_like(cnfs_rbf[0])
    x_poly = np.zeros_like(cnfs_poly[0])
    x_rf = np.zeros_like(cnfs_rf[0])
    # for psa classifiers
    print ("TEST PSA CONF")
    cnfs_psa_all = np.array(cnfs_psa_all)
    cnfs_psa_pet = np.array(cnfs_psa_pet)
    cnfs_psa_ct = np.array(cnfs_psa_ct)
    cnfs_psa_best = np.array(cnfs_psa_best)
    cnfs_lin = np.array(cnfs_lin)
    print (cnfs_lin.shape)
    print (cnfs_psa_all)
    print (cnfs_psa_pet.shape)
    print (cnfs_psa_ct.shape)
    print (cnfs_psa_best.shape)
    
#    if do_psa_classification:
#        xx_all = np.zeros_like(cnfs_psa_all[0])
#        xx_pet = np.zeros_like(cnfs_psa_pet[0])
#        xx_ct = np.zeros_like(cnfs_psa_ct[0])
#        xx_best = np.zeros_like(cnfs_psa_best[0])
    for i in pid_range:
        x += cnfs[i]
        x_lin += cnfs_lin[i]
        x_rbf += cnfs_rbf[i]
        x_poly += cnfs_poly[i]
        x_rf += cnfs_rf[i]
#        if do_psa_classification:
#            xx_all += cnfs_psa_all[i]
#            xx_pet += cnfs_psa_pet[i]
#            xx_ct += cnfs_psa_ct[i]
#            xx_best += cnfs_psa_best[i]

    # calclate sensitivity/specificity
    
    # for hotspot classifiers
    # Extea trees
    print ("Sensitivity/Specificity")
    fpr = float(x[0][1]/(x[0][1] + x[0][0]))
    tpr = float(x[1][1]/(x[1][1] + x[1][0]))
    tnr = float(x[0][0]/(x[0][0] + x[0][1]))
    fnr = float(x[1][0]/(x[1][0] + x[1][1]))
    print ("Extra Trees: ",tpr, tnr)       
    
    # linear SVC
    fpr = float(x_lin[0][1]/(x_lin[0][1] + x_lin[0][0]))
    tpr = float(x_lin[1][1]/(x_lin[1][1] + x_lin[1][0]))
    tnr = float(x_lin[0][0]/(x_lin[0][0] + x_lin[0][1]))
    fnr = float(x_lin[1][0]/(x_lin[1][0] + x_lin[1][1]))
    print ("Linear SVC: ",tpr, tnr) 

    # rbf
    fpr = float(x_rbf[0][1]/(x_rbf[0][1] + x_rbf[0][0]))
    tpr = float(x_rbf[1][1]/(x_rbf[1][1] + x_rbf[1][0]))
    tnr = float(x_rbf[0][0]/(x_rbf[0][0] + x_rbf[0][1]))
    fnr = float(x_rbf[1][0]/(x_rbf[1][0] + x_rbf[1][1]))
    print ("RBF SVC: ",tpr, tnr)             
    
    # poly
    fpr = float(x_poly[0][1]/(x_poly[0][1] + x_poly[0][0]))
    tpr = float(x_poly[1][1]/(x_poly[1][1] + x_poly[1][0]))
    tnr = float(x_poly[0][0]/(x_poly[0][0] + x_poly[0][1]))
    fnr = float(x_poly[1][0]/(x_poly[1][0] + x_poly[1][1]))
    print ("Poly SVC: ",tpr, tnr)       
    
    # rf
    fpr = float(x_rf[0][1]/(x_rf[0][1] + x_rf[0][0]))
    tpr = float(x_rf[1][1]/(x_rf[1][1] + x_rf[1][0]))
    tnr = float(x_rf[0][0]/(x_rf[0][0] + x_rf[0][1]))
    fnr = float(x_rf[1][0]/(x_rf[1][0] + x_rf[1][1]))
    print ("Random Forest: ",tpr, tnr)       
    
    
#    if do_psa_classification:
#        # for psa classifiers 
#        print ("Sensitivity/Specificity for PSA analysis")
#        clf_txt = "RBF SVC"
#        if classifier == 1:
#            clf_txt = "Poly SVC"
#        elif classifier == 2:
#            clf_txt = "Extra Trees"
#        elif classifier == 3:
#            clf_txt = "Linear SVC"
#        
#        # for all
#        fpr = float(xx_all[0][1]/(xx_all[0][1] + xx_all[0][0]))
#        tpr = float(xx_all[1][1]/(xx_all[1][1] + xx_all[1][0]))
#        tnr = float(xx_all[0][0]/(xx_all[0][0] + xx_all[0][1]))
#        fnr = float(xx_all[1][0]/(xx_all[1][0] + xx_all[1][1]))
#        print ("%s for PET/CT: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for pet
#        fpr = float(xx_pet[0][1]/(xx_pet[0][1] + xx_pet[0][0]))
#        tpr = float(xx_pet[1][1]/(xx_pet[1][1] + xx_pet[1][0]))
#        tnr = float(xx_pet[0][0]/(xx_pet[0][0] + xx_pet[0][1]))
#        fnr = float(xx_pet[1][0]/(xx_pet[1][0] + xx_pet[1][1]))
#        print ("%s for PET: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for ct
#        fpr = float(xx_ct[0][1]/(xx_ct[0][1] + xx_ct[0][0]))
#        tpr = float(xx_ct[1][1]/(xx_ct[1][1] + xx_ct[1][0]))
#        tnr = float(xx_ct[0][0]/(xx_ct[0][0] + xx_ct[0][1]))
#        fnr = float(xx_ct[1][0]/(xx_ct[1][0] + xx_ct[1][1]))
#        print ("%s for CT: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for best
#        fpr = float(xx_best[0][1]/(xx_best[0][1] + xx_best[0][0]))
#        tpr = float(xx_best[1][1]/(xx_best[1][1] + xx_best[1][0]))
#        tnr = float(xx_best[0][0]/(xx_best[0][0] + xx_best[0][1]))
#        fnr = float(xx_best[1][0]/(xx_best[1][0] + xx_best[1][1]))
#        print ("%s for BEST: %f, %f" % (clf_txt, tpr, tnr))       
#        
    class_names = ["physiological", "pathological" ]
    class_names_psa = ["non_responder", "responder" ]
#    plot_confusion_matrix(x, classes=class_names, normalize=False,
#                              title='Confusion matrix')        
#    plt.show()
    
    plot_confusion_matrix(x_lin, classes=class_names, normalize=False,
                              title='Confusion matrix')        
    plt.show()
    
    if do_psa_classification:
        
        plot_confusion_matrix(xx_all, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_pet, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_ct, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
        
        plot_confusion_matrix(xx_best, classes=class_names_psa, normalize=False,
                                  title='Confusion matrix')        
        plt.show()
    
    # regressoion scores
    if do_regresson:
        # RRRRRRRRR
        y_true_etr = np.array(y_true_etr)    
        mean_y_true = np.mean(y_true_etr, axis=0)
        y_pred_etr = np.array(y_pred_etr)
        y_pred_rbf = np.array(y_pred_rbf)
        y_pred_rfr = np.array(y_pred_rfr)
        y_pred_poly = np.array(y_pred_poly)
        y_pred_lin = np.array(y_pred_lin)
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("ETR")
        print (np.mean(etr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))        
          
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RBF")
        print (np.mean(rbfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("POLY")
        print (np.mean(polyr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("LIN")
        print (np.mean(lnr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in pid_range:
            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RFR")
        print (np.mean(rfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        


######################################
# start leave two subjects out 
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_two_subjects:
    cv_steps = 2
    # init subject based variables
    n_subjects = 72    
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    ##################
    # appending all  #
    ##################
    probas_best = []
    probas_all = []
    probas_pet_ = []
    probas_ct_ = []
    y_true_psa_ = []
    
    for i in range(0,30,cv_steps):
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) not in range(i, i+cv_steps):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
            print ("TEST")
            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
#                if pid != i:
                if pid not in range(i, i+cv_steps):
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)

            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid not in range(i, i+cv_steps):
                        train_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)

                scalerObj = preprocessing.StandardScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
            c_rbf = 2**15 # 2**15
            gamma_rbf = 2**-9 # 2**-9 for tex: 2**-11
#            c_rbf = 2**-5
#            gamma_rbf = 0.00048828125
            c_lin = 2**15
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=3, C=1).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (probas_pet)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
            c_rbf = 2**-5 # (unbalanced 70%)
            gamma_rbf = 0.00048828125 # (unbalanced)
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (probas_ct)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
#            c_rbf = 2
#            gamma_rbf = 0.00048828125
            c_rbf = 2**13 # 2**13 none
            gamma_rbf = 2**-13 # 2**-13
            
            c_lin = 2**15            
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (probas_)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 2**-9 # 2**-9
                c_lin = 2**-5
                if classifier == 0:
                    # rbf svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=3, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                        
                print ("POBAS_TEST")
                print (probas_)
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (probas_)
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("Y_PSA_TEST")
            print (y_test_psa)
            y_true_psa.append(y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid not in range(i, i+cv_steps):
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
    
    for i in range(0,30,cv_steps):
        if do_psa_classification:
            
            i = int(i/cv_steps)
            
            for ii in range(0, cv_steps):
                print (poly_probas[i])
                probas_best.append(poly_probas[i][ii])
                probas_all.append(poly_probas_all[i][ii])
                probas_pet_.append(poly_probas_pet[i][ii])
                probas_ct_.append(poly_probas_ct[i][ii])
                y_true_psa_.append(y_true_psa[i][ii])
            
    print ("THE GREAT TEST!!!")
    print (probas_all)
    print (probas_pet_)
    print (probas_ct_)
    print (probas_best)
            
    if do_psa_classification:            
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            probas_best = np.array(probas_best)
            y_true_psa_ = np.array(y_true_psa_)
            print (y_true_psa_.shape, probas_best.shape)
            fpr, tpr, _ = roc_curve(y_true_psa_, probas_best)
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        
        
        
        # poly svc classifier all
        probas_all = np.array(probas_all)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_all)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        probas_pet_ = np.array(probas_pet_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_pet_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        probas_ct_ = np.array(probas_ct_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_ct_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
    
    # draw feature importances?    
    if do_draw_feature_importances:
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
        
#    # draw confusion matrix 
#    # for hotspot classifiers
#    x = np.zeros_like(cnfs[0])
#    x_lin = np.zeros_like(cnfs_lin[0])
#    x_rbf = np.zeros_like(cnfs_rbf[0])
#    x_poly = np.zeros_like(cnfs_poly[0])
#    x_rf = np.zeros_like(cnfs_rf[0])
#    # for psa classifiers
#    print ("TEST PSA CONF")
#    cnfs_psa_all = np.array(cnfs_psa_all)
#    cnfs_psa_pet = np.array(cnfs_psa_pet)
#    cnfs_psa_ct = np.array(cnfs_psa_ct)
#    cnfs_psa_best = np.array(cnfs_psa_best)
#    cnfs_lin = np.array(cnfs_lin)
#    print (cnfs_lin.shape)
#    print (cnfs_psa_all)
#    print (cnfs_psa_pet.shape)
#    print (cnfs_psa_ct.shape)
#    print (cnfs_psa_best.shape)
#    
#    if do_psa_classification:
#        xx_all = np.zeros_like(cnfs_psa_all[0])
#        xx_pet = np.zeros_like(cnfs_psa_pet[0])
#        xx_ct = np.zeros_like(cnfs_psa_ct[0])
#        xx_best = np.zeros_like(cnfs_psa_best[0])
#    for i in pid_range:
#        x += cnfs[i]
#        x_lin += cnfs_lin[i]
#        x_rbf += cnfs_rbf[i]
#        x_poly += cnfs_poly[i]
#        x_rf += cnfs_rf[i]
#        if do_psa_classification:
#            xx_all += cnfs_psa_all[i]
#            xx_pet += cnfs_psa_pet[i]
#            xx_ct += cnfs_psa_ct[i]
#            xx_best += cnfs_psa_best[i]
#
#    # calclate sensitivity/specificity
#    
#    # for hotspot classifiers
#    # Extea trees
#    print ("Sensitivity/Specificity")
#    fpr = float(x[0][1]/(x[0][1] + x[0][0]))
#    tpr = float(x[1][1]/(x[1][1] + x[1][0]))
#    tnr = float(x[0][0]/(x[0][0] + x[0][1]))
#    fnr = float(x[1][0]/(x[1][0] + x[1][1]))
#    print ("Extra Trees: ",tpr, tnr)       
#    
#    # linear SVC
#    fpr = float(x_lin[0][1]/(x_lin[0][1] + x_lin[0][0]))
#    tpr = float(x_lin[1][1]/(x_lin[1][1] + x_lin[1][0]))
#    tnr = float(x_lin[0][0]/(x_lin[0][0] + x_lin[0][1]))
#    fnr = float(x_lin[1][0]/(x_lin[1][0] + x_lin[1][1]))
#    print ("Linear SVC: ",tpr, tnr) 
#
#    # rbf
#    fpr = float(x_rbf[0][1]/(x_rbf[0][1] + x_rbf[0][0]))
#    tpr = float(x_rbf[1][1]/(x_rbf[1][1] + x_rbf[1][0]))
#    tnr = float(x_rbf[0][0]/(x_rbf[0][0] + x_rbf[0][1]))
#    fnr = float(x_rbf[1][0]/(x_rbf[1][0] + x_rbf[1][1]))
#    print ("RBF SVC: ",tpr, tnr)             
#    
#    # poly
#    fpr = float(x_poly[0][1]/(x_poly[0][1] + x_poly[0][0]))
#    tpr = float(x_poly[1][1]/(x_poly[1][1] + x_poly[1][0]))
#    tnr = float(x_poly[0][0]/(x_poly[0][0] + x_poly[0][1]))
#    fnr = float(x_poly[1][0]/(x_poly[1][0] + x_poly[1][1]))
#    print ("Poly SVC: ",tpr, tnr)       
#    
#    # rf
#    fpr = float(x_rf[0][1]/(x_rf[0][1] + x_rf[0][0]))
#    tpr = float(x_rf[1][1]/(x_rf[1][1] + x_rf[1][0]))
#    tnr = float(x_rf[0][0]/(x_rf[0][0] + x_rf[0][1]))
#    fnr = float(x_rf[1][0]/(x_rf[1][0] + x_rf[1][1]))
#    print ("Random Forest: ",tpr, tnr)       
#    
#    
#    if do_psa_classification:
#        # for psa classifiers 
#        print ("Sensitivity/Specificity for PSA analysis")
#        clf_txt = "RBF SVC"
#        if classifier == 1:
#            clf_txt = "Poly SVC"
#        elif classifier == 2:
#            clf_txt = "Extra Trees"
#        elif classifier == 3:
#            clf_txt = "Linear SVC"
#        
#        # for all
#        fpr = float(xx_all[0][1]/(xx_all[0][1] + xx_all[0][0]))
#        tpr = float(xx_all[1][1]/(xx_all[1][1] + xx_all[1][0]))
#        tnr = float(xx_all[0][0]/(xx_all[0][0] + xx_all[0][1]))
#        fnr = float(xx_all[1][0]/(xx_all[1][0] + xx_all[1][1]))
#        print ("%s for PET/CT: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for pet
#        fpr = float(xx_pet[0][1]/(xx_pet[0][1] + xx_pet[0][0]))
#        tpr = float(xx_pet[1][1]/(xx_pet[1][1] + xx_pet[1][0]))
#        tnr = float(xx_pet[0][0]/(xx_pet[0][0] + xx_pet[0][1]))
#        fnr = float(xx_pet[1][0]/(xx_pet[1][0] + xx_pet[1][1]))
#        print ("%s for PET: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for ct
#        fpr = float(xx_ct[0][1]/(xx_ct[0][1] + xx_ct[0][0]))
#        tpr = float(xx_ct[1][1]/(xx_ct[1][1] + xx_ct[1][0]))
#        tnr = float(xx_ct[0][0]/(xx_ct[0][0] + xx_ct[0][1]))
#        fnr = float(xx_ct[1][0]/(xx_ct[1][0] + xx_ct[1][1]))
#        print ("%s for CT: %f, %f" % (clf_txt, tpr, tnr))       
#        
#        # for best
#        fpr = float(xx_best[0][1]/(xx_best[0][1] + xx_best[0][0]))
#        tpr = float(xx_best[1][1]/(xx_best[1][1] + xx_best[1][0]))
#        tnr = float(xx_best[0][0]/(xx_best[0][0] + xx_best[0][1]))
#        fnr = float(xx_best[1][0]/(xx_best[1][0] + xx_best[1][1]))
#        print ("%s for BEST: %f, %f" % (clf_txt, tpr, tnr))       
#        
#    class_names = ["physiological", "pathological" ]
#    class_names_psa = ["non_responder", "responder" ]
#    plot_confusion_matrix(x, classes=class_names, normalize=False,
#                              title='Confusion matrix')        
#    plt.show()
#    
##    plot_confusion_matrix(x_lin, classes=class_names, normalize=False,
##                              title='Confusion matrix')        
##    plt.show()
#    
#    if do_psa_classification:
#        
#        plot_confusion_matrix(xx_all, classes=class_names_psa, normalize=False,
#                                  title='Confusion matrix')        
#        plt.show()
#        
#        plot_confusion_matrix(xx_pet, classes=class_names_psa, normalize=False,
#                                  title='Confusion matrix')        
#        plt.show()
#        
#        plot_confusion_matrix(xx_ct, classes=class_names_psa, normalize=False,
#                                  title='Confusion matrix')        
#        plt.show()
#        
#        plot_confusion_matrix(xx_best, classes=class_names_psa, normalize=False,
#                                  title='Confusion matrix')        
#        plt.show()
#    
#    # regressoion scores
#    if do_regresson:
#        # RRRRRRRRR
#        y_true_etr = np.array(y_true_etr)    
#        mean_y_true = np.mean(y_true_etr, axis=0)
#        y_pred_etr = np.array(y_pred_etr)
#        y_pred_rbf = np.array(y_pred_rbf)
#        y_pred_rfr = np.array(y_pred_rfr)
#        y_pred_poly = np.array(y_pred_poly)
#        y_pred_lin = np.array(y_pred_lin)
#        u = 0
#        v = 0
#        for i in pid_range:
#            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
#            v += float(pow((y_true_etr[i] - mean_y_true), 2))
#        print ("ETR")
#        print (np.mean(etr_scores, axis=0))
#        print ("u", u)
#        print ("v", v)
#        print ("u/v:", float(u/v))
#        print ("1 - u/v:", 1 - float(u/v))        
#          
#        u = 0
#        v = 0
#        for i in pid_range:
#            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
#            v += float(pow((y_true_etr[i] - mean_y_true), 2))
#        print ("RBF")
#        print (np.mean(rbfr_scores, axis=0))
#        print ("u", u)
#        print ("v", v)
#        print ("u/v:", float(u/v))
#        print ("1 - u/v:", 1 - float(u/v))
#        
#        u = 0
#        v = 0
#        for i in pid_range:
#            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
#            v += float(pow((y_true_etr[i] - mean_y_true), 2))
#        print ("POLY")
#        print (np.mean(polyr_scores, axis=0))
#        print ("u", u)
#        print ("v", v)
#        print ("u/v:", float(u/v))
#        print ("1 - u/v:", 1 - float(u/v))
#        
#        u = 0
#        v = 0
#        for i in pid_range:
#            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
#            v += float(pow((y_true_etr[i] - mean_y_true), 2))
#        print ("LIN")
#        print (np.mean(lnr_scores, axis=0))
#        print ("u", u)
#        print ("v", v)
#        print ("u/v:", float(u/v))
#        print ("1 - u/v:", 1 - float(u/v))
#        
#        u = 0
#        v = 0
#        for i in pid_range:
#            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
#            v += float(pow((y_true_etr[i] - mean_y_true), 2))
#        print ("RFR")
#        print (np.mean(rfr_scores, axis=0))
#        print ("u", u)
#        print ("v", v)
#        print ("u/v:", float(u/v))
#        print ("1 - u/v:", 1 - float(u/v))
        


######################################
# start leave one subject out NEW
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_loo_subjects_new:
    cv_steps = 1
    # init subject based variables
    n_subjects = 72
    # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    ##################
    # appending all  #
    ##################
    probas_best = []
    probas_all = []
    probas_pet_ = []
    probas_ct_ = []
    y_true_psa_ = []
    
    for i in range(0,n_subjects - 1,cv_steps):
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        train_delta_psa = []
        test_delta_psa = []
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) not in range(pid_map[i], pid_map[i+cv_steps]):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
#        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_lin.append(cnf)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rbf.append(cnf)
#        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_poly.append(cnf)
#        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
            
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Polynomial SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs.append(cnf)
        feature_importances_tree.append(trees.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        cnfs_rf.append(cnf)
        feature_importances_rf.append(forest.feature_importances_)        
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC Leave Subj %d Out (AUC = %0.2f)' % (i, roc_auc))
        
        if do_psa_classification: 
#            print ("TEST")
#            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
#                if pid != i:
                if pid not in range(pid_map[i], pid_map[i+cv_steps]):
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
#                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid not in range(pid_map[i], pid_map[i+cv_steps]):
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 

            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
#            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid not in range(pid_map[i], pid_map[i+cv_steps]):
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)

            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid not in range(pid_map[i], pid_map[i+cv_steps]):
                        train_psa.append([mean_fi_reg[pid_reverse_map[pid]][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid_reverse_map[pid]][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)

                scalerObj = preprocessing.StandardScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            

#            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
#            c_rbf = 2
#            gamma_rbf = 2**-13
#            
            c_rbf = 1000
            gamma_rbf = 0.001953125
            
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=3, C=1).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_pet.append(cnf)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION poly")
            print (probas_pet)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_pet.append(y_score_psa)            
            
            # for ct
#            c_rbf = 2**-5 
#            gamma_rbf = 2**-11
            c_rbf = 32 
            gamma_rbf = 0.5
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_ct.append(cnf)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION poly")
            print (probas_ct)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_ct.append(y_score_psa)

            # for all features
#            c_rbf = 2
#            gamma_rbf = 0.00048828125 or 2**-11
            c_rbf = 100
            gamma_rbf = 0.0001220703125
            
            c_lin = 2**-5            
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                cnfs_psa_all.append(cnf)
            
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION poly")
            print (probas_)
            print (y_pred_psa, y_test_psa, y_score_psa)
            scores_psa_poly_all.append(y_score_psa)

            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 2**-7 
                c_lin = 2**-5
                if classifier == 0:
                    # rbf svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf).fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=3, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    cnf = confusion_matrix(y_test_psa, y_pred, cnf_labels)
                    cnfs_psa_best.append(cnf)
                        
                print ("POBAS_TEST")
                print (probas_)
                poly_probas.append(probas_)
                y_pred_psa = clf.predict(test_psa)
                y_pred_psa_poly.append(y_pred_psa[0])
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION poly")
                print (probas_)
                print (y_pred_psa, y_test_psa, y_score_psa)
                scores_psa_poly.append(y_score_psa)
            
            
            
            # appending true psa label
            print ("Y_PSA_TEST")
            print (y_test_psa)
            y_true_psa.append(y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid not in range(pid_map[i], pid_map[i+cv_steps]):
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.StandardScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
#            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
#            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            feature_importances_tree_r.append(etr.feature_importances_)
            feature_importances_rf_r.append(rfr.feature_importances_)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            # append them to their vectors
            rbfr_scores.append(rbf_score)
            lnr_scores.append(lin_score)
            polyr_scores.append(poly_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            
            print ("REGRESSION", i)
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            y_pred_etr.append(y_etr)
            y_pred_poly.append(y_poly)
            y_pred_rfr.append(y_rfr)
            y_pred_lin.append(y_lin)
            y_pred_rbf.append(y_rbf)
            y_true_etr.append(y_test[0])
    
    # rocs
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='y',
             label=r'Mean ROC: Linear SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    lin_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr = np.mean(mean_fpr, axis=0)
    lin_tnr = 1 - mean_fpr
    
    # rbf svc classifier
    mean_tpr = np.mean(tprs_rbf, axis=0)
    mean_auc = auc(mean_fpr_rbf, mean_tpr)
    std_auc = np.std(aucs_rbf)
    plt.plot(mean_fpr_rbf, mean_tpr, color='r',
             label=r'Mean ROC: RBF SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)
    rbf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rbf = np.mean(mean_fpr_rbf, axis=0) 
    rbf_tnr = 1 - mean_fpr_rbf
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='b',
             label=r'Mean ROC: Polynomial SVC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashed', lw=2, alpha=.8)
    poly_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_poly = np.mean(mean_fpr_poly, axis=0)
    poly_tnr = 1 - mean_fpr_poly
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dashdot', lw=2, alpha=.8)
    tree_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_tree = np.mean(mean_fpr_tree, axis=0)
    tree_tnr = 1 - mean_fpr_tree
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             ls='dotted', lw=2, alpha=.8)   
    rf_tpr = np.mean(mean_tpr, axis=0)
    mean_fpr_rf = np.mean(mean_fpr_rf, axis=0)
    rf_tnr = 1 - mean_fpr_rf
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()   
    
    
    for i in range(0,n_subjects - 1, cv_steps):
        if do_psa_classification:
            
            i = int(i/cv_steps)
            
            for ii in range(0, cv_steps):
                print (poly_probas[i])
                probas_best.append(poly_probas[i][ii])
                probas_all.append(poly_probas_all[i][ii])
                probas_pet_.append(poly_probas_pet[i][ii])
                probas_ct_.append(poly_probas_ct[i][ii])
                y_true_psa_.append(y_true_psa[i][ii])
            
    print ("THE GREAT TEST!!!")
    print (probas_all)
    print (probas_pet_)
    print (probas_ct_)
    print (probas_best)
            
    if do_psa_classification:            
        # psa rocs
        # poly svc classifier best
        clf_txt = "RBF SVC"
        if classifier == 1:
            clf_txt = "Poly SVC"
        elif classifier == 2:
            clf_txt = "Extra Trees"
        elif classifier == 3:
            clf_txt = "Linear SVC"
        if do_regresson:
            probas_best = np.array(probas_best)
            y_true_psa_ = np.array(y_true_psa_)
            print (y_true_psa_.shape, probas_best.shape)
            fpr, tpr, _ = roc_curve(y_true_psa_, probas_best)
            psa_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, color='cyan',
                     label=r'ROC: %s, best features (AUC = %0.2f)' % (clf_txt, psa_auc),
                     lw=2, alpha=.8)
        
        
        
        
        # poly svc classifier all
        probas_all = np.array(probas_all)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_all)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='g',
                 label=r'ROC: %s, all features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashed', lw=2, alpha=.8)
        
        # poly svc classifier pet
        probas_pet_ = np.array(probas_pet_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_pet_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='b',
                 label=r'ROC: %s, PET features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dashdot', lw=2, alpha=.8)
        
        # poly svc classifier ct
        probas_ct_ = np.array(probas_ct_)
        y_true_psa_ = np.array(y_true_psa_)
        fpr, tpr, _ = roc_curve(y_true_psa_, probas_ct_)
        psa_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, color='r',
                 label=r'ROC: %s, CT features (AUC = %0.2f)' % (clf_txt, psa_auc),
                 ls='dotted', lw=2, alpha=.8)
        
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
    
    # draw feature importances?    
    if do_draw_feature_importances:
        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")
    
    
    # psa regressoion scores
    if do_regresson:
        # RRRRRRRRR
        y_true_etr = np.array(y_true_etr)    
        mean_y_true = np.mean(y_true_etr, axis=0)
        y_pred_etr = np.array(y_pred_etr)
        y_pred_rbf = np.array(y_pred_rbf)
        y_pred_rfr = np.array(y_pred_rfr)
        y_pred_poly = np.array(y_pred_poly)
        y_pred_lin = np.array(y_pred_lin)
        u = 0
        v = 0
        for i in range(len(pid_range) - 1):
            u += float(pow((y_true_etr[i] - y_pred_etr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("ETR")
        print (np.mean(etr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))        
          
        u = 0
        v = 0
        for i in range(len(pid_range) - 1):
            u += float(pow((y_true_etr[i] - y_pred_rbf[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RBF")
        print (np.mean(rbfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in range(len(pid_range) - 1):
            u += float(pow((y_true_etr[i] - y_pred_poly[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("POLY")
        print (np.mean(polyr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in range(len(pid_range) - 1):
            u += float(pow((y_true_etr[i] - y_pred_lin[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("LIN")
        print (np.mean(lnr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        
        u = 0
        v = 0
        for i in range(len(pid_range) - 1):
            u += float(pow((y_true_etr[i] - y_pred_rfr[i]), 2))
            v += float(pow((y_true_etr[i] - mean_y_true), 2))
        print ("RFR")
        print (np.mean(rfr_scores, axis=0))
        print ("u", u)
        print ("v", v)
        print ("u/v:", float(u/v))
        print ("1 - u/v:", 1 - float(u/v))
        

######################################
# start analyses for dgn 2020 abstract
######################################    
# init feature importance matrices
feature_importances_lsvc = []
feature_importances_tree = []
feature_importances_rf = []
feature_importances_tree_r = []
feature_importances_rf_r = []
y_true_etr = []
y_pred_etr = []
y_pred_poly = []
y_pred_rfr = []
y_pred_lin = []
y_pred_rbf = []
y_pred_psa_poly = []
bag_probas_all = []
y_pred_psa_bag_all = []
scores_psa_bag_all = []
y_pred_psa_poly_all = []
y_pred_psa_poly_pet = []
y_pred_psa_poly_ct = []
y_pred_psa_trees = []
y_true_psa = []
if do_dgn2020:
    cv_steps = 1
    # init subject based variables
    if balanced:
        n_subjects = 48 # 28
    else:
        n_subjects = 83 # 72
    training_ranges = []
    
#    training_ranges.append(pid_range)
    # NON-RESPONDERS: 0,19,20,23,24,25,37,42,52,53,60,61,63
    # RESPONDERS:     1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49,50,51,54,55,56,57,58,59,62,64,65,66,67,68,69,70,71
    

    if not balanced:
        training_ranges.append([0,19,20,23,24,25,37,42,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45])
        training_ranges.append([19,20,23,24,25,37,42,52,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49])
        training_ranges.append([20,23,24,25,37,42,52,53,10,11,12,13,14,15,16,17,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49,50,51,54,55])
        training_ranges.append([23,24,25,37,42,52,53,60,14,15,16,17,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49,50,51,54,55,56,57,58,59])
        training_ranges.append([24,25,37,42,52,53,60,61,18,21,22,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49,50,51,54,55,56,57,58,59,62,64,65,66])
        training_ranges.append([25,37,42,52,53,60,61,63,27,28,29,30,31,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,49,50,51,54,55,56,57,58,59,62,64,65,66,67,68,69,70,71])
        
    
    
#    # alternative new ids for the balanced case:
#    # NON-RESPONDERS: 17,18,19,20,21,22,23,24,25,26,27,28,29
#    # RESPONDERS:     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
#    
#    if balanced:
#        training_ranges.append([0,1,2,3,4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24])
#        training_ranges.append([1,2,3,4,5,6,7,8,9,10,11,12,18,19,20,21,22,23,24,25])
#        training_ranges.append([2,3,4,5,6,7,8,9,10,11,12,13,19,20,21,22,23,24,25,26])
#        training_ranges.append([3,4,5,6,7,8,9,10,11,12,13,14,20,21,22,23,24,25,26,27])
#        training_ranges.append([4,5,6,7,8,9,10,11,12,13,14,15,21,22,23,24,25,26,27,28])
#        training_ranges.append([5,6,7,8,9,10,11,12,13,14,15,16,22,23,24,25,26,27,28,29])
        
    # alternative new ids for the balanced case with clinical and radiomics parameters:
    # RESPONDERS:         13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
    # NON-RESPONDERS:     0,1,2,3,4,5,6,7,8,9,10,11,12
    
    if balanced:
        training_ranges.append([0,1,2,3,4,5,6,7,13,14,15,16,17,18,19,20,21,22])
        training_ranges.append([1,2,3,4,5,6,7,8,14,15,16,17,18,19,20,21,22,23])
        training_ranges.append([2,3,4,5,6,7,8,9,15,16,17,18,19,20,21,22,23,24])
        training_ranges.append([3,4,5,6,7,8,9,10,16,17,18,19,20,21,22,23,24,25])
        training_ranges.append([4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26])
        training_ranges.append([5,6,7,8,9,10,11,12,18,19,20,21,22,23,24,25,26,27])

                       # for psa regression
    etr_scores = []
    rfr_scores = []
    lnr_scores = []
    rbfr_scores = []
    polyr_scores = []
    # for confusion matrices
    # lin svc
    cnfs_lin = []
    # rbf
    cnfs_rbf = []
    # poly
    cnfs_poly = []
    # et
    cnfs = []    
    # rf
    cnfs_rf = []
    # psa
    cnf_labels = [0,1]
    cnfs_psa_all = []
    cnfs_psa_pet = []
    cnfs_psa_ct = []
    cnfs_psa_best = []
    
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    scores_psa_lsvc = []
    lsvc_probas = []
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    scores_psa_rbf = []
    rbf_probas = []
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    scores_psa_poly = []
    scores_psa_poly_all = []
    scores_psa_poly_pet = []
    scores_psa_poly_ct = []
    poly_probas = []
    poly_probas_all = []
    poly_probas_pet = []
    poly_probas_ct = []
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    etr_scores = []
    scores_psa = []
    tprs_tree_psa = []
    aucs_tree_psa = []
    mean_fpr_tree_psa = np.linspace(0, 1, 100)
    trees_probas = []
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100) 
    rfr_scores = []
    scores_psa_rf = []
    rf_probas = []
    
    ##################
    # appending all  #
    ##################
    probas_best = []
    probas_all = []
    probas_pet_ = []
    probas_ct_ = []
    y_true_psa_ = []
    
      
    # init data containers
    train_data = []
    test_data = []
    labels = []
    test_labels = []
    train_delta_psa = []
    test_delta_psa = []
    # for the results
    pet_scores = []
    ct_scores = []
    pet_ct_scores = []
    best_features_scores = []
    # for regression results
    etr_scores = []
    rfr_scores = []
    lin_scores = []
    poly_scores = []
    rbf_scores = []
    # import data
    for training_subj_range in (training_ranges):
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if int(row['patient_id']) in training_subj_range:
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['label']))
                    train_delta_psa.append(float(row['delta_psa']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['label']))
                    test_delta_psa.append(float(row['delta_psa']))
    
        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
    #        X_train = scale(X_train)
    
        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        # normalize test data
    #        X_test = np.subtract(X_test, mean)
    #        X_test = np.divide(X_test, std * std)
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)   
        
        # start ploting roc curves
        
        # linear svc classifier
        clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        y_pred = clf.predict(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        roc_auc = auc(fpr, tpr)
            
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
             label='Linear SVC, ROC (AUC = %0.2f)' % (roc_auc))
            
        # rbf svc classifier
        rbf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(X_train, y_train)
        probas_ = rbf.decision_function(X_test)
        y_pred = rbf.predict(X_test)
             # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        roc_auc = auc(fpr, tpr)
            
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
             label='RBF SVC, ROC (AUC = %0.2f)' % (roc_auc))
        
        # polynomial svc classifier        
        poly = SVC(kernel='poly', degree=2, C=1.0).fit(X_train, y_train)
        probas_ = poly.decision_function(X_test)
        y_pred = poly.predict(X_test)
    #        feature_importances_psvc.append(poly.feature_importances_)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        roc_auc = auc(fpr, tpr)
            
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
             label='Polynomial SVC, ROC AUC = %0.2f)' % (roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        y_pred = trees.predict(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        roc_auc = auc(fpr, tpr)
    
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
             label='Extra Trees, ROC (AUC = %0.2f)' % (roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(max_depth=dep_dec_rf, min_samples_leaf=1, n_estimators=est_dec_rf, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        y_pred = forest.predict(X_test)
        
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        roc_auc = auc(fpr, tpr)
    
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
             label='Random Forest, ROC (AUC = %0.2f)' % (roc_auc))
        
        plt.plot([0, 1], [0, 1], 'k--', lw=1)
        plot_roc_legend()
        
        if do_psa_classification: 
            print ("TEST")
#            print (mean_fi.shape)
            
            
            # pet and ct features
            train_psa_all = []
            test_psa_all = []
            y_train_psa = []
            y_test_psa = []
            for pid in pid_range:
    #                if pid != i:
                if pid in training_subj_range:
                    train_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
    #                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_train_psa.append(1)
                    else:
                        y_train_psa.append(0)
                else:
                    test_psa_all.append([mean_fi[pid][fid] for fid in range(n_features)])
    #                    if mean_fi[pid][n_features+1] >= 0:
                    if delta_psa_patients[pid_reverse_map[pid]] < 0:
                        y_test_psa.append(1)
                    else:
                        y_test_psa.append(0)
            train_psa_all = np.array(train_psa_all)
            train_psa_all = scale(train_psa_all)
            mean = np.mean(train_psa_all, axis=0)        
                        
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_all)
            train_psa_all = scaler.transform(train_psa_all)
            test_psa_all = scaler.transform(test_psa_all) 
            
            # pet features
            train_psa_pet = []
            test_psa_pet = []
            for pid in pid_range:
                if pid in training_subj_range:
                    train_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                else:
                    test_psa_pet.append([mean_fi[pid][fid] for fid in range(num_pet_features)])
                        
            print ("YTRAIN")
            print (y_train_psa)
            train_psa_pet = np.array(train_psa_pet)
            
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_pet)
            train_psa_pet = scaler.transform(train_psa_pet)
            test_psa_pet = scaler.transform(test_psa_pet) 
    
            y_train_psa = np.array(y_train_psa)
            y_test_psa = np.array(y_test_psa)
    #            print ("PSA")
            # ct features
            train_psa_ct = []
            test_psa_ct = []
            for pid in pid_range:
                if pid in training_subj_range:
                    train_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
                else:
                    test_psa_ct.append([mean_fi[pid][fid] for fid in range(num_pet_features, num_all_features)])
            train_psa_ct = np.array(train_psa_ct)
    
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_psa_ct)
            train_psa_ct = scaler.transform(train_psa_ct)
            test_psa_ct = scaler.transform(test_psa_ct) 
            
            
            
            # only best 3 features
            if do_regresson:
                train_psa = []
                test_psa = []
                n_features = len(best_features) - 2
                for pid in pid_range:
                    if pid in training_subj_range:
                        train_psa.append([mean_fi_reg[pid_reverse_map[pid]][fid] for fid in range(n_features)])
                    else:
                        test_psa.append([mean_fi_reg[pid_reverse_map[pid]][fid] for fid in range(n_features)])
                train_psa = np.array(train_psa)
    
                scalerObj = preprocessing.StandardScaler()
                scaler = scalerObj.fit(train_psa)
                train_psa = scaler.transform(train_psa)
                test_psa = scaler.transform(test_psa) 
            
    
    #            print ("TEST2")
            print (train_psa_pet.shape)
            print (y_train_psa)            
            
            # for pet
    #            c_rbf = 2
    #            gamma_rbf = 2**-13
    #            
            c_rbf = 1000
            gamma_rbf = 0.001953125
            
            if balanced:
                c_rbf = 1000
                gamma_rbf = 0.001953125
            
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                
            elif classifier == 1:            
                # poly svc classifier for pet features
                clf = SVC(kernel='poly', degree=3, C=1).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_pet, y_train_psa)
                probas_pet = clf.predict_proba(test_psa_pet)[:,1]
                y_pred = clf.predict(test_psa_pet)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_pet, y_train_psa)
                probas_pet = clf.decision_function(test_psa_pet)
                y_pred = clf.predict(test_psa_pet)
                
            poly_probas_pet.append(probas_pet)
            y_pred_psa = clf.predict(test_psa_pet)
            y_pred_psa_poly_pet.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_pet, y_test_psa)
            print ("PREDICTION PET")
    #        print (probas_pet)
    #        print (y_pred_psa, y_test_psa, y_score_psa)
            print (y_score_psa)
            pet_scores.append(y_score_psa)
            
            # for ct
    #            c_rbf = 2**-5 
    #            gamma_rbf = 2**-11
            c_rbf = 32 
            gamma_rbf = 0.5
            c_lin = 2**-5
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=1.0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_ct, y_train_psa)
                probas_ct = clf.predict_proba(test_psa_ct)[:,1]
                y_pred = clf.predict(test_psa_ct)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_ct, y_train_psa)
                probas_ct = clf.decision_function(test_psa_ct)
                y_pred = clf.predict(test_psa_ct)
                
            poly_probas_ct.append(probas_ct)
            y_pred_psa = clf.predict(test_psa_ct)
            y_pred_psa_poly_ct.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_ct, y_test_psa)
            print ("PREDICTION CT")
    #        print (probas_ct)
    #        print (y_pred_psa, y_test_psa, y_score_psa)
            print (y_score_psa)
            ct_scores.append(y_score_psa)
            
            # for all features
    #            c_rbf = 2
    #            gamma_rbf = 0.00048828125 or 2**-11
            c_rbf = 100
            gamma_rbf = 0.0001220703125
            
            c_lin = 2**-5            
            if classifier == 0:
                # rbf svc
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                
            elif classifier == 1:            
                # poly svc classifier 
                clf = SVC(kernel='poly', degree=2, C=2**-5).fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                
            elif classifier == 2:
                # extra trees
                clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa_all, y_train_psa)
                probas_ = clf.predict_proba(test_psa_all)[:,1]          
                y_pred = clf.predict(test_psa_all)
                
            elif classifier == 3:
                # linear svc
                clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa_all, y_train_psa)
                probas_ = clf.decision_function(test_psa_all)
                y_pred = clf.predict(test_psa_all)
                
            if not do_proba_average:
                poly_probas_all.append(probas_)
            # OR average pet and ct estimators instead
            else:
                poly_probas_all.append(float(0.5 * (probas_pet + probas_ct)))
            y_pred_psa = clf.predict(test_psa_all)
            y_pred_psa_poly_all.append(y_pred_psa[0])
            y_score_psa = clf.score(test_psa_all, y_test_psa)
            print ("PREDICTION PET/CT")
    #        print (probas_)
    #        print (y_pred_psa, y_test_psa, y_score_psa)
            print (y_score_psa)
            pet_ct_scores.append(y_score_psa)
            
            # poly svc classifier for best features
            if do_regresson:
                # for best 3 features
                c_rbf = 10
                gamma_rbf = 2**-7 
                c_lin = 2**-5
                if classifier == 0:
                    # rbf svc
                    clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    
                elif classifier == 1:            
                    # poly svc classifier 
                    clf = SVC(kernel='poly', degree=3, C=1, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                    
                elif classifier == 2:
                    # extra trees
                    clf = ExtraTreesClassifier(n_estimators=250, random_state=0).fit(train_psa, y_train_psa)
                    probas_ = clf.predict_proba(test_psa)[:,1]         
                    y_pred = clf.predict(test_psa)
                    
                elif classifier == 3:
                    # linear svc
                    clf = SVC(kernel='linear', C=c_lin, class_weight='balanced').fit(train_psa, y_train_psa)
                    probas_ = clf.decision_function(test_psa)
                    y_pred = clf.predict(test_psa)
                        
                y_pred_psa = clf.predict(test_psa)
                y_score_psa = clf.score(test_psa, y_test_psa)
                print ("PREDICTION BEST")
    #            print (probas_)
    #            print (y_pred_psa, y_test_psa, y_score_psa)
                print (y_score_psa)
                best_features_scores.append(y_score_psa)
            
            
            
            # appending true psa label
    #        print ("Y_PSA_TEST")
    #        print (y_test_psa)
            
        # delta psa regression
        if do_regresson:
            # RRRRRRR
            c_rbf_r = 2**13
            gamma_rbf_r = 2**-15
            c_lin_r = 0.5
            c_poly_r = 1
        
            train_reg = []
            test_reg = []
            y_train = []
            y_test = []
            for pid in pid_range:
                if pid in training_subj_range:
                    train_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_train.append(mean_fi[pid][n_features-1])
                else:
                    test_reg.append([mean_fi[pid][fid] for fid in range(n_features)])
                    y_test.append(mean_fi[pid][n_features-1])
                    
            # standardize data
            scalerObj = preprocessing.MinMaxScaler()
            scaler = scalerObj.fit(train_reg)
            train_reg = scaler.transform(train_reg)
            test_reg = scaler.transform(test_reg)
            
            y_train = np.array(y_train)
            
            if minmaxed_psa:
                scalerObjY = preprocessing.MinMaxScaler()
                scalerY = scalerObjY.fit(y_train)
                y_train = scalerY.transform(y_train)
                y_test = scalerY.transform(y_test)
            
            
            
    #            svr_rbf = SVR(kernel='rbf', C=c_rbf_r, gamma=gamma_rbf_r)
            svr_rbf = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='sigmoid', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    #            svr_lin = SVR(kernel='linear', C=c_lin_r)
            svr_lin = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='linear', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    #            svr_poly = SVR(kernel='poly', C=c_poly_r, degree=2)
            svr_poly = SVR(C=10, cache_size=200, coef0=0.0, degree=3, epsilon=0.5, gamma='scale',
                          kernel='poly', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
            etr = ExtraTreesRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            rfr = RandomForestRegressor(max_depth=30, min_samples_leaf=1, n_estimators=1000, random_state=0)
            y_rbf = svr_rbf.fit(train_reg, y_train).predict(test_reg)
            y_lin = svr_lin.fit(train_reg, y_train).predict(test_reg)
            y_poly = svr_poly.fit(train_reg, y_train).predict(test_reg)
            y_etr = etr.fit(train_reg, y_train).predict(test_reg)
            y_rfr = rfr.fit(train_reg, y_train).predict(test_reg)
            
            rbf_score = svr_rbf.score(test_reg, y_test)
            lin_score = svr_lin.score(test_reg, y_test)
            poly_score = svr_poly.score(test_reg, y_test)
            etr_score = etr.score(test_reg, y_test)
            rfr_score = rfr.score(test_reg, y_test)
            
            print ("REGRESSION")
            print (etr_score, rfr_score, lin_score, poly_score, rbf_score)
            etr_scores.append(etr_score)
            rfr_scores.append(rfr_score)
            lin_scores.append(lin_score)
            poly_scores.append(poly_score)
            rbf_scores.append(rbf_score)
    
    print ("AVERAGE_PSA_CLASSIFICATION_SCORES:")
    print ("PET, CT, PET/CT, BEST_FEATURES")
    print (np.mean(pet_scores), np.mean(ct_scores), np.mean(pet_ct_scores), np.mean(best_features_scores))
    
    print ("AVERAGE_PSA_REGRESSION_SCORES:")
    print ("ETR, RFR, LIN, POLY, RBF")
    print (np.mean(etr_scores), np.mean(rfr_scores), np.mean(lin_scores), np.mean(poly_scores), np.mean(rbf_scores))



