import os
import xml.etree.ElementTree as ET
import numpy as np

import matplotlib.pyplot as plt

from skimage import draw
from matplotlib import path


#ds_path = "/home/sobhan/data/dataset/"
#ds_path = "/home/sobhan/data/2021/"
ds_path = "/home/sobhan/data/test_old/non_responders/"

do_pyradiomics = False

def get_mask_from_polygon_mpl(image_shape, polygon):
  """Get a mask image of pixels inside the polygon.

  Args:
    image_shape: tuple of size 2.
    polygon: Numpy array of dimension 2 (2xN).
  """
  xx, yy = np.meshgrid(np.arange(image_shape[1]), np.arange(image_shape[0]))
  xx, yy = xx.flatten(), yy.flatten()
  indices = np.vstack((xx, yy)).T
#  print (indices)
#  print (polygon)
  mask = path.Path(polygon).contains_points(indices)
  mask = mask.reshape(image_shape)
  mask = mask.astype('bool')
  return mask


def get_mask_from_polygon_skimage(image_shape, polygon):
  """Get a mask image of pixels inside the polygon.

  Args:
    image_shape: tuple of size 2.
    polygon: Numpy array of dimension 2 (2xN).
  """
  vertex_row_coords = polygon[:, 1]
  vertex_col_coords = polygon[:, 0]
  fill_row_coords, fill_col_coords = draw.polygon(vertex_row_coords, vertex_col_coords, image_shape)
  mask = np.zeros(image_shape, dtype=np.bool)
  mask[fill_row_coords, fill_col_coords] = True
  return mask


affine_mat = np.array([[ 9.76562e-01,  0.00000e+00,  0.00000e+00, -2.49512e+02],
 [ 0.00000e+00,  9.76562e-01,  0.00000e+00, -4.12512e+02],
 [ 0.00000e+00,  0.00000e+00,  2.40000e+00, -1.65700e+03],
 [ 0.00000e+00,  0.00000e+00,  0.00000e+00,  1.00000e+00]])

#affine_mat = np.array([[   1.17188, 0.,      0.,   -300.  ],
# [   0.,      1.17188, 0.,   -230.  ],
# [   0.,      0.,      3.5,     0.     ],
# [   0.,      0.,      0.,      1.     ]])
    
#affine_mat = np.array([[ 9.76562e-01,  0.00000e+00  , 0.00000e+00 , -2.50000e+02],
# [ 0.00000e+00,  9.76562e-01,  0.00000e+00, -2.50000e+02],
# [ 0.00000e+00 , 0.00000e+00,  2.00000e+00, -9.96500e+02],
# [ 0.00000e+00  ,0.00000e+00 , 0.00000e+00 , 1.00000e+00]])
    
#affine_mat = np.array([[   5.46875,      0.,          -0.,        -347.26562  ],
# [   0.    ,       5.46875,      0.,        -347.26562  ],
# [   0.     ,     -0.      ,     3.2700195, -996.5      ],
# [   0.      ,     0.       ,    0.,           1.       ]])
    
# for #56 pet
#affine_mat = np.array([[   4.07283,  0.,       0., -406.5    ],
# [   0.,    4.07283,    0., -579.879  ],
# [   0.,    0.,    3., -884.5    ],
# [   0.,    0.,    0.,    1.     ]])
#
## for # 56 ct
#affine_mat = np.array([[   0.976562,    0.,          0.,       -249.512   ],
# [   0.,          0.976562,    0.,       -424.012   ],
# [   0.,         0.,          2.,       -883.5     ],
# [   0.,          0.,          0.,          1.      ]])
    

# for # 63 pet  
affine_mat = np.array([[   4.,         0.,         0.,      -286.586  ],
 [   0.        , 4.        , 0. ,     -216.586  ],
 [   0.        , 0.        , 4.  ,       1.69995],
 [   0.        , 0.       ,  0.   ,      1.     ]])

# for # 63 ct
affine_mat = np.array([[   1.17188   , 0.,         0. ,     -300.,     ],
 [   0.       ,  1.17188   , 0.  ,    -230.     ],
 [   0.        , 0.        , 3.5  ,      0.     ],
 [   0.        , 0.         ,0.    ,     1.     ]])
    
# get the inverse transform
affine_mat = np.linalg.inv(affine_mat)
    
M = affine_mat[:3, :3]
abc = affine_mat[:3, 3]

num_slices = []
idx = 0
for index, patient_id in enumerate(sorted(os.listdir(ds_path))):
    if patient_id in ["63"]:
        print (index)
        print (patient_id)
        num_slices.append(len(os.listdir(os.path.join(ds_path, patient_id,"dicom/pet/images/"))))
        print (num_slices)
        path_mediso = os.path.join(ds_path, patient_id,"mediso/")
        tree = ET.parse(os.path.join(path_mediso,"roi.roi"))
        root = tree.getroot()
        print (root)
        print (root.attrib)
        print (affine_mat)
        polygons_coords = []
        rois = dict()
        roi_names = []
        for roi in root.iter('ROI'):
            names = []
            for name in roi.iter('Name'):
                print (name.text)
                names.append(name.text)
                roi_names.append(name.text)
            for iname, polygons in enumerate(roi.iter('Polygons')):
                plgns = []
                for polygon in polygons.iter('Polygon'):
                    z_coords = []
                    zxy_coords = []
                    POLYGONS = dict()
                    for offset in polygon.iter('Offset'):
                        z_coords.append(offset.text.split(' ')[1])
                    itr = 0
                    for i, points in enumerate(polygon.iter('Points')):
                        _xy_coords = []
                        _zxy_coords = []
                        for point in points.iter('Point'):
                            z, x, y = float(z_coords[itr]), float(point.text.split(' ')[0]), float(point.text.split(' ')[1])
                            # for patients 0-82 M.dot([x,y,z]) + abc
                            x, y, z = M.dot([x, y, z]) + abc
                            _zxy_coords.append([z, x, y])
                            _xy_coords.append([x, y])
                        POLYGONS[float(z_coords[itr])] = _xy_coords
                        zxy_coords.append(_zxy_coords)
                        image_shape = (512, 512)
                        polygon_ = POLYGONS[float(z_coords[itr])]
                        if len(polygon_) > 0:
                            mask1 = get_mask_from_polygon_mpl(image_shape, polygon_).astype('uint')
                        results_dir = "/home/sobhan/src/r_scripts/xyName/%s/" % names[iname]
                        if not os.path.isdir(results_dir):
                            os.makedirs(results_dir)
    #                    plt.imsave(results_dir + "%d.png" % int(z_coords[itr]), mask1, dpi=300)
                        itr +=1
                    plgns.append(POLYGONS)
                rois [names[iname]] = plgns
        
        # merging polygons of single slices
        polygons = dict()
        print (num_slices)
#        print (index)
        for z in range(num_slices[idx]):
            plgns = []
            for name in np.unique(roi_names):
                if "met" in str(name).lower() or "tum" in str(name).lower():
                    for poly in rois[name]:
                        if poly.get(float(z)):
                            plgns.append(poly.get(float(z)))
            polygons[int(z)] = plgns
        image_shape = (512, 512)
        for z in range(num_slices[idx]):
            print (z)
            masks = np.zeros(image_shape)
            for polygon in polygons[z]:
                masks += get_mask_from_polygon_mpl(image_shape, polygon).astype('uint')
            results_dir = os.path.join(path_mediso, "masks/")
            if not os.path.isdir(results_dir):
                os.makedirs(results_dir) 
            masks = np.where(masks>0,1,0)
            if do_pyradiomics:
                from radiomics import featureextractor
                from radiomics import firstorder
                import SimpleITK as sitk
                import six
                if masks.any():
                    extractor = featureextractor.RadiomicsFeatureExtractor()
                    img = sitk.GetImageFromArray(masks)
                    mask = sitk.GetImageFromArray(masks)
                    pyrad_res = extractor.execute(img, mask)
                    firstOrderFeatures = firstorder.RadiomicsFirstOrder(img,mask)
                    firstOrderFeatures.enableAllFeatures()
                    for (key,val) in six.iteritems(firstOrderFeatures.execute()):                    
                        print("\t%s: %s" % (key, val))
            plt.imsave(results_dir + "%d.png" % int(z), masks, dpi=300, cmap="gray")
        idx += 1