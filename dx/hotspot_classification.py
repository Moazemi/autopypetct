#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 13:04:06 2020

@author: sobhan
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 11:22:06 2020

@author: sobhan
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:48:37 2020

@author: sobhan
"""

import matplotlib.pyplot as plt
from sklearn.svm import SVC, SVR
from sklearn.model_selection import LeaveOneOut, KFold
import numpy as np
import pandas as pd
import csv
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from scipy import interp
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
import seaborn as sns
from sklearn.model_selection import GridSearchCV

n_train = 72
n_bootstraps = 100
n_subjects = 72

feature_set = "PET-CT" # PET, CT, or PET/CT
# step_str = " (with %d Tranining Subjects)" % n_train # final validation or Inter-observer Tests


train_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/oncotarget/train72.csv"
header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/oncotarget/head_pet_ct.csv"
test_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/oncotarget/test.csv"

show_interim_rocs = False
do_psa_classification = False




pid_range = range(n_subjects)
#pid_range = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17,21,23,24,26,32,44,63,35,27,25,28,29,40,16,38,52,51,37,18,48,20,22]
pid_map = dict()
pid_reverse_map = dict()
ii = 0
for pid in pid_range:
    pid_map[ii] = pid
    pid_reverse_map[pid] = ii
    ii += 1


# params for et classifier, ...
dep_dec = 20
est_dec = 250

# import feature names
header = []
with open(header_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header = next(reader)
#    for column in header:
#        print (column)

# import training data
data = []
labels = []
data_new = []
with open(train_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        data.append([row[column] for column in header])
        labels.append(int(row['label']))
        data_new.append([[row[column] for column in header], int(row['label'])])

# import validation data
test_data = []
test_labels = []
with open(test_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        test_data.append([row[column] for column in header])
        test_labels.append(int(row['label']))



X, y = np.array(data), np.array(labels)
X_test, y_test = np.array(test_data), np.array(test_labels)


# scalerObj = preprocessing.MinMaxScaler()
# scaler = scalerObj.fit(X)
# X = scaler.transform(X)
# X_test = scaler.transform(X_test)

print(X.shape)
##X = scale(X)
#scalerObj = preprocessing.MinMaxScaler()
#scaler = scalerObj.fit(X)
#X = scaler.transform(X)
#X_test = scaler.transform(X_test)

# initialize cross validation kernels
#loo = LeaveOneOut()
#print (loo.get_n_splits(X))
#kf = KFold(n_splits=5, shuffle=True)
#print (kf.get_n_splits(X))
#n_classes = 2



######################################
# helper functions
######################################    
# draw the ROC curves
def plot_roc_legend(caption=''):
    classification_task = "Hotspot"
    if do_psa_classification:
        classification_task = "PSA"
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.rc('xtick', labelsize=11)
    plt.rc('ytick', labelsize=11)
    plt.title('ROC for %s Classification: %s' % (classification_task, caption))
    plt.legend(loc="lower right")
#        fig = plt.gcf()
#        fig.set_size_inches((10, 6), forward=False)
#    manager = plt.get_current_fig_manager()
#    manager.window.showMaximized()
#        fig.savefig("./res/NEW_ROC.png", dpi=300)
    plt.savefig("./res/oncotarget/ROC_%s.png" % caption, dpi=150)
    plt.close()




# i = n_train
aucss = []
stds_auc = []
senss = []
specs = []
stds_spe = []
for i in range(30, n_subjects + 1):
    step_str = " (with %d Tranining Subjects)" % (i)  # final validation or Inter-observer Tests
    _tprs = []
    _tnrs = []
    tprs = []
    fprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)

    for ib in range(n_bootstraps):
        train_data = []
        labels = []
        # with open(train_path, 'rt') as csvfile1:
        #     reader = csv.DictReader(csvfile1, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        #     for row in reader:
        #         if int(row['patient_id']) in range(0, i):
        #             train_data.append([row[column] for column in header])
        #             labels.append(int(row['label']))
        #                print (i, int(row['patient_id']))
        data_new = np.array(data_new)
        np.random.shuffle(data_new)
        print(data_new.shape)
        for idx, (data_, label_) in enumerate(data_new):
            if idx in range(0, i * int(len(data_new)/72)):
                # print(data_)
                # print(label_)
                train_data.append(data_)
                labels.append(label_)

        print ("HERE")
        print (i)

        X_train = np.asarray(train_data, dtype=np.float32)
        X_test = np.asarray(X_test, dtype=np.float32)
        y_train = np.asarray(labels)
        print (X_train.shape)
        # print (y_train)
        # print (X_test.shape)


        # normalize data

        # scalerObj = preprocessing.MinMaxScaler()
        # scaler = scalerObj.fit(X_train)
        # X_train = scaler.transform(X_train)
        # X_test = scaler.transform(X_test)



        # extra trees classifier
        trees = ExtraTreesClassifier(max_depth=dep_dec, min_samples_leaf=1, n_estimators=est_dec, random_state=True, bootstrap=True, oob_score=True, class_weight='balanced')

        from sklearn.pipeline import make_pipeline

        # pipe = make_pipeline(preprocessing.MinMaxScaler(), trees)

        pipe = make_pipeline(preprocessing.MinMaxScaler(),
                             # GridSearchCV(ExtraTreesClassifier(bootstrap=True, n_estimators=250, oob_score=True, class_weight='balanced', random_state=True),
                             #              param_grid={
                             #                  'max_depth': [1, 10, 20, 30],
                             #                  'min_samples_leaf': [1, 5, 10]
                             #              },
                             #              cv=5,
                             #              refit=True,
                             #              n_jobs=-1))
                             trees)
        # X_train.fillna(X_train.mean(), inplace=True)
        # X_test.fillna(X_train.mean(), inplace=True)
        # X_train = np.nan_to_num(X_train, nan=0, posinf=0.99, neginf=-0.99)
        # y_train = np.nan_to_num(y_train, nan=0, posinf=0.99, neginf=-0.99)

        # trees.fit(X_train, y_train)

        pipe.fit(X_train, y_train)
        # print("PARAMS")
        # print(pipe.get_params(deep=True))
        X_test = np.array(X_test, dtype='float32')
        try:
            # probas_ = trees.predict_proba(X_test)
            probas_ = pipe.predict_proba(X_test)
        except ValueError as e:
            print ("Vallue Error")
            print(X_test[np.where(np.isinf(X_test))])
            print(X_test[np.where(np.isnan(X_test))])
            X_test = np.nan_to_num(X_test, nan=0, posinf=0.99, neginf=-0.99)
            # probas_ = trees.predict_proba(X_test)
            probas_ = pipe.predict_proba(X_test)

        y_pred = pipe.predict(X_test)
        cnf_et = confusion_matrix(y_test, y_pred)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        #tprs_tree.append(interp(mean_fpr, fpr, tpr))
        #tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        #aucs_tree.append(roc_auc)

        # et SVC
        _fpr = float(cnf_et[0][1]/(cnf_et[0][1] + cnf_et[0][0]))
        _tpr = float(cnf_et[1][1]/(cnf_et[1][1] + cnf_et[1][0]))
        _tnr = float(cnf_et[0][0]/(cnf_et[0][0] + cnf_et[0][1]))
        _fnr = float(cnf_et[1][0]/(cnf_et[1][0] + cnf_et[1][1]))
        print ("Extra Trees: ",_tpr, _tnr)

        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=2, alpha=0.8,
                 label='Extra Trees (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

        _tprs.append(_tpr)
        _tnrs.append(_tnr)
        fprs.append(fpr)
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0

    _tpr = np.mean(np.array(_tprs))
    std_tpr = np.std(np.array(_tprs))
    _tnr = np.mean(np.array(_tnrs))
    std_tnr = np.std(np.array(_tnrs))
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    roc_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(np.array(aucs))

    plt.plot(mean_fpr, mean_tpr, lw=2, alpha=0.8,
                 label='Extra Trees (AUC=%0.2f$\pm$%0.2f, SE=%0.2f$\pm$%0.2f, SP=%0.2f$\pm$%0.2f)' % (roc_auc, std_auc, _tpr, std_tpr, _tnr, std_tnr))

    aucss.append(roc_auc)
    senss.append(_tpr)
    specs.append(_tnr)

    stds_auc.append(std_auc)
    stds_spe.append(std_tnr)

    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend(feature_set+step_str)

mean_auc = np.mean(np.array(aucss))
std_auc = np.std(np.array((aucss)))

mean_senss = np.mean(np.array(senss))
std_senss = np.std(np.array((senss)))

mean_specs = np.mean(np.array(specs))
std_specs = np.std(np.array((specs)))

print("FINAL SCORES:")
print("AUC: ", mean_auc, std_auc)
print("SE: ", mean_senss, std_senss)
print("SP: ", mean_specs, std_specs)



# df = pd.DataFrame()
# df['auc'] = np.array(aucss)
# df['sen'] = np.array(senss)
# df['spe'] = np.array(specs)

sns.color_palette("Paired")
sns.scatterplot(x=range(30, 30 + len(aucss)), y=np.array(aucss))
sns.lineplot(x=range(30, 30 + len(aucss)), y=np.array(aucss), label='AUC')
sns.scatterplot(x=range(30, 30 + len(aucss)), y=np.array(senss))
sns.lineplot(x=range(30, 30 + len(aucss)), y=np.array(senss), label='SE')
sns.scatterplot(x=range(30, 30 + len(aucss)), y=np.array(specs))
sns.lineplot(x=range(30, 30 + len(aucss)), y=np.array(specs), label='SP')

# sns.boxplot(x=range(30, 30 + len(aucss)), y='sen', data=df)
# plt.bar(range(30, 30 + len(aucss)), np.array(specs), xerr=stds_spe, align='center')
# sns.scatterplot(range(30, 30 + len(aucss)), np.array(specs), x_estimator=np.mean)


plt.title("Average Prediction Scores")
plt.xlabel("Training Set Size: 30-72 Subjects")
plt.ylabel("Score")



plt.savefig("./res/oncotarget/Scores_box.png", dpi=150)
plt.close()