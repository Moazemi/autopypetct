#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:48:37 2020

@author: sobhan
"""

import matplotlib.pyplot as plt
from sklearn.svm import SVC, SVR
from sklearn.model_selection import LeaveOneOut, KFold, StratifiedKFold
import numpy as np
import csv
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from scipy import interp
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from scipy.stats import linregress
import os
from sklearn.linear_model import LassoCV
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import RFECV

do_kfold = True
do_rfe = False #recursive feature elimination
show_interim_rocs = False
do_psa_classification = True
do_draw_feature_importances = False
do_regression = False
do_lassocv = False
do_combine_pet_ct = True
do_combine_rad_cli = True
do_mediso = False

feature_set = "Radiomics+Clinical(FO_Normalized)_THRES" # Radiomics, Clinical AND mask type  etc.
# feature_set = "Clinical_Mediso" # Mediso: pet, ct, cli, else.
step_str = " (3-Fold CV)" # CV type, [Balanced or Unbalanced]

responders_ids_path = "/home/sobhan/data/train/responders"
responders_patient_ids = np.asarray(sorted(os.listdir(responders_ids_path)), np.int)
nonresponders_ids_path = "/home/sobhan/data/train/non_responders"
nonresponders_patient_ids = np.asarray(sorted(os.listdir(nonresponders_ids_path)), np.int)

res_dir = "/home/sobhan/data/2021/res"

# print (responders_patient_ids)
# print (nonresponders_patient_ids)

# train_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/train_psa_all_pyrad_pet_gt80.csv"
# header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_all.csv"
if not do_mediso:
    train_path = "/home/sobhan/data/csv/2021/train_psa_pyrad_fo_unet_pet_99.csv"
    train_path_ct = "/home/sobhan/data/csv/2021/train_psa_pyrad_fo_unet_ct_99.csv"
    # header_path = "/home/sobhan/data/csv/2021/head_pyrad_all_nomalized_pet.csv"
    # header_path = "/home/sobhan/data/2021/res/RFE_extra_trees_svc_Radiomics+Clinical(All_Normalized)_UNet.csv"
    header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_pet.csv"
    # header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"
    header_path_ct = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_ct.csv"
    # header_path_ct = "/home/sobhan/data/csv/2021/head_pyrad_all_nomalized_ct.csv"
    header_path_cli = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"
    header_path_best = "/home/sobhan/data/2021/res/RFE_extra_trees_svc_PET+CT_Radiomics(All)_UNet.csv"
else:
    train_path = "/home/sobhan/data/csv/2021/train_psa_99_mix_new.csv"
    header_path = "/home/sobhan/data/csv/2021/head_med_all.csv"
    # header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"

def get_min_maxed_trasform(x):
    min_ = np.min(x)
    max_ = np.max(x)
    range_ = max_ - min_
    ret = np.zeros(len(x))
    for i in range(len(x)):
        ret[i] = float((x[i]-min_)/range_)
    return ret

# import feature names
def load_header(header_path):
    header = []
    with open(header_path, 'rt') as header_file:
        reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        header = next(reader)
        for i in range(len(header)):
            if header[i] == 'time_dif':
                    header[i] = 'time_def'
    return header

header = load_header(header_path)
n_features_pet = len(header)
if not do_mediso:
    header_ct = load_header(header_path_ct)
    n_features_ct = len(header_ct)
    header_cli = load_header(header_path_cli)
    n_features_cli = len(header_cli)

# import training data
def load_train_data(train_path, header):
    data = []
    labels = []
    deltaPSA = []
    with open(train_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(row['patient_id']) in nonresponders_patient_ids:
                data.append([float(row[column]) for column in header])
                labels.append(int(row['label']))
                deltaPSA.append(float(row['delta_psa']))
    return data, labels, deltaPSA

data, labels, deltaPSA = load_train_data(train_path, header)
if not do_mediso:
    data_ct, labels, deltaPSA = load_train_data(train_path_ct, header_ct)
    data_cli, labels, deltaPSA = load_train_data(train_path, header_cli)

if do_combine_pet_ct:
    data = np.concatenate((data, data_ct), axis=1)
    header = np.concatenate((header, header_ct))
    if do_combine_rad_cli:
        data = np.concatenate((data, data_cli), axis=1)
        header = np.concatenate((header, header_cli))

X, y = np.array(data), np.array(labels)

if do_regression:
    best_features = []
    for i in range(len(header)):
    #    print(header[i],linregress(X[:,i],deltaPSA))
        slope, intercept, r_value, p_value, std_err = linregress(X[:,i],deltaPSA)
        if p_value < 0.05:
            if header[i] == 'time_def':
                header[i] = 'time_dif'
            best_features.append(header[i])
            lw = 2
            plt.scatter(X[:,i], deltaPSA, color='cyan', label='original data')
            plt.plot(X[:,i], intercept + slope * X[:,i], 'r', label='fitted line')
            plt.xlabel('%s (r_value: %0.4f, p_value: %0.4f)' % (header[i], r_value, p_value))
            plt.ylabel('Delta PSA')
            plt.title('Regression test' )
            plt.legend()
            # plt.savefig("./res/2020/32/Reg_%s.png" % header[i], dpi=300)
            plt.savefig(os.path.join(res_dir,"Reg_%s.png" % header[i]), dpi=300)
            plt.close()

    with open(os.path.join(res_dir,"head_best_%s.csv" % feature_set), 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(best_features)


if do_lassocv:
    # normalize data    
    scalerObj = preprocessing.MinMaxScaler()
    scaler = scalerObj.fit(X)
    X = scaler.transform(X)
    deltaPSA = get_min_maxed_trasform(deltaPSA)
    print(deltaPSA)
    # clf = LassoCV(cv=5, selection='random', random_state=5).fit(X, deltaPSA)
    clf = LassoCV(cv=5, selection='random', random_state=5).fit(X, labels)
    print ("SCORE")

    print (clf.score(X, labels))
    importance = np.abs(clf.coef_)
    print (importance)
    for idx, imp in enumerate(importance):
        if imp > 0:
            print(header[idx], imp)


kf = StratifiedKFold(n_splits=3, shuffle=True, random_state=2)
# print (kf.get_n_splits(X))
n_classes = 2

######################################
# helper functions
######################################    
# draw the ROC curves
def plot_roc_legend(caption=''):
    classification_task = "Hotspot"
    if do_psa_classification:
        classification_task = "PSA"
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.rc('xtick', labelsize=11)
    plt.rc('ytick', labelsize=11)
    plt.title('ROC for %s Classification: %s' % (classification_task, caption))
    plt.legend(loc="lower right")
    # plt.savefig("./res/2020/NEW_ROC_%s.png" % caption, dpi=300)
    plt.savefig(os.path.join(res_dir, "NEW_ROC_%s.png" % caption), dpi=300)
    plt.close()


# draw feature importances diagrams          
def feauture_importances(coef, std, names, title):
    names = np.array(names)
    coef = np.array(coef)
    imp = np.absolute(coef)
    imp, std, names = zip(*sorted(zip(imp, std, names)))
#    print ('INSIDE feauture_importances', coef, names)
    _names = []
    for i in range(len(names)):
        name = str(names[i])
        print ('INSIDE feauture_importances', name)
        name = name.replace("Gray-LevelNon-Uniformity", "GLNU")
        name = name.replace("BoneMineralDensity", "BMD")
        name = name.replace("LongRun", "LR")
        name = name.replace("LongZone", "LZ")
        name = name.replace("HighGrey", "HG")
        name = name.replace("ShortRun", "SR")
        name = name.replace("Level", "L")
        name = name.replace("LongZoneHighGrey-Level", "LZHG-L")
        name = name.replace("Emphasis", "E")
        if not ('CT' in name):
            name = name.replace(name, 'PET_' + name)
        print ('INSIDE feauture_importances', name)
        _names.append(name)
    plt.rc('xtick', labelsize=5)
    plt.rc('ytick', labelsize=5)
    plt.title("Feature Importances for %s Classifier (PET/CT)" % title)
    plt.barh(range(20), imp[-20:], xerr=std[-20:], align='center')
    plt.yticks(range(20), _names[-20:])
#    manager = plt.get_current_fig_manager()
#    manager.window.showMaximized()
    plt.savefig("./res/2020/NEW_FI.png", dpi=300)
    plt.show()

if do_kfold:
    # init roc variables
    tprs_lr = []
    aucs_lr = []
    mean_fpr_lr = np.linspace(0, 1, 100)
    cnfs_lr = []

    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    cnfs_svc = []
    
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    cnfs_poly = []
    
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    cnfs_rbf = []
    
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    cnfs_tree = []
    
    tprs_rf = []
    aucs_rf = []
    mean_rf = np.linspace(0, 1, 100)
    cnfs_rf = []
    
    # init feature importances vector
    feature_importances_tree = []
    
    i = 0

    scores_lr = []
    scores_lin = []
    scores_poly = []
    scores_rbf = []
    scores_et = []
    scores_rf = []
    for train_index, test_index in kf.split(X, y):
        #print("TEST sample index:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
        # normalize data    
        scalerObj = preprocessing.MinMaxScaler()
        scaler = scalerObj.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

        # # logistic regression classifier
        # scores = []
        # old_tnr = -1
        # for c_lr in np.logspace(-3,3,7):
        #     for penalty_lr in ["l1","l2"]:
        #         for solver_lr in ["liblinear"]:
        #             clf = LogisticRegression(C=c_lr, n_jobs=4, solver=solver_lr, penalty=penalty_lr, class_weight="balanced")
        #             probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
        #             y_pred = clf.predict(X_test)
        #
        #             cnf = confusion_matrix(y_test, y_pred)
        #             tn, fp, fn, tp = cnf.ravel()
        #             tpr = tp / (tp + fn)
        #             tnr = tn / (tn + fp)
        #             fpr = 1 - tnr
        #             if tnr > old_tnr:
        #                 old_tnr = tnr
        #                 penalty_lr = 1 if "l1" else 2
        #                 scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), c_lr, penalty_lr, 0])
        # tnr, tpr, fpr, score, c, pen, sol = np.max(scores, axis=0)
        # pen = "l1" if 1 else "l2"
        # sol = "liblinear"
        # print(tnr, tpr, fpr, score, c, pen, sol)
        # clf = LogisticRegression(C=c, solver=sol, penalty=pen, class_weight="balanced")
        # probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
        # y_pred = clf.predict(X_test)
        #
        # cnf = confusion_matrix(y_test, y_pred)
        # tn, fp, fn, tp = cnf.ravel()
        # tnr = tn / (tn + fp)
        #
        # cnfs_lr.append(cnf)
        # # Compute ROC curve and area the curve
        # fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        # tprs_lr.append(interp(mean_fpr, fpr, tpr))
        # tprs_lr[-1][0] = 0.0
        # #        tnrs_lr.append(1-fpr)
        # roc_auc = auc(fpr, tpr)
        # aucs_lr.append(roc_auc)
        #
        # if show_interim_rocs:
        #     plt.plot(fpr, tpr, lw=1, alpha=0.3,
        #              label='Logistic Regression, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        # scores_lr.append(np.max(scores, axis=0))

        # logistic regression
        clf = LogisticRegression(penalty='l1', solver='liblinear', random_state=0, class_weight='balanced')
        probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
        y_pred = clf.predict(X_test)
        cnf_lr = confusion_matrix(y_test, y_pred)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        roc_auc = auc(fpr, tpr)

        _fpr = float(cnf_lr[0][1] / (cnf_lr[0][1] + cnf_lr[0][0]))
        _tpr = float(cnf_lr[1][1] / (cnf_lr[1][1] + cnf_lr[1][0]))
        _tnr = float(cnf_lr[0][0] / (cnf_lr[0][0] + cnf_lr[0][1]))
        _fnr = float(cnf_lr[1][0] / (cnf_lr[1][0] + cnf_lr[1][1]))
        # print("Logistic Regression: ", _tpr, _tnr)

        # if show_interim_rocs:
        #     plt.plot(fpr, tpr, lw=2, alpha=0.8,
        #              label='Logistic Regression (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

        tprs_lr.append(interp(mean_fpr, fpr, tpr))
        aucs_lr.append(roc_auc)
        scores_lr.append([_tnr, _tpr, _fpr, clf.score(X_test, y_test), 0.001, 0, 0])

        # linsvc classifier
        scores = []
        old_tnr = -1
        for c_lin in [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5]:
            for gamma_lin in [1e-3, 1e-4, 2**-15, 2**-13, 2**-11, 2**-9, 2**-7, 2**-5, 2**-3, 2**-1, 2, 2**3]:
                clf = SVC(kernel='linear', C=c_lin, gamma=gamma_lin, class_weight='balanced')
                probas_ = clf.fit(X_train, y_train).decision_function(X_test)
                y_pred = clf.predict(X_test)
                
                cnf = confusion_matrix(y_test, y_pred)
                tn, fp, fn, tp = cnf.ravel()
                tpr = tp / (tp+fn)
                tnr = tn / (tn+fp)
                fpr = 1-tnr
                if tnr>old_tnr:
                    old_tnr = tnr
                    scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), c_lin, gamma_lin])
        tnr, tpr, fpr, score, c, gamma = np.max(scores, axis=0)
        print (tnr, tpr, fpr, score, c, gamma)
        clf = SVC(kernel='linear', C=c, gamma=gamma, class_weight='balanced')
        probas_ = clf.fit(X_train, y_train).decision_function(X_test)
        y_pred = clf.predict(X_test)
        
        cnf = confusion_matrix(y_test, y_pred)
        tn, fp, fn, tp = cnf.ravel()
        tnr = tn / (tn+fp)
        
        cnfs_svc.append(cnf)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
#        tnrs_svc.append(1-fpr)
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        scores_lin.append(np.max(scores, axis=0))

        # poly svc classifier
        scores = []
        old_tnr = -1
        for c_poly in [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5]:
            for deg_poly in [2, 3]:
#                for cw_poly in ['balanced', None]:
                poly = SVC(kernel='poly', degree=deg_poly, C=c_poly, class_weight='balanced')#)
                probas_ = poly.fit(X_train, y_train).decision_function(X_test)
                y_pred = poly.predict(X_test)
#                scores.append([clf.score(X_test, y_test), c_poly, deg_poly])
                cnf = confusion_matrix(y_test, y_pred)
                tn, fp, fn, tp = cnf.ravel()
                tpr = tp / (tp+fn)
                tnr = tn / (tn+fp)
                fpr = 1-tnr
                if tnr>old_tnr:
                    old_tnr = tnr
                    scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), c_poly, deg_poly])
        tnr, tpr, fpr, score, c, degree = np.max(scores, axis=0)
        clf = SVC(kernel='poly', C=c, degree=degree, class_weight='balanced')
        probas_ = poly.fit(X_train, y_train).decision_function(X_test)
        y_pred = poly.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        tn, fp, fn, tp = cnf.ravel()
        tnr = tn / (tn+fp)
        cnfs_poly.append(cnf)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_poly.append(interp(mean_fpr, fpr, tpr))
        tprs_poly[-1][0] = 0.0
        
        
        roc_auc = auc(fpr, tpr)
        aucs_poly.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Plynomial SVC, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        scores_poly.append(np.max(scores, axis=0))
            
        # rbf classifier
        scores = []
        old_tnr = 0
        for c_rbf in [1, 10, 100, 1000, 2**-5, 2**-3, 2**-1, 2, 2**3, 2**15, 2**13, 2**11, 2**9, 2**7, 2**5]:
            for gamma_rbf in [1e-3, 1e-4, 2**-15, 2**-13, 2**-11, 2**-9, 2**-7, 2**-5, 2**-3, 2**-1, 2, 2**3]:
                clf = SVC(kernel='rbf', C=c_rbf, gamma=gamma_rbf, class_weight='balanced')
                probas_ = clf.fit(X_train, y_train).decision_function(X_test)
                y_pred = clf.predict(X_test)
                cnf = confusion_matrix(y_test, y_pred)
                tn, fp, fn, tp = cnf.ravel()
                tpr = tp / (tp+fn)
                tnr = tn / (tn+fp)
                fpr = 1-tnr
                if tnr>old_tnr:
                    old_tnr = tnr
                    scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), c_rbf, gamma_rbf])
        tnr, tpr, fpr, score, c, gamma = np.max(scores, axis=0)
        clf = SVC(kernel='rbf', C=c, gamma=gamma, class_weight='balanced')
        probas_ = clf.fit(X_train, y_train).decision_function(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        
        tn, fp, fn, tp = cnf.ravel()
        tnr = tn / (tn+fp)
        
        cnfs_rbf.append(cnf)
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_rbf.append(interp(mean_fpr, fpr, tpr))
        tprs_rbf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rbf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='RBF SVC, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        scores_rbf.append(np.max(scores, axis=0))
        
        # extra trees classifier
        scores = []
        old_tnr = 0
        for max_depth in [1, 5, 10, 15, 20, 25, 30]:
            for min_samples_leaf in [1, 2, 4, 6, 8, 10]:
                clf = ExtraTreesClassifier(max_depth=max_depth, min_samples_leaf=min_samples_leaf, class_weight='balanced')
                probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
                y_pred = clf.predict(X_test)
                cnf = confusion_matrix(y_test, y_pred)
                tn, fp, fn, tp = cnf.ravel()
                tpr = tp / (tp+fn)
                tnr = tn / (tn+fp)
                fpr = 1-tnr
                if tnr>old_tnr:
                    old_tnr = tnr
                    scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), max_depth, min_samples_leaf])
        tnr, tpr, fpr, score, max_depth, min_samples_leaf = np.max(scores, axis=0)
        
        clf = ExtraTreesClassifier(max_depth=int(max_depth), min_samples_leaf=int(min_samples_leaf), class_weight='balanced')
        probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        
        print ("DEBUG")        
        tn, fp, fn, tp = cnf.ravel()
        tnr = tn / (tn+fp)
        print (tnr)
        
        cnfs_tree.append(cnf)
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        scores_et.append(np.max(scores, axis=0))

        # random forest classifier
        scores = []
        old_tnr = 0
        for max_depth in [1, 5, 10, 15, 20, 25, 30]:
            for min_samples_leaf in [1, 2, 4, 6, 8, 10]:
                clf = RandomForestClassifier(max_depth=max_depth, min_samples_leaf=min_samples_leaf, class_weight='balanced')
                probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
                y_pred = clf.predict(X_test)
                cnf = confusion_matrix(y_test, y_pred)
                tn, fp, fn, tp = cnf.ravel()
                tpr = tp / (tp+fn)
                tnr = tn / (tn+fp)
                fpr = 1-tnr
                if tnr>old_tnr:
                    old_tnr = tnr
                    scores.append([tnr, tpr, fpr, clf.score(X_test, y_test), max_depth, min_samples_leaf])
        tnr, tpr, fpr, score, max_depth, min_samples_leaf = np.max(scores, axis=0)
#        score, max_depth, min_samples_leaf = np.max(scores, axis=0)
        clf = RandomForestClassifier(max_depth=int(max_depth), min_samples_leaf=int(min_samples_leaf), class_weight='balanced')
        probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
        y_pred = clf.predict(X_test)
        cnf = confusion_matrix(y_test, y_pred)
        
        tn, fp, fn, tp = cnf.ravel()
        tnr = tn / (tn+fp)
        
        cnfs_rf.append(cnf)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr, fpr, tpr))
        tprs_rf[-1][0] = 0.0
#       roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        scores_rf.append(np.max(scores, axis=0))
        i += 1
   
    
    
    # logistic regression classifier
    mean_tpr = np.max(tprs_lr, axis=0)
    mean_tpr[-1] = 1.0
    std_auc = np.std(aucs_lr)
    #
    print("LOGGGGG")
    print(scores_lr)
    tnr, tpr, fpr, score, c, pen, sol = np.max(scores_lr, axis=0)
    print('Logistic Regression')
    print(c, pen, sol)
    plt.plot(mean_fpr, mean_tpr,  # color='b',
             label=r'Logistic Regression (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)



    # linear SVC classifier
    mean_tpr = np.max(tprs_svc, axis=0)
    mean_tpr[-1] = 1.0    
    std_auc = np.std(aucs_svc)
#   
    tnr, tpr, fpr, score, c, gamma = np.max(scores_lin, axis=0)
    print ('linear SVM')
    print (c, gamma)
    plt.plot(mean_fpr, mean_tpr, #color='b',
             label=r'Linear SVC (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)
    
    # poly svc classifier
    # poly
    mean_tpr = np.max(tprs_poly, axis=0)
    mean_tpr[-1] = 1.0
    std_auc = np.std(aucs_poly)
    tnr, tpr, fpr, score, c, degree = np.max(scores_poly, axis=0)
    print ('poly SVM')
    print (c, degree)
    plt.plot(mean_fpr, mean_tpr, #color='b',
             label=r'Polynomial SVC (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)
    
    # rbf svc classifier
    # rbf
    mean_tpr = np.max(tprs_rbf, axis=0)
    mean_tpr[-1] = 1.0
    std_auc = np.std(aucs_rbf)
    tnr, tpr, fpr, score, c, gamma = np.max(scores_rbf, axis=0)
    print ('RBF SVM')
    print (c, gamma)
    plt.plot(mean_fpr, mean_tpr, #color='b',
             label=r'RBF SVC (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)
    
    # extra trees classifier
    # Extea trees
    mean_tpr = np.max(tprs_tree, axis=0)
    mean_tpr[-1] = 1.0
    std_auc = np.std(aucs_tree)
    tnr, tpr, fpr, score, max_depth, min_sample_leaf = np.max(scores_et, axis=0)
    print ('ET')
    print (max_depth, min_sample_leaf )
    plt.plot(mean_fpr, mean_tpr, #color='r',
             label=r'Extra Trees (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)
    
    # random forest classifier
    # rf
    mean_tpr = np.max(tprs_rf, axis=0)
    mean_tpr[-1] = 1.0
    std_auc = np.std(aucs_rf)
    tnr, tpr, fpr, score, max_depth, min_sample_leaf = np.max(scores_rf, axis=0)
    print ('RF')
    print (max_depth, min_sample_leaf )
    plt.plot(mean_fpr, mean_tpr, #color='g',
             label=r'Random Forest (AUC = %0.2f $\pm$ %0.2f, SE = %0.2f, SP = %0.2f)' % (score, std_auc, tpr, tnr),
             lw=2, linestyle='-', alpha=.8)
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend(feature_set+step_str)
    
    # draw feature importances?    
    if do_draw_feature_importances:        
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "Extra Trees")

if do_rfe: #recursive feature elimination
    scalerObj = preprocessing.MinMaxScaler()
    scaler = scalerObj.fit(X)
    X = scaler.transform(X)

    # logistic regression
    best_features_lr = []
    estimator = LogisticRegression(penalty='l1', solver='liblinear')
    selector = RFECV(estimator, step=1, cv=5)
    selector = selector.fit(X, y)
    for idx, rank in enumerate(selector.ranking_):
        if rank == 1:
            print(header[idx], rank)
            if idx >= n_features_pet and idx < n_features_pet + n_features_ct:
                best_features_lr.append("ct_" + header[idx])
            else:
                best_features_lr.append(header[idx])
    with open(os.path.join(res_dir, "RFE_logistic_regression_%s.csv" % feature_set), 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(best_features_lr)

    # linear svc
    best_features_lin = []
    estimator = SVC(kernel="linear")
    selector = RFECV(estimator, step=1, cv=5)
    selector = selector.fit(X, y)
    for idx, rank in enumerate(selector.ranking_):
        if rank == 1:
            print(header[idx], rank)
            if idx >= n_features_pet and idx < n_features_pet+n_features_ct:
                best_features_lin.append("ct_" + header[idx])
            else:
                best_features_lin.append(header[idx])
    with open(os.path.join(res_dir,"RFE_lin_svc_%s.csv" % feature_set), 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(best_features_lin)

    # # poly svc
    # best_features_poly = []
    # estimator = SVC(kernel="poly")
    # selector = RFECV(estimator, step=1, cv=5)
    # selector = selector.fit(X, y)
    # for idx, rank in enumerate(selector.ranking_):
    #     if rank == 1:
    #         print(header[idx], rank)
    #         if idx >= n_features_pet and idx < n_features_pet + n_features_ct:
    #             best_features_poly.append("ct_" + header[idx])
    #         else:
    #             best_features_poly.append(header[idx])
    # with open(os.path.join(res_dir, "RFE_poly_svc_%s.csv" % feature_set), 'w') as writer_file:
    #     writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    #     writer.writerow(best_features_poly)

    # # rbf svc
    # best_features_rbf = []
    # estimator = SVC(kernel="rbf")
    # selector = RFECV(estimator, step=1, cv=5)
    # selector = selector.fit(X, y)
    # for idx, rank in enumerate(selector.ranking_):
    #     if rank == 1:
    #         print(header[idx], rank)
    #         if idx >= n_features_pet and idx < n_features_pet + n_features_ct:
    #             best_features_rbf.append("ct_" + header[idx])
    #         else:
    #             best_features_rbf.append(header[idx])
    # with open(os.path.join(res_dir, "RFE_rbf_svc_%s.csv" % feature_set), 'w') as writer_file:
    #     writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    #     writer.writerow(best_features_rbf)

    # extra trees
    best_features_et = []
    estimator = ExtraTreesClassifier(n_estimators=150)
    selector = RFECV(estimator, step=1, cv=5)
    selector = selector.fit(X, y)
    for idx, rank in enumerate(selector.ranking_):
        if rank == 1:
            print(header[idx], rank)
            if idx >= n_features_pet and idx < n_features_pet + n_features_ct:
                best_features_et.append("ct_" + header[idx])
            else:
                best_features_et.append(header[idx])
    with open(os.path.join(res_dir, "RFE_extra_trees_svc_%s.csv" % feature_set), 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(best_features_et)

    # random forest
    best_features_rf = []
    estimator = RandomForestClassifier(n_estimators=150)
    selector = RFECV(estimator, step=1, cv=5)
    selector = selector.fit(X, y)
    for idx, rank in enumerate(selector.ranking_):
        if rank == 1:
            print(header[idx], rank)
            if idx >= n_features_pet and idx < n_features_pet + n_features_ct:
                best_features_rf.append("ct_" + header[idx])
            else:
                best_features_rf.append(header[idx])
    with open(os.path.join(res_dir, "RFE_random_forest_svc_%s.csv" % feature_set), 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(best_features_rf)