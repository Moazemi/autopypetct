#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 2 13:23:13 2018

@author: sobhan
"""

# imports
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold, LeaveOneOut, KFold
from sklearn.feature_selection import RFECV
from sklearn.datasets import make_classification
import csv
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import normalize, scale
from sklearn.metrics import roc_curve, auc
from scipy import interp
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from itertools import cycle
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import itertools
import pandas as pd
from sklearn import preprocessing


# pipeline control inputs
do_loo = False
do_kfold = False
do_loo_subject = True
show_interim_rocs = False
do_multilabel_classification = False
do_draw_feature_importances = False
do_grid_search = False

# input paths
header_path = "./csv/2020/heads/head_best_rad.csv"
train_path = "./csv/2020/train_psa32.csv"
multi_path = "./csv/multi.csv"

thres = 0.749

# import feature names
header = []
with open(header_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header = next(reader)
    for column in header:
        print (column)

# import trainig data
data = []
labels = []
with open(train_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        data.append([row[column] for column in header])
        labels.append(int(row['label']))

X, y = np.array(data), np.array(labels)
X = scale(X)

# initialize cross validation kernels
loo = LeaveOneOut()
print (loo.get_n_splits(X))
kf = KFold(n_splits=20, shuffle=True)
print (kf.get_n_splits(X))
n_classes = 2
n_samples, n_features = X.shape



######################################
# helper functions
######################################    
# draw the ROC curves
def plot_roc_legend():
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        plt.show()
        
# draw feature importances diagrams          
def feauture_importances(coef, std, names, title):
    names = np.array(names)
    coef = np.array(coef)
    imp = np.absolute(coef)
    imp, std, names = zip(*sorted(zip(imp, std, names)))
    print ('INSIDE feauture_importances', coef, names)
    plt.title("Feature importances for %s classifier" % title)
    plt.barh(range(0,len(names)), imp, xerr=std, align='center')
    plt.yticks(range(0,len(names)), names)
    plt.show()

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

######################################
# start leave one out 
######################################    
if do_loo:
    scores_svc = [] # linear svc
    scores_tree = [] # extra trees
    scores_rf = [] # random forest
    
    predict_svc = [] # linear svc
    predict_tree = [] # extra trees
    predict_rf = [] # random forest
    
    
    for train_index, test_index in loo.split(X):    
        #print("TEST sample index:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
        # linear svc classifier
        clf = SVC(kernel='linear')
        clf.fit(X_train, y_train)
        scores_svc.append(clf.score(X_test, y_test))
        predict_svc.append(clf.decision_function(X_test))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(n_estimators=250, random_state=0)
        trees.fit(X_train, y_train)
        scores_tree.append(trees.score(X_test, y_test))
        predict_tree.append(trees.predict_proba(X_test))
    
    	# random forest classifierfrom sklearn.tree import DecisionTreeClassifier
        forest = RandomForestClassifier(n_estimators=250, random_state=0)
        forest.fit(X_train, y_train)
        scores_rf.append(forest.score(X_test, y_test))
        predict_rf.append(forest.predict_proba(X_test))
    	
    
    scores_svc = np.array(scores_svc)
    scores_tree = np.array(scores_tree)
    scores_rf = np.array(scores_rf)
    predict_svc = np.array(predict_svc)
    predict_tree = np.array(predict_tree)
    predict_rf = np.array(predict_rf)
    
    print (predict_svc.shape, predict_tree.shape, predict_rf.shape)
    
    #print (scores_svc, y)
    print (scores_svc.mean(), scores_tree.mean(), scores_rf.mean())
    #print (predict_svc.mean(), predict_tree.mean(), predict_rf.mean())
    
    # Compute ROC curve and area the curve for linear svc
    fpr, tpr, thresholds = roc_curve(y, predict_svc)
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Linear SVC, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	
    
    
    
    # Compute ROC curve and area the curve for extra tree
    fpr, tpr, thresholds = roc_curve(y, predict_tree[:, :, 1]) 
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Extra Trees, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	
    
    
    # Compute ROC curve and area the curve for extra tree
    fpr, tpr, thresholds = roc_curve(y, predict_rf[:, :, 1])
    print (fpr, tpr)
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='Random Forest, LeaveOneOut (AUC = %0.2f)' % (roc_auc))	



#######################################from sklearn.tree import DecisionTreeClassifier
# start kfold
#######################################
if do_kfold:
    # init roc variables
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    
    tprs_rf = []
    aucs_rf = []
    mean_rf = np.linspace(0, 1, 100)
    
    i = 0
    for train_index, test_index in kf.split(X):
        #print("TEST sample index:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
        # linear svc classifier
        clf = SVC(kernel='linear')
        probas_ = clf.fit(X_train, y_train).decision_function(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        tprs_svc.append(interp(mean_fpr, fpr, tpr))
        tprs_svc[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_svc.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='SVC, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
    
        # extra trees classifier
        trees = ExtraTreesClassifier(n_estimators=250, random_state=0)
        probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_tree.append(interp(mean_fpr, fpr, tpr))
        tprs_tree[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_tree.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Extra Trees, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
    
        # random forest classifier
        forest = RandomForestClassifier(n_estimators=250, random_state=0)
        probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
        tprs_rf.append(interp(mean_fpr, fpr, tpr))
        tprs_rf[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs_rf.append(roc_auc)
        
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Random Forest, ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
    
    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC: Linear SVC, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)
    
    # extra trees classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr, mean_tpr, color='r',
             label=r'Mean ROC: Extra Trees, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)
    
    # random forest classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr, mean_tpr, color='g',
             label=r'Mean ROC: Random Forest, KFold(20) (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, linestyle='--', alpha=.8)

######################################
# start leave one subject out 
######################################    
# init feature importance matrices
if do_loo_subject:
    perm_aucs_lsvc = []
    perm_aucs_psvc = []
    perm_aucs_rbf = []
    perm_aucs_tree = []
    perm_aucs_rf = []
    
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    
    # rbf svc
    tprs_rbf = []
    aucs_rbf = []
    mean_fpr_rbf = np.linspace(0, 1, 100)
    
    
    # extra trees
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    
    # random forest
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100)
    
    n_subjects = 48    
    patient_ids = range(n_subjects) 
    
    num_high_scores = 0
    
    
    train_data_all = []
    test_data_all = []
    train_labels_all = []
    test_labels_all = []
    for i in patient_ids:
        print (i)
          
        # init data containers
        train_data = []
        test_data = []
        train_labels = []
        test_labels = []
        
        
        # import data
        with open(train_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            ind = 0
            for row in reader:  
                if i != int(row['patient_id']):
                    train_data.append([row[column] for column in header])
                    train_labels.append(labels[ind])
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(labels[ind])
                ind += 1
        train_data_all.append(train_data)
        test_data_all.append(test_data)
        train_labels_all.append(train_labels)
        test_labels_all.append(test_labels)
    
    for itr in range(100):
    # init subject based variables
        print (itr)
        labels = np.array(labels)
        np.random.shuffle(labels)
        # init roc variables
        # linear svc
        # extra trees
        tprs_tree = []
        aucs_tree = []
        mean_fpr_tree = np.linspace(0, 1, 100)
        
        for i in patient_ids:
#            print (i)
            X_train, y_train = np.asarray(train_data_all[i], dtype=np.float64), np.asarray(train_labels_all[i])
            mean = np.mean(X_train, axis=0)        
            std = np.std(X_train, axis=0)      
            
            

    
            # set test parameters
            X_test, y_test = np.asarray(test_data_all[i], dtype=np.float64), np.asarray(test_labels_all[i])
            
            # normalize test data
#            X_test = np.subtract(X_test, mean)
#            X_test = np.divide(X_test, std * std)
# normalize train data
#            X_train = scale(X_train)
#            scalerObj = preprocessing.MinMaxScaler()
#            scaler = scalerObj.fit(X_train)
#            X_train = scaler.transform(X_train)
#            X_test = scaler.transform(X_test)
            
            # start permutation
            np.random.shuffle(y_train)
            np.random.shuffle(y_test)
            
            
            # linear svc classifier
            clf = SVC(kernel='linear', gamma='auto').fit(X_train, y_train)
            probas_ = clf.decision_function(X_test)
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y_test, probas_)
            tprs_svc.append(interp(mean_fpr, fpr, tpr))
            tprs_svc[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            if roc_auc > 0.849:
                num_high_scores += 1
                print ("FOUND")
            perm_aucs_lsvc.append(roc_auc)
            aucs_svc.append(roc_auc)
            
            # polynomial svc classifier        
            poly = SVC(kernel='poly', degree=2, C=1.0, gamma='auto').fit(X_train, y_train)
            probas_ = poly.decision_function(X_test)
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y_test, probas_)
            tprs_poly.append(interp(mean_fpr_poly, fpr, tpr))
            tprs_poly[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            if roc_auc > 0.849:
                num_high_scores += 1
            perm_aucs_psvc.append(roc_auc)
            aucs_poly.append(roc_auc)
            
            # rbf svc classifier
            rbf = SVC(kernel='rbf', gamma='auto').fit(X_train, y_train)
            probas_ = rbf.decision_function(X_test)
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y_test, probas_)
            tprs_rbf.append(interp(mean_fpr_rbf, fpr, tpr))
            tprs_rbf[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            if roc_auc > 0.849:
                num_high_scores += 1
                print ("FOUND")
            perm_aucs_rbf.append(roc_auc)
            aucs_rbf.append(roc_auc)
            
            # random forest classifier
            forest = RandomForestClassifier(max_depth=20, min_samples_leaf=1, n_estimators=20, random_state=0)
            probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
            
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
            tprs_rf.append(interp(mean_fpr_rf, fpr, tpr))
            tprs_rf[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            if roc_auc > 0.849:
                num_high_scores += 1
            perm_aucs_rf.append(roc_auc)
            aucs_rf.append(roc_auc)
            
        
            # extra trees classifier
            trees = ExtraTreesClassifier(n_estimators=20, random_state=0)
            probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
            tprs_tree.append(interp(mean_fpr_tree, fpr, tpr))
            tprs_tree[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            if roc_auc > 0.849:
                num_high_scores += 1
            perm_aucs_tree.append(roc_auc)
            aucs_tree.append(roc_auc)
    
        
        
        # linear svc classifier
        mean_tpr = np.mean(tprs_svc, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        if mean_auc > thres:
                num_high_scores += 1
        perm_aucs_lsvc.append(mean_auc)
        
        
        # polynomial svc classifier
        mean_tpr = np.mean(tprs_poly, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr_poly, mean_tpr)
        if mean_auc > thres:
                num_high_scores += 1
        perm_aucs_psvc.append(mean_auc)
        
         # rbf svc classifier
        mean_tpr = np.mean(tprs_rbf, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr_rbf, mean_tpr)
        if mean_auc > thres:
                num_high_scores += 1
        perm_aucs_rbf.append(mean_auc)
        
        # extra trees classifier
        mean_tpr = np.mean(tprs_tree, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr_tree, mean_tpr)        
        if mean_auc > thres:
                num_high_scores += 1
        perm_aucs_tree.append(mean_auc)
        print ("Permutation test no. %d, mean_auc: %0.2f" % (itr, mean_auc))
        
        # random forest classifier
        mean_tpr = np.mean(tprs_rf, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr_rf, mean_tpr)
        if mean_auc > thres:
                num_high_scores += 1
        perm_aucs_rf.append(mean_auc)
        
    perm_mean = np.mean(perm_aucs_lsvc, axis=0)
    perm_std = np.std(perm_aucs_lsvc, axis=0)
    print ("Permutation final score for linear svc: mean: %0.2f, std: %0.2f" % (perm_mean, perm_std))
    # the histogram of the data
    n, bins, patches = plt.hist(perm_aucs_lsvc, 50, normed=True, facecolor='g', alpha=0.75)
    
    plt.xlabel('AUC')
    plt.ylabel('Count')
    plt.title('Histogram of AUCs')
    plt.axis([0, 1, 0, 50])
    plt.grid(True)
    plt.show()
    
    perm_mean = np.mean(perm_aucs_psvc, axis=0)
    perm_std = np.std(perm_aucs_psvc, axis=0)
    print ("Permutation final score for polynomial svc: mean: %0.2f, std: %0.2f" % (perm_mean, perm_std))
    # the histogram of the data
    n, bins, patches = plt.hist(perm_aucs_psvc, 50, normed=True, facecolor='g', alpha=0.75)
    
    plt.xlabel('AUC')
    plt.ylabel('Count')
    plt.title('Histogram of AUCs')
    plt.axis([0, 1, 0, 50])
    plt.grid(True)
    plt.show()
    
    perm_mean = np.mean(perm_aucs_rbf, axis=0)
    perm_std = np.std(perm_aucs_rbf, axis=0)
    print ("Permutation final score for linear svc: mean: %0.2f, std: %0.2f" % (perm_mean, perm_std))
    # the histogram of the data
    n, bins, patches = plt.hist(perm_aucs_rbf, 50, normed=True, facecolor='g', alpha=0.75)
    
    plt.xlabel('AUC')
    plt.ylabel('Count')
    plt.title('Histogram of AUCs')
    plt.axis([0, 1, 0, 50])
    plt.grid(True)
    plt.show()
    
    perm_mean = np.mean(perm_aucs_tree, axis=0)
    perm_std = np.std(perm_aucs_tree, axis=0)
    print ("Permutation final score for extra trees: mean: %0.2f, std: %0.2f" % (perm_mean, perm_std))
    # the histogram of the data
    n, bins, patches = plt.hist(perm_aucs_tree, 50, normed=True, facecolor='g', alpha=0.75)
    
    plt.xlabel('AUC')
    plt.ylabel('Count')
    plt.title('Histogram of AUCs')
    plt.axis([0, 1, 0, 50])
    plt.grid(True)
    plt.show()
    
    perm_mean = np.mean(perm_aucs_rf, axis=0)
    perm_std = np.std(perm_aucs_rf, axis=0)
    print ("Permutation final score for random forest: mean: %0.2f, std: %0.2f" % (perm_mean, perm_std))
    # the histogram of the data
    n, bins, patches = plt.hist(perm_aucs_rf, 50, normed=True, facecolor='g', alpha=0.75)
    
    plt.xlabel('AUC')
    plt.ylabel('Count')
    plt.title('Histogram of AUCs')
    plt.axis([0, 1, 0, 50])
    plt.grid(True)
    plt.show()
    print ("number of high scores: ", num_high_scores)
    print ("number of iterations: ", 24000)
    print ("p-value of permutation: %0.6f" % float(float(num_high_scores)/24000.0))
    
        
    
        
######################################
# mulitable classification
######################################    

def get_label(id):
        if id == 0: return "Bone"
        elif id == 1: return "Lymph node"
        elif id == 2: return "Kidney"
        elif id == 3: return "Liver"
        elif id == 4: return "Prostate"
        elif id == 5: return "Bladder"
        elif id == 6: return "Lung"
        elif id == 7: return "Spleen"
        elif id == 8: return "Sal. gland"
        elif id == 9: return "Other tissue"


# init feature importance matrices
feature_importances_lsvc = []
feature_importances_ada = []
if do_multilabel_classification:    
    conf_mat = []
    cnfs = []
    cnfs_rf = []
    n_classes = 10
    n_subjects = 30
    # init roc variables
    # linear svc
    tprs_svc = []
    aucs_svc = []
    mean_fpr = np.linspace(0, 1, 100)
    # polynomial svc
    tprs_poly = []
    aucs_poly = []
    mean_fpr_poly = np.linspace(0, 1, 100)
    # adaboost
    tprs_ada = []
    aucs_ada = []
    mean_fpr_ada = np.linspace(0, 1, 100)
    # rf
    tprs_rf = []
    aucs_rf = []
    mean_fpr_rf = np.linspace(0, 1, 100)
    # et
    tprs_tree = []
    aucs_tree = []
    mean_fpr_tree = np.linspace(0, 1, 100)
    
    clfs = []
    y_scores = []
#    fpr = []
#    tpr = []
#    roc_auc = []
    clf_names = ['linear SVC', 'poly SVC', 'AdaBoost']
    clfs.append(OneVsRestClassifier(SVC(kernel='linear', probability=True)))
    clfs.append(OneVsRestClassifier(SVC(kernel='poly', degree=2, C=1.0)))
    clfs.append(OneVsRestClassifier(AdaBoostClassifier()))
    
    
    
    
    patient_ids = range(n_subjects) 
    for pid in patient_ids:
        print (pid)
    
        # init data containers
        train_data = []
        test_data = []
        labels = []
        test_labels = []
        # import data
        with open(multi_path, 'rt') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for row in reader:  
                if pid != int(row['patient_id']):
                    train_data.append([row[column] for column in header])
                    labels.append(int(row['category']))
                else:
                    test_data.append([row[column] for column in header])
                    test_labels.append(int(row['category']))

        # set train parameters
        X_train, y_train = np.asarray(train_data, dtype=np.float64), np.asarray(labels)
        mean = np.mean(X_train, axis=0)        
        std = np.std(X_train, axis=0)        
        
        # normalize train data
        X_train = scale(X_train)

        # set test parameters
        X_test, y_test = np.asarray(test_data, dtype=np.float64), np.asarray(test_labels)
        
        y_test_b = label_binarize(y_test, classes=range(n_classes))
        y_train_b = label_binarize(y_train, classes=range(n_classes))
        
        
        print ("X_test.shape, y_test.shape", X_test.shape, y_test.shape)
        # normalize test data
        X_test = np.subtract(X_test, mean)
        X_test = np.divide(X_test, std * std)
        
        # linear svc classifier
        clf = OneVsRestClassifier(SVC(kernel='linear', probability=True)).fit(X_train, y_train_b)
        probas_ = clf.decision_function(X_test)
        feature_importances_lsvc.append(clf.coef_[0])
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_svc.append(interp(mean_fpr, fpr["micro"], tpr["micro"]))
        tprs_svc[-1][0] = 0.0
        aucs_svc.append(roc_auc["micro"])
       
        if show_interim_rocs:
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='Linear SVC, ROC Leave Subj %d Out (AUC = %0.2f)' % (pid, roc_auc))
            
            
        # polynomial svc classifier
        clf = OneVsRestClassifier(SVC(kernel='poly', degree=2, C=1.0)).fit(X_train, y_train_b)
        probas_ = clf.decision_function(X_test)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_poly.append(interp(mean_fpr_poly, fpr["micro"], tpr["micro"]))
        tprs_poly[-1][0] = 0.0
        aucs_poly.append(roc_auc["micro"])


        # AdaBoostClassifier
        clf = AdaBoostClassifier().fit(X_train, y_train)
        probas_ = clf.decision_function(X_test)
        feature_importances_ada.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test, probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_ada.append(interp(mean_fpr_ada, fpr["micro"], tpr["micro"]))
        tprs_ada[-1][0] = 0.0
        aucs_ada.append(roc_auc["micro"])
        
        
        # RandomForestClassifier        
        clf = RandomForestClassifier(n_estimators=250, criterion = 'entropy', random_state=0).fit(X_train, y_train)
        probas_ = clf.predict_proba(X_test)
        
        # feeding confusion matrix
#        y_pred = clf.predict(X_test)
#        y_pred = np.array(y_pred)
#        class_names = [cat for cat in range(10)]
#        print ("y_test, y_pred", y_test, y_pred)
#        cnf = confusion_matrix(y_test, y_pred, labels=class_names)
#        print ("CNF", cnf.shape, cnf)
#        cnfs.append(cnf)
        
        feature_importances_rf.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_rf.append(interp(mean_fpr_rf, fpr["micro"], tpr["micro"]))
        tprs_rf[-1][0] = 0.0
        aucs_rf.append(roc_auc["micro"])
        
        
        # ExtraTreesClassifier        
        clf = ExtraTreesClassifier(n_estimators=250, criterion = 'entropy', random_state=0).fit(X_train, y_train)
        probas_ = clf.predict_proba(X_test)
        
        # feeding confusion matrix
        y_pred = clf.predict(X_test)
        y_pred = np.array(y_pred)
        class_names = [cat for cat in range(10)]
        print ("y_test, y_pred", y_test, y_pred)
        cnf = confusion_matrix(y_test_b[:, i], y_pred[:, i], labels=class_names)
        print ("CNF", cnf.shape, cnf)
        cnfs.append(cnf)
        
        
        feature_importances_tree.append(clf.feature_importances_)
        # Compute ROC curve and area the curve
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test_b[:, i], probas_[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_b.ravel(), probas_.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        tprs_tree.append(interp(mean_fpr_tree, fpr["micro"], tpr["micro"]))
        tprs_tree[-1][0] = 0.0
        aucs_tree.append(roc_auc["micro"])

            
#    # linear svc classifier
    mean_tpr = np.mean(tprs_svc, axis=0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr, mean_tpr, color='r',
             label=r'Mean ROC: %s, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (clf_names[0], mean_auc, std_auc),
             lw=2, alpha=.8)
    
    # polynomial svc classifier
    mean_tpr = np.mean(tprs_poly, axis=0)
    mean_auc = auc(mean_fpr_poly, mean_tpr)
    std_auc = np.std(aucs_svc)
    plt.plot(mean_fpr_poly, mean_tpr, color='g',
             label=r'Mean ROC: Polynomial SVC, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)


    # adaboost classifier
    mean_tpr = np.mean(tprs_ada, axis=0)
    mean_auc = auc(mean_fpr_ada, mean_tpr)
    std_auc = np.std(aucs_ada)
    plt.plot(mean_fpr_ada, mean_tpr, color='b',
             label=r'Mean ROC: AdaBoost, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    # rf classifier
    mean_tpr = np.mean(tprs_rf, axis=0)
    mean_auc = auc(mean_fpr_rf, mean_tpr)
    std_auc = np.std(aucs_rf)
    plt.plot(mean_fpr_rf, mean_tpr, color='y',
             label=r'Mean ROC: Random forest, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    
    # et classifier
    mean_tpr = np.mean(tprs_tree, axis=0)
    mean_auc = auc(mean_fpr_tree, mean_tpr)
    std_auc = np.std(aucs_tree)
    plt.plot(mean_fpr_tree, mean_tpr, color='cyan',
             label=r'Mean ROC: Extra trees, LeaveOneSubjectOut (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    plt.plot([0, 1], [0, 1], 'k--', lw=1)
    plot_roc_legend()
    
        # draw feature importances?    
    if do_draw_feature_importances:
        mean_fi_lsvc = np.mean(feature_importances_lsvc, axis=0)
        std_fi_lsvc = np.std(feature_importances_lsvc, axis=0)
        feauture_importances(mean_fi_lsvc, std_fi_lsvc, header, "linear SVC")
        mean_fi_ada = np.mean(feature_importances_ada, axis=0)
        std_fi_ada = np.std(feature_importances_ada, axis=0)
        feauture_importances(mean_fi_ada, std_fi_ada, header, "AdaBoost")
        mean_fi_rf = np.mean(feature_importances_rf, axis=0)
        std_fi_rf = np.std(feature_importances_rf, axis=0)
        feauture_importances(mean_fi_rf, std_fi_rf, header, "random forest")
        mean_fi_tree = np.mean(feature_importances_tree, axis=0)
        std_fi_tree = np.std(feature_importances_tree, axis=0)
        feauture_importances(mean_fi_tree, std_fi_tree, header, "extra trees")

    plt.figure()
    class_names = ["bone", "lymph node", "kidney", "liver", "prostate", "bladder", "lung", "spleen", "sal. gland", "other"]
    x = np.zeros_like(cnfs[0])
    for cnf in cnfs:
        x += cnf
    plot_confusion_matrix(x, classes=class_names, normalize=False,
                              title='Normalized confusion matrix')        
    plt.show()

    
if do_grid_search:
    # Split the dataset in two equal parts
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.5, random_state=0)
    
    # Set the parameters by cross-validation
    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                         'C': [1, 10, 100, 1000]},
                        {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
                        {'kernel': ['poly'], 'degree': [2, 3]}]
    
    scores = ['precision', 'recall']
    
    for score in scores:
        print("# Tuning hyper-parameters for %s" % score)
        print()
    
        clf = GridSearchCV(SVC(), tuned_parameters, cv=5,
                           scoring='%s_macro' % score)
        clf.fit(X_train, y_train)
    
        print("Best parameters set found on development set:")
        print()
        print(clf.best_params_)
        print()
        print("Grid scores on development set:")
        print()
        means = clf.cv_results_['mean_test_score']
        stds = clf.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
            print("%0.3f (+/-%0.03f) for %r"
                  % (mean, std * 2, params))
        print()
    
        print("Detailed classification report:")
        print()
        print("The model is trained on the full development set.")
        print("The scores are computed on the full evaluation set.")
        print()
        y_true, y_pred = y_test, clf.predict(X_test)
        print(classification_report(y_true, y_pred))
        print()

