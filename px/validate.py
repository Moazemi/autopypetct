#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 11:22:06 2020

@author: sobhan
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:48:37 2020

@author: sobhan
"""

import matplotlib.pyplot as plt
from sklearn.svm import SVC, SVR
from sklearn.linear_model import LogisticRegression
import numpy as np
import csv
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
import os 

show_interim_rocs = True
do_psa_classification = True
do_combine_pet_ct = True
do_combine_rad_cli = True
do_validate_best_features = False
do_mediso = False

feature_set = "Radiomics+Clinical(FO_Normalized)_THRES" # Radiomics, Clinical, etc.
# feature_set = "RF_Best_Radiomics_Mediso" # Mediso: pet, ct, cli, else.
step_str = "(Validate)" # [Balanced or Unbalanced]

responders_ids_path = "/home/sobhan/data/train/responders"
responders_patient_ids = np.asarray(sorted(os.listdir(responders_ids_path)), np.int)
nonresponders_ids_path = "/home/sobhan/data/train/non_responders"
nonresponders_patient_ids = np.asarray(sorted(os.listdir(nonresponders_ids_path)), np.int)

responders_ids_path_test = "/home/sobhan/data/test/responders"
responders_patient_ids_test = np.asarray(sorted(os.listdir(responders_ids_path_test)), np.int)
nonresponders_ids_path_test = "/home/sobhan/data/test/non_responders"
nonresponders_patient_ids_test = np.asarray(sorted(os.listdir(nonresponders_ids_path_test)), np.int)


# train_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/train_psa_fo_pyrad_ct_thres80.csv"
# header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_ct.csv"

if not do_mediso:
    train_path = "/home/sobhan/data/csv/2021/train_psa_pyrad_fo_unet_pet_99.csv"
    train_path_ct = "/home/sobhan/data/csv/2021/train_psa_pyrad_fo_unet_ct_99.csv"
    # header_path = "/home/sobhan/data/csv/2021/head_pyrad_all_nomalized_pet.csv"
    header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_pet.csv"
    # header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"
    header_path_ct = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_pyrad_ct.csv"
    # header_path_ct = "/home/sobhan/data/csv/2021/head_pyrad_all_nomalized_ct.csv"
    header_path_cli = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"
else:
    train_path = "/home/sobhan/data/csv/2021/train_psa_99_mix_new.csv"
    # header_path = "/home/sobhan/data/csv/2021/head_mix.csv"
    header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/head_cli.csv"

if do_validate_best_features:
    header_path_best = "/home/sobhan/data/2021/res/RFE_logistic_regression_Radiomics+Clinical(All_Normalized)_UNet.csv"

res_dir = "/home/sobhan/data/2021/res"


#test_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/train_psa_pyrad_pet81.csv"
test_path = train_path
if not do_mediso:
    test_path_ct = train_path_ct



# import feature names
def load_header(header_path):
    header = []
    with open(header_path, 'rt') as header_file:
        reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        header = next(reader)
        for i in range(len(header)):
            if header[i] == 'time_dif':
                    header[i] = 'time_def'
    return header

header = load_header(header_path)
if not do_mediso:
    header_ct = load_header(header_path_ct)
    header_cli = load_header(header_path_cli)
if do_validate_best_features:
    header_best = load_header(header_path_best)

# import training data
def load_train_data(train_path, header):
    data = []
    labels = []
    deltaPSA = []
    with open(train_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(row['patient_id']) in nonresponders_patient_ids:
                data.append([float(row[column]) for column in header])
                labels.append(int(row['label']))
                deltaPSA.append(float(row['delta_psa']))
    return data, labels, deltaPSA

# import training data for best features
def load_train_data_best(train_path_pet, train_path_ct, header):
    data = []
    data_ct = []
    labels = []
    deltaPSA = []
    with open(train_path_ct, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(row['patient_id']) in nonresponders_patient_ids:
                _row = []
                for column in header:
                    if 'ct_' in column:
                        column = column[3:]
                        _row.append(float(row[column]))
                data_ct.append(_row)
    with open(train_path_pet, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(
                    row['patient_id']) in nonresponders_patient_ids:
                _row = []
                for column in header:
                    if 'ct_' not in column:
                        _row.append(float(row[column]))
                # data.append([float(row[column]) for column in header])
                data.append(_row)
                labels.append(int(row['label']))
                deltaPSA.append(float(row['delta_psa']))
    if len(data_ct) > 0 and len(data) > 0:
        data = np.concatenate((data, data_ct), axis=1)
    return data, labels, deltaPSA

# import validation data
def load_test_data(test_path, header):
    test_data = []
    test_labels = []
    with open(test_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids_test or int(row['patient_id']) in nonresponders_patient_ids_test:
                test_data.append([row[column] for column in header])
                test_labels.append(int(row['label']))

    return test_data, test_labels

# import test data for best features
def load_test_data_best(test_path_pet, test_path_ct, header):
    data = []
    data_ct = []
    labels = []
    with open(test_path_ct, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids_test or int(row['patient_id']) in nonresponders_patient_ids_test:
                _row = []
                for column in header:
                    if 'ct_' in column:
                        column = column[3:]
                        _row.append(float(row[column]))
                data_ct.append(_row)
    with open(test_path_pet, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids_test or int(
                    row['patient_id']) in nonresponders_patient_ids_test:
                _row = []
                for column in header:
                    if 'ct_' not in column:
                        _row.append(float(row[column]))
                # data.append([float(row[column]) for column in header])
                data.append(_row)
                labels.append(int(row['label']))
    if len(data_ct) > 0 and len(data) > 0:
        data = np.concatenate((data, data_ct), axis=1)
    return data, labels


if do_validate_best_features:
    if not do_mediso:
        data, labels, deltaPSA = load_train_data_best(train_path, train_path_ct, header_best)
        test_data, test_labels = load_test_data_best(test_path, test_path_ct, header_best)
    else:
        data, labels, deltaPSA = load_train_data(train_path, header_best)
        test_data, test_labels = load_test_data(test_path, header_best)
else:
    data, labels, deltaPSA = load_train_data(train_path, header)
    if not do_mediso:
        data_ct, labels, deltaPSA = load_train_data(train_path_ct, header_ct)
        data_cli, labels, deltaPSA = load_train_data(train_path, header_cli)

    test_data, test_labels = load_test_data(test_path, header)
    if not do_mediso:
        test_data_ct, test_labels = load_test_data(test_path_ct, header_ct)
        test_data_cli, test_labels = load_test_data(test_path, header_cli)

if do_combine_pet_ct:
    data = np.concatenate((data, data_ct), axis=1)
    test_data = np.concatenate((test_data, test_data_ct), axis=1)
    if do_combine_rad_cli:
        data = np.concatenate((data, data_cli), axis=1)
        test_data = np.concatenate((test_data, test_data_cli), axis=1)

X, y = np.array(data), np.array(labels)
X_test, y_test = np.array(test_data), np.array(test_labels)
#X = scale(X)
scalerObj = preprocessing.MinMaxScaler()
scaler = scalerObj.fit(X)
X = scaler.transform(X)
X_test = scaler.transform(X_test)


print ("X SHAPE")
print (X.shape)


######################################
# helper functions
######################################    
# draw the ROC curves
def plot_roc_legend(caption=''):
    classification_task = "Hotspot"
    if do_psa_classification:
        classification_task = "PSA"
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.rc('xtick', labelsize=11)
    plt.rc('ytick', labelsize=11)
    plt.title('%s Classification: %s' % (classification_task, caption))
    plt.legend(loc="lower right")
    # plt.savefig("./res/2020/56/Validate_ROC_%s.png" % caption, dpi=300)
    plt.savefig(os.path.join(res_dir, "Validate_ROC_%s.png" % caption), dpi=300)
    plt.close()


X_train, X_test = X, X_test
y_train, y_test = y, y_test

print ("YYYY")
print (np.array(y))
print (np.array(y_test))

# logistic regression
clf = LogisticRegression(penalty='l1', solver='liblinear', random_state=0, class_weight='balanced')
probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
y_pred = clf.predict(X_test)
cnf_lr = confusion_matrix(y_test, y_pred)
# Compute ROC curve and area the curve
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
roc_auc = auc(fpr, tpr)

_fpr = float(cnf_lr[0][1]/(cnf_lr[0][1] + cnf_lr[0][0]))
_tpr = float(cnf_lr[1][1]/(cnf_lr[1][1] + cnf_lr[1][0]))
_tnr = float(cnf_lr[0][0]/(cnf_lr[0][0] + cnf_lr[0][1]))
_fnr = float(cnf_lr[1][0]/(cnf_lr[1][0] + cnf_lr[1][1]))
print ("Logistic Regression: ",_tpr, _tnr)

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='Logistic Regression (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

# linear svc classifier
clf = SVC(kernel='linear', C=1.0, gamma=0.001, class_weight='balanced')
probas_ = clf.fit(X_train, y_train).decision_function(X_test)
y_pred = clf.predict(X_test)
print (np.array(y_pred))
cnf = confusion_matrix(y_test, y_pred)
# Compute ROC curve and area the curve
fpr, tpr, thresholds = roc_curve(y_test, probas_)
roc_auc = auc(fpr, tpr)

fpr = float(cnf[0][1]/(cnf[0][1] + cnf[0][0]))
tpr = float(cnf[1][1]/(cnf[1][1] + cnf[1][0]))
tnr = float(cnf[0][0]/(cnf[0][0] + cnf[0][1]))
fnr = float(cnf[1][0]/(cnf[1][0] + cnf[1][1]))
print ("Linear SVC: ",tpr, tnr) 

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='Linear SVC (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, tpr, tnr))


# polynomial svc classifier
poly = SVC(kernel='poly', C=10.0, degree=3, class_weight='balanced')
probas_ = poly.fit(X_train, y_train).decision_function(X_test)
y_pred = poly.predict(X_test)
cnf_poly = confusion_matrix(y_test, y_pred)
fpr, tpr, thresholds = roc_curve(y_test, probas_)
roc_auc = auc(fpr, tpr)
_fpr = float(cnf_poly[0][1]/(cnf_poly[0][1] + cnf_poly[0][0]))
_tpr = float(cnf_poly[1][1]/(cnf_poly[1][1] + cnf_poly[1][0]))
_tnr = float(cnf_poly[0][0]/(cnf_poly[0][0] + cnf_poly[0][1]))
_fnr = float(cnf_poly[1][0]/(cnf_poly[1][0] + cnf_poly[1][1]))
print ("Polynimal SVC: ",tpr, tnr) 

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='Polynomial SVC (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

# rbf svc classifier
rbf = SVC(kernel='rbf', C=1.0, gamma=0.125, class_weight='balanced')
probas_ = rbf.fit(X_train, y_train).decision_function(X_test)
y_pred = rbf.predict(X_test)
cnf_rbf = confusion_matrix(y_test, y_pred)
fpr, tpr, thresholds = roc_curve(y_test, probas_)
roc_auc = auc(fpr, tpr)

_fpr = float(cnf_rbf[0][1]/(cnf_rbf[0][1] + cnf_rbf[0][0]))
_tpr = float(cnf_rbf[1][1]/(cnf_rbf[1][1] + cnf_rbf[1][0]))
_tnr = float(cnf_rbf[0][0]/(cnf_rbf[0][0] + cnf_rbf[0][1]))
_fnr = float(cnf_rbf[1][0]/(cnf_rbf[1][0] + cnf_rbf[1][1]))
print ("RBF SVC: ",_tpr, _tnr) 

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='RBF SVC (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

# extra trees classifier
trees = ExtraTreesClassifier(max_depth=10, min_samples_leaf=10, n_estimators=150, class_weight='balanced')
probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
y_pred = trees.predict(X_test)
cnf_et = confusion_matrix(y_test, y_pred)
# Compute ROC curve and area the curve
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
roc_auc = auc(fpr, tpr)

_fpr = float(cnf_et[0][1]/(cnf_et[0][1] + cnf_et[0][0]))
_tpr = float(cnf_et[1][1]/(cnf_et[1][1] + cnf_et[1][0]))
_tnr = float(cnf_et[0][0]/(cnf_et[0][0] + cnf_et[0][1]))
_fnr = float(cnf_et[1][0]/(cnf_et[1][0] + cnf_et[1][1]))
print ("Extra Trees: ",_tpr, _tnr) 

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='Extra Trees (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

# random forest classifier
forest = RandomForestClassifier(max_depth=20, min_samples_leaf=10, n_estimators=150, class_weight='balanced')
probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
y_pred = forest.predict(X_test)
cnf_rf = confusion_matrix(y_test, y_pred)# Compute ROC curve and area the curve
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
roc_auc = auc(fpr, tpr)
_fpr = float(cnf_rf[0][1]/(cnf_rf[0][1] + cnf_rf[0][0]))
_tpr = float(cnf_rf[1][1]/(cnf_rf[1][1] + cnf_rf[1][0]))
_tnr = float(cnf_rf[0][0]/(cnf_rf[0][0] + cnf_rf[0][1]))
_fnr = float(cnf_rf[1][0]/(cnf_rf[1][0] + cnf_rf[1][1]))
print ("Random Forest: ",_tpr, _tnr) 

if show_interim_rocs:
    plt.plot(fpr, tpr, lw=2, alpha=0.8,
         label='Random Forest (AUC = %0.2f, SE = %0.2f, SP = %0.2f)' % (roc_auc, _tpr, _tnr))

plt.plot([0, 1], [0, 1], 'k--', lw=1)
plot_roc_legend(feature_set+step_str)
