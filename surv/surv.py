#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 16:17:04 2020

@author: sobhan
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 10:31:58 2020

@author: sobhan
"""

import matplotlib.pyplot as plt
#from sksurv.datasets import load_whas500
from sksurv.ensemble import RandomSurvivalForest
from sksurv.util import Surv
import csv
from sklearn import preprocessing
import numpy as np
from sksurv.linear_model import CoxPHSurvivalAnalysis
import pandas as pd

#X, y = load_whas500()
#X = X.astype(float)

#print (y)

header_path = "./csv/2020/orig/survival/head_med_new.csv"
header_num_cli_path = "./csv/2020/orig/survival/head_num_cli.csv"
train_path = "./csv/2020/orig/survival/train_surv56.csv"
test_path = "./csv/2020/orig/survival/test_surv27.csv"
train_sig_path = "./csv/2020/orig/survival/rad_sig56_rad.csv"
test_sig_path = "./csv/2020/orig/survival/rad_sig27_rad.csv"

#best_lasso = ["Deviation","Min","Sum","Contrast","Long Run Low Grey-Level Emphasis","CT_Min","CT_Contrast","CT_Kurtosis","CT_ShortRunLowGrey-LevelEmphasis"]
best_lasso = ["Deviation","Min","Sum","Long Run Low Grey-Level Emphasis", "CT_Max", "CT_Min","CT_Contrast","CT_Kurtosis", "CT_LongZoneEmphasis","CT_ShortRunLowGrey-LevelEmphasis"]
do_validate = True
do_feature_ranking = False
do_lasso = False

# import feature names
header = []
with open(header_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header = next(reader)
#    for column in header:
#        print (column)

# import numerical clinical params names
header_num_cli = []
with open(header_num_cli_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header_num_cli = next(reader)

# import radiomics signature of training set
sig_train = []
with open(train_sig_path, 'rt') as sig_train_file:
    reader = csv.DictReader(sig_train_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)    
    for row in reader:
#        print (row)
        sig_train.append(row['x'])
#print (sig_train)


# import radiomics signature of test set
sig_test = []
with open(test_sig_path, 'rt') as sig_test_file:
    reader = csv.DictReader(sig_test_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)    
    for row in reader:
        sig_test.append(row['x'])

# import trainig data
y_train = []
# init data containers
train_data = []
test_data = []
train_dead = []
test_dead = []
train_survival = []
test_survival = []

train_lasso = []
test_lasso = []

train_num_cli = []
test_num_cli = []

with open(train_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        train_data.append([row[column] for column in np.unique(header)])                                    
        train_dead.append(int(row['dead']))
        train_survival.append(int(row['survival']))
        train_lasso.append([row[column] for column in best_lasso])
        train_num_cli.append([row[column] for column in header_num_cli])
        
        

y_train = Surv.from_arrays(train_dead, train_survival)

with open(test_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:
        test_data.append([row[column] for column in np.unique(header)])            
        test_dead.append(int(row['dead']))
        test_survival.append(int(row['survival']))
        test_lasso.append([row[column] for column in best_lasso])
        test_num_cli.append([row[column] for column in header_num_cli])

y_test = Surv.from_arrays(test_dead, test_survival)

def fit_and_score_features(X, y, estimator='cox'):
#    print ('Estimator:')
#    print (estimator)
    n_features = X.shape[1]
    scores = np.empty(n_features)
    if estimator=='cox':
        m = CoxPHSurvivalAnalysis(alpha=0.1)
    else:
        m = RandomSurvivalForest()
    for j in range(n_features):
        Xj = X[:, j:j+1]
        m.fit(Xj, y)
        scores[j] = m.score(Xj, y)
    return scores


def fit_and_score_signature(X, y, estimator='cox'):
    if estimator=='cox':
        m = CoxPHSurvivalAnalysis(alpha=0.1)
    else:
        m = RandomSurvivalForest()        
    X = X.reshape(-1,1)
    m.fit(X, y)        
    return m.score(X, y)


        


X_train = np.asarray(train_data, dtype=np.float64)
y_train = Surv.from_arrays(train_dead, train_survival)
mean = np.mean(X_train, axis=0)        
std = np.std(X_train, axis=0)        
    

# set test parameters
X_test = np.asarray(test_data, dtype=np.float64)
y_test = Surv.from_arrays(test_dead, test_survival)


sig_train = np.asarray(sig_train, dtype=np.float64)
sig_test = np.asarray(sig_test, dtype=np.float64)

train_lasso = np.asarray(train_lasso, dtype=np.float64)
test_lasso = np.asarray(test_lasso, dtype=np.float64)

train_num_cli = np.asarray(train_num_cli, dtype=np.float64)
test_num_cli = np.asarray(test_num_cli, dtype=np.float64)


print ("DEBUG_LASSO")
print (train_lasso.shape)
print (test_lasso.shape)
print (X_train.shape)
print (X_test.shape)

    
# normalize test data
#        X_test = np.subtract(X_test, mean)
#        X_test = np.divide(X_test, std * std)
scalerObj = preprocessing.MinMaxScaler()
scaler = scalerObj.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)   
   
        
X = X_train
y = y_train
print(X.shape)

if do_validate:
    scalerObj1 = preprocessing.MinMaxScaler()
    scaler1 = scalerObj1.fit(train_lasso)
    train_lasso = scaler1.transform(train_lasso)
    test_lasso = scaler1.transform(test_lasso)
    
    scalerObj2 = preprocessing.MinMaxScaler()
    scaler2 = scalerObj2.fit(sig_train.reshape(-1,1))
    sig_train = scaler2.transform(sig_train.reshape(-1,1))
    sig_test = scaler2.transform(sig_test.reshape(-1,1))
        
    scalerObj3 = preprocessing.MinMaxScaler()
    scaler3 = scalerObj3.fit(train_num_cli)
    train_num_cli = scaler3.transform(train_num_cli)
    test_num_cli = scaler3.transform(test_num_cli)
    
    
    estimator = RandomSurvivalForest().fit(X, y)
    print ("RandomSurvivalForest")
    print ("validate", estimator.score(X_test,y_test))
    score_ci = estimator.score(X_test,y_test)
    scores_fi = fit_and_score_features(X_test, y_test, estimator='rfs')
#    print ("train", pd.Series(scores_fi, index=np.unique(header)).sort_values(ascending=False))
    print ("CV")
    print (estimator.score(X_train,y_train))
    
    
    df = pd.Series(scores_fi, index=np.unique(header)).sort_values(ascending=False)
    print ("CoxPHSurvivalAnalysis")
    print ("validate", CoxPHSurvivalAnalysis(alpha=0.1).fit(X_train, y_train).score(X_test, y_test))
    scores_fi = fit_and_score_features(X_test, y_test, estimator='cox')
#    print ("train", pd.Series(scores_fi, index=np.unique(header)).sort_values(ascending=False))
    print ("CV")
    print (estimator.score(X_train,y_train))
    
    print ("LASSO") 
    print ("validate RSF", RandomSurvivalForest().fit(train_lasso, y_train).score(test_lasso, y_test))
    print ("CV RSF", RandomSurvivalForest().fit(train_lasso, y_train).score(train_lasso, y_train))    
    print ("validate COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(train_lasso, y_train).score(test_lasso, y_test))
    print ("CV COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(train_lasso, y_train).score(train_lasso, y_train))
#    print ("train RSF", pd.Series(fit_and_score_features(np.array(train_lasso), y_train, estimator='rfs'), index=best_lasso).sort_values(ascending=False))
#    print ("train COX", pd.Series(fit_and_score_features(np.array(train_lasso), y_train, estimator='cox'), index=best_lasso).sort_values(ascending=False))
    
    print ("Num. CLINICAL") 
    print ("validate RSF", RandomSurvivalForest().fit(train_num_cli, y_train).score(test_num_cli, y_test))
    print ("CV RSF", RandomSurvivalForest().fit(train_num_cli, y_train).score(train_num_cli, y_train))
    print ("validate COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(train_num_cli, y_train).score(test_num_cli, y_test))
    print ("CV COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(train_num_cli, y_train).score(train_num_cli, y_train))
    
#    print ("train RSF", pd.Series(fit_and_score_features(np.array(train_num_cli), y_train, estimator='rfs'), index=header_num_cli).sort_values(ascending=False))
#    print ("train COX", pd.Series(fit_and_score_features(np.array(train_num_cli), y_train, estimator='cox'), index=header_num_cli).sort_values(ascending=False))
    
    print ("RADIOMICS_SIGNATURE") 
    print ("validate RSF", RandomSurvivalForest().fit(sig_train.reshape(-1,1), y_train).score(sig_test.reshape(-1,1), y_test))
    print ("CV RSF", RandomSurvivalForest().fit(sig_train.reshape(-1,1), y_train).score(sig_train.reshape(-1,1), y_train))
    print ("validate COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(sig_train.reshape(-1,1), y_train).score(sig_test.reshape(-1,1), y_test))
    print ("CV COX", CoxPHSurvivalAnalysis(alpha=0.1).fit(sig_train.reshape(-1,1), y_train).score(sig_train.reshape(-1,1), y_train))
    
#    print ("train RSF", fit_and_score_signature(np.array(sig_train), y_train, estimator='rfs'))
#    print ("train COX", fit_and_score_signature(np.array(sig_train), y_train, estimator='cox'))


if do_feature_ranking:
    from sklearn.feature_selection import SelectKBest
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import KFold
    
    pipe = Pipeline([('select', SelectKBest(fit_and_score_features, k=3)),
                     ('model', CoxPHSurvivalAnalysis())])
    from sklearn.model_selection import GridSearchCV
    param_grid = {'select__k': np.arange(1, X.shape[1] + 1)}
    cv = KFold(n_splits=3, random_state=1, shuffle=True)
    gcv = GridSearchCV(pipe, param_grid, return_train_score=True, cv=cv)
    gcv.fit(X, y)
    df = pd.DataFrame(gcv.cv_results_).sort_values(by='mean_test_score', ascending=False)
    print (df)
    df.to_csv("./csv/2020/orig/survival/feature_selection56_rad.csv", index=False)
    pipe.set_params(**gcv.best_params_)
    pipe.fit(X, y)
    
    transformer, final_estimator = [s[1] for s in pipe.steps]
    print ("DEBUGG")
    print (transformer.get_support())
    df = pd.Series(final_estimator.coef_, index=np.array(header)[transformer.get_support()])
    print (df)
    df.to_csv("./csv/2020/orig/survival/feature_selection56_final_rad.csv", index=True)
    
if do_lasso:
    from sksurv.linear_model import CoxnetSurvivalAnalysis
    estimator = CoxnetSurvivalAnalysis(l1_ratio=0.99, fit_baseline_model=True)
    estimator.fit(X, y)
    chf_funcs = {}
    for alpha in estimator.alphas_[:5]:
        chf_funcs[alpha] = estimator.predict_cumulative_hazard_function(
                X_test, alpha=alpha)
    for alpha, chf_alpha in chf_funcs.items():
        for fn in chf_alpha:
            plt.step(fn.x, fn(fn.x), where="post",
                     label="alpha = {:.3f}".format(alpha))
    plt.ylim(0, 1)
    plt.legend()
    plt.show()