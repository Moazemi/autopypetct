#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 13:21:33 2020

@author: sobhan
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:48:37 2020

@author: sobhan
"""

import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
import numpy as np
import csv
from sklearn import preprocessing
from sksurv.ensemble import RandomSurvivalForest
from sksurv.util import Surv
from sksurv.linear_model import CoxPHSurvivalAnalysis
from sksurv.preprocessing import OneHotEncoder

feature_set = "Best-Radiomics" # Radiomics, Clinical, etc.
step_str = " (3-Fold CV-Unbalanced)" # Balanced or Unbalanced


train_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/survival/train_surv83.csv"
header_path = "/home/sobhan/miniconda3/envs/mypy2/src/python-pet-ct-lesion-detection/csv/2020/orig/survival/head_cli.csv"





# import feature names
header = []
with open(header_path, 'rt') as header_file:
    reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    header = next(reader)
    for i in range(len(header)):
        if header[i] == 'time_dif':
                header[i] = 'time_def'
#        print (column)

# import training data
data = []
labels = []
dead = []
survival = []
with open(train_path, 'rt') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for row in reader:                
        data.append([float(row[column]) for column in np.unique(header)])
        labels.append(int(row['label']))
        dead.append(int(row['dead']))
        survival.append(int(row['survival']))

y = Surv.from_arrays(dead, survival)

X = np.array(data)

kf = KFold(n_splits=3, shuffle=True, random_state=2)
print (kf.get_n_splits(X))
scores = []
scores_cox = []
for train_index, test_index in kf.split(X, y):
    #print("TEST sample index:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # normalize data    
    scalerObj = preprocessing.MinMaxScaler()
    scaler = scalerObj.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    
    scores = []
    old_tnr = 0
    for max_depth in [1, 5, 10, 15, 20, 25, 30]:
        for min_samples_leaf in [1, 2, 4, 6, 8, 10]:
            clf = RandomSurvivalForest(max_depth=max_depth, min_samples_leaf=min_samples_leaf, n_estimators=150).fit(X_train, y_train)
            scores.append([clf.score(X_test, y_test), max_depth, min_samples_leaf])
#    score, max_depth, min_samples_leaf = np.max(scores, axis=0)
#    clf = RandomSurvivalForest(max_depth=int(max_depth), min_samples_leaf=int(min_samples_leaf)).fit(X_train, y_train)
#    scores_rf.append([clf.score(X_test, y_test), max_depth, min_samples_leaf])
    print (X_train)
    with open("./csv/2020/orig/survival/train_test56.csv", 'w') as writer_file:
        writer = csv.writer(writer_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        writer.writerows(X_train)
#    X_train = OneHotEncoder().fit(X_train)
#    np.is
    clf = CoxPHSurvivalAnalysis(alpha=0.1, verbose=10).fit(X_train, y_train)
    scores_cox.append(clf.score(X_test, y_test))
    
    

# random survival forest 
# rsf
score, max_depth, min_sample_leaf = np.max(scores, axis=0)
print ('RSF')
print (score, max_depth, min_sample_leaf )
print ('Cox')
print (np.max(scores_cox))
