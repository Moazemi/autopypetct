
import os
import numpy as np
import cv2
import tensorflow as tf
from tensorflow.keras.utils import CustomObjectScope
from tqdm import tqdm
from data import load_predict_data, tf_dataset, tf_dataset_dual, tf_dataset_multi
from train import iou, dice
import pandas as pd
from tensorflow.keras.layers import *

def read_image(path):
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = x/255.0
    x = np.expand_dims(x, axis=-1)
    return x

def read_thres_image(path):
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = np.expand_dims(x, axis=-1)
    x = x/255.0
    return x

def read_mask(path):
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = np.expand_dims(x, axis=-1)
    return x

def mask_parse(mask):
    mask = np.squeeze(mask)
    mask = [mask, mask, mask]
    mask = np.transpose(mask, (1, 2, 0))
    return mask

if __name__ == "__main__":
    ## Dataset
    data_path = "/home/sobhan/data/dataset/"
    pyrad_keys = []
    pyrad_results = []
    pyrad_results_ct = []

    test_paths = []
    test_paths.append("/home/sobhan/data/train/responders")
    test_paths.append("/home/sobhan/data/train/non_responders")
    test_ids = []
    for dest_path in test_paths:
        for id, patient_id in enumerate(sorted(os.listdir(dest_path))):
            test_ids.append(str(patient_id))

    for patient_id in test_ids:
        path = os.path.join(data_path, patient_id)
        print (path)
        batch_size = 16
        (test_x, test_x_ct, test_y, test_th, test_bones) = load_predict_data(path)

        # Predict for single channel input
        # test_dataset = tf_dataset(test_x, test_y, batch=batch_size)

        # Predict for dual channel input
        test_dataset = tf_dataset_dual(test_x, test_x_ct, test_y, batch=batch_size)

        # Predict for multi channel input
        # test_dataset = tf_dataset_multi(test_x, test_x_ct, test_bones, test_y, batch=batch_size)

        test_steps = (len(test_x)//batch_size)
        if len(test_x) % batch_size != 0:
            test_steps += 1

        lr = 0.001  # 1e-4
        opt = tf.keras.optimizers.Adam(lr)
        with CustomObjectScope({'dice': dice}):
            metrics = ["acc", tf.keras.metrics.Recall(), tf.keras.metrics.Precision(), dice]
            model = tf.keras.models.load_model("/home/sobhan/src/autopypetct/useg/files/model_NEW_PETCT_binary_cross_entropy.h5")
            model.compile(loss="binary_crossentropy", optimizer=opt, metrics=metrics)
            try:
                model.evaluate(test_dataset, steps=test_steps)
            except Exception as e:
                print(e)
        preds = model.predict(test_dataset, steps=test_steps)

        images = []
        images_ct = []
        for i, (x, x_ct, y, th) in tqdm(enumerate(zip(test_x, test_x_ct, test_y, test_th)), total=len(test_x)):
            x = read_image(x)
            x_ct = read_image(x_ct)
            x_thres = read_thres_image(th)
            y = read_mask(y)
            images.append(x)
            images_ct.append(x_ct)
            y_pred = preds[i]

            # Check whether the output directories are present, if not, create them.
            if not os.path.isdir(os.path.join(path, "pred/")):
                os.makedirs(os.path.join(path, "pred/"))
            if not os.path.isdir(os.path.join(path, "petct/")):
                os.makedirs(os.path.join(path, "petct/"))
            if not os.path.isdir(os.path.join(path, "combined/")):
                os.makedirs(os.path.join(path, "combined/"))
            if not os.path.isdir(os.path.join(path, "stacked/")):
                os.makedirs(os.path.join(path, "stacked/"))
            if not os.path.isdir(os.path.join(path, "results/")):
                os.makedirs(os.path.join(path, "results/"))
            path_petct = os.path.join(path, f"petct/{i}.png")
            path_pred = os.path.join(path, f"pred/{i}.png")
            path_combi = os.path.join(path, f"combined/{i}.png")
            path_stack = os.path.join(path, f"stacked/{i}.tif")

            # Generate output images.
            cv2.imwrite(path_petct, x_ct * mask_parse(y_pred) * 255.0)
            cv2.imwrite(path_combi, x*x_thres*mask_parse(y_pred) * 255.0)
            cv2.imwrite(path_pred, mask_parse(y_pred) * 255.0)
            h, w, _ = x.shape
            white_line = np.ones((h, 5, 3)) * 255.0

            all_images = [
                mask_parse(x) * 255.0, white_line,
                mask_parse(x_ct) * 255.0, white_line,
                mask_parse(y), white_line,
                mask_parse(x_thres) * 255.0, white_line,
                mask_parse(y_pred) * 255.0
            ]

            image = np.concatenate(all_images, axis=1)
            path_res = os.path.join(path, f"results/{i}.png")
            cv2.imwrite(path_res, image)
        for i in range(len(preds)):
            if not os.path.isdir(os.path.join(path, "pathologic/")):
                os.makedirs(os.path.join(path, "pathologic/"))
            path_preds = os.path.join(path, f"pathologic/{i}.png")
            cv2.imwrite(path_preds, mask_parse(preds[i]) * 255.0)