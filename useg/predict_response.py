import numpy as np
import csv
import os
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from sklearn.svm import SVC, SVR
from sklearn.linear_model import LogisticRegression
from scipy import interp
from sklearn.model_selection import LeaveOneOut, KFold, StratifiedKFold

FEATURE_SET = "PET+CT(All)"

do_load_feature_datasets_from_file = True

do_ct_only = False
do_combine_pet_ct = True
do_combine_rad_cli = False

responders_ids_path = "/home/sobhan/data/train/responders"
responders_patient_ids = np.asarray(sorted(os.listdir(responders_ids_path)), np.int)
nonresponders_ids_path = "/home/sobhan/data/train/non_responders"
nonresponders_patient_ids = np.asarray(sorted(os.listdir(nonresponders_ids_path)), np.int)

responders_ids_path_test = "/home/sobhan/data/test/responders"
responders_patient_ids_test = np.asarray(sorted(os.listdir(responders_ids_path_test)), np.int)
nonresponders_ids_path_test = "/home/sobhan/data/test/non_responders"
nonresponders_patient_ids_test = np.asarray(sorted(os.listdir(nonresponders_ids_path_test)), np.int)


train_path = "/home/sobhan/data/csv/2021/pet_all.csv"
train_path_ct = "/home/sobhan/data/csv/2021/ct_all.csv"

test_path = "/home/sobhan/data/radiomics_out/pyradiomics_pet.csv"
test_path_ct = "/home/sobhan/data/radiomics_out/pyradiomics_ct.csv"

df_path = "/home/sobhan/data/csv/2021/pet_all.csv"
df_path_ct = "/home/sobhan/data/csv/2021/ct_all.csv"
header_path = "/home/sobhan/data/csv/2021/head_pyrad_all_100_pet.csv"
header_path_ct = "/home/sobhan/data/csv/2021/head_pyrad_all_100_ct.csv"

# import feature names
def load_header(header_path):
    header = []
    with open(header_path, 'rt') as header_file:
        reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        header = next(reader)
    return header

def load_train_data(df_path, header):
    data = []
    with open(df_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(row['patient_id']) in nonresponders_patient_ids:
                data.append([float(row[column]) for column in header])
    return data

def load_test_data(df_path, header):
    data_test = []
    with open(df_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids_test or int(row['patient_id']) in nonresponders_patient_ids_test:
                data_test.append([float(row[column]) for column in header])
    return data_test

def load_train_subject_labels(path):
    labels = []
    delta_psa = []
    with open(path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in responders_patient_ids or int(row['patient_id']) in nonresponders_patient_ids:
                labels.append(int(row['label']))
                delta_psa.append(float(row['delta_psa']))
    return labels, delta_psa

# def load_test_subject_labels(path):
#     labels_test = []
#     # delta_psa_test = []
#     with open(path, 'rt') as csvfile:
#         reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
#         for row in reader:
#             if int(row['patient_id']) in responders_patient_ids_test or int(row['patient_id']) in nonresponders_patient_ids_test:
#                 labels_test.append(int(row['label']))
#                 # delta_psa_test.append(float(row['delta_psa']))
#     return labels_test #, delta_psa_test

# load headers
header = load_header(header_path)
n_features_pet = len(header)
header_ct = load_header(header_path_ct)
n_features_ct = len(header_ct)
print("NUM_FEATURES:")
print(n_features_pet, n_features_ct)
n_sub = 61

data_train_pet = load_train_data(train_path, header)
data_train_ct = load_train_data(train_path, header_ct)

data_train = load_train_data(train_path, header)
data_train_ct = load_train_data(train_path_ct, header_ct)

data_test = load_test_data(test_path, header)
data_test_ct = load_test_data(test_path_ct, header_ct)

if do_combine_pet_ct:
    data_train = np.concatenate((data_train, data_train_ct), axis=1)
    data_test = np.concatenate((data_test, data_test_ct), axis=1)
    n_features_pet = n_features_pet + n_features_ct
    header = np.concatenate((header, header_ct))

train_labels, train_delta_psa = load_train_subject_labels(train_path)

X_train, y_train = data_train, train_labels
X_test = data_test


scalerObj = preprocessing.MinMaxScaler()
scaler = scalerObj.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)


predictions = []

# # logistic regression
clf = LogisticRegression(penalty='l1', solver='liblinear', random_state=0, class_weight='balanced')
probas_ = clf.fit(X_train, y_train).predict_proba(X_test)
y_pred = clf.predict(X_test)
print("Predicted Labels Using Logistic Regression Classifier")
print(y_pred)
predictions.append(y_pred)

# # linear svc classifier
clf = SVC(kernel='linear', C=1.0, gamma=0.001, class_weight='balanced')
probas_ = clf.fit(X_train, y_train).decision_function(X_test)
y_pred = clf.predict(X_test)
print("Predicted Labels Using Linear SVM Classifier")
print(y_pred)
predictions.append(y_pred)


# # polynomial svc classifier
poly = SVC(kernel='poly', C=1.0, degree=2.0, class_weight='balanced')
probas_ = poly.fit(X_train, y_train).decision_function(X_test)
y_pred = poly.predict(X_test)
print("Predicted Labels Using Polynomial SVM Classifier")
print(y_pred)
predictions.append(y_pred)

# # rbf svc classifier
rbf = SVC(kernel='rbf', C=100, gamma=0.125, class_weight='balanced')
probas_ = rbf.fit(X_train, y_train).decision_function(X_test)
y_pred = rbf.predict(X_test)
print("Predicted Labels Using RBF SVM Classifier")
print(y_pred)
predictions.append(y_pred)

# # extra trees classifier
trees = ExtraTreesClassifier(max_depth=1, min_samples_leaf=1, n_estimators=150, class_weight='balanced')
probas_ = trees.fit(X_train, y_train).predict_proba(X_test)
y_pred = trees.predict(X_test)
print("Predicted Labels Using Extra Trees Classifier")
print(y_pred)
predictions.append(y_pred)

# # random forest classifier
forest = RandomForestClassifier(max_depth=1, min_samples_leaf=1, n_estimators=150, class_weight='balanced')
probas_ = forest.fit(X_train, y_train).predict_proba(X_test)
y_pred = forest.predict(X_test)
print("Predicted Labels Using Random Forest Classifier")
print(y_pred)
predictions.append(y_pred)



ma_vote_labels = np.zeros(len(X_test))
for i_sub in range(len(X_test)):
    if np.count_nonzero(np.array(predictions)[:, i_sub]) > 3:
        ma_vote_labels[i_sub] = 1

print("Predicted Labels Using Majority Voting:")
print(ma_vote_labels)