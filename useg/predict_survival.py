import numpy as np
import csv
import os
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, ExtraTreesRegressor, RandomForestRegressor
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from sklearn.svm import SVC, SVR
from sklearn.linear_model import LogisticRegression
from scipy import interp
from sklearn.model_selection import LeaveOneOut, KFold, StratifiedKFold
from sklearn import linear_model

FEATURE_SET = "PET+CT(All)"

do_load_feature_datasets_from_file = True

do_ct_only = False
do_combine_pet_ct = True
do_combine_rad_cli = False


responders_ids_path_test = "/home/sobhan/data/test/responders"
responders_patient_ids_test = np.asarray(sorted(os.listdir(responders_ids_path_test)), np.int)
nonresponders_ids_path_test = "/home/sobhan/data/test/non_responders"
nonresponders_patient_ids_test = np.asarray(sorted(os.listdir(nonresponders_ids_path_test)), np.int)
# survival = np.array(np.load("/home/sobhan/data/csv/2021/survival99.csv", allow_pickle=True), dtype=int)




train_path = "/home/sobhan/data/csv/2021/pet_all.csv"
train_path_ct = "/home/sobhan/data/csv/2021/ct_all.csv"

test_path = "/home/sobhan/data/radiomics_out/pyradiomics_pet.csv"
test_path_ct = "/home/sobhan/data/radiomics_out/pyradiomics_ct.csv"

df_path = "/home/sobhan/data/csv/2021/pet_all.csv"
df_path_ct = "/home/sobhan/data/csv/2021/ct_all.csv"
header_path = "/home/sobhan/data/csv/2021/head_pyrad_all_100_pet.csv"
header_path_ct = "/home/sobhan/data/csv/2021/head_pyrad_all_100_ct.csv"

# import feature names
def load_header(header_path):
    header = []
    with open(header_path, 'rt') as header_file:
        reader = csv.reader(header_file, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        header = next(reader)
    return header

print("DEBUG")
survival = np.array(load_header("/home/sobhan/data/csv/2021/survival99.csv"), dtype=int)
# print(survival)
patient_ids_train = np.array(range(len(survival)))
# print(patient_ids_train)
patient_ids_test = np.concatenate((responders_patient_ids_test, nonresponders_patient_ids_test))
# print(patient_ids_test)

def load_train_data(df_path, header):
    data = []
    with open(df_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in patient_ids_train:
                data.append([float(row[column]) for column in header])
    return data

def load_test_data(df_path, header):
    data_test = []
    with open(df_path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            if int(row['patient_id']) in patient_ids_test:
                data_test.append([float(row[column]) for column in header])
    return data_test


# load headers
header = load_header(header_path)
n_features_pet = len(header)
header_ct = load_header(header_path_ct)
n_features_ct = len(header_ct)
# print("NUM_FEATURES:")
# print(n_features_pet, n_features_ct)
n_sub = 99

data_train_pet = load_train_data(train_path, header)
data_train_ct = load_train_data(train_path, header_ct)

data_train = load_train_data(train_path, header)
data_train_ct = load_train_data(train_path_ct, header_ct)

data_test = load_test_data(test_path, header)
data_test_ct = load_test_data(test_path_ct, header_ct)

if do_combine_pet_ct:
    data_train = np.concatenate((data_train, data_train_ct), axis=1)
    data_test = np.concatenate((data_test, data_test_ct), axis=1)
    n_features_pet = n_features_pet + n_features_ct
    header = np.concatenate((header, header_ct))

train_labels = survival

X_train, y_train = data_train, train_labels
X_test = data_test


scalerObj = preprocessing.MinMaxScaler()
scaler = scalerObj.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# # mulrtivariate linear regression
clr = linear_model.LinearRegression()
clr.fit(X_train, y_train)
print(y_train)
y_pred = np.array(clr.predict(X_test), dtype=int) * 60
print("Predicted Labels Using Multivariate Linear Regression (Days of Survival)")
print(y_pred)

