
import os
import numpy as np
import cv2
# import tensorflow as tf
# from tensorflow.keras.utils import CustomObjectScope
from tqdm import tqdm
from data import load_data, load_test_data, tf_dataset
# from train import iou
# import pandas as pd
from radiomics import featureextractor
from radiomics import firstorder
import SimpleITK as sitk
import six
import csv


# The following constants are used to customize the feature and mask types
# Please choose between "fo" and "all" for FEATURES_TYPE and
# "thres" or "unet" or "gt" or "bone" or "bone_met" or "bone_pred" MASK_TYPE
FEATURES_TYPE = "all" # "fo" or "all"
MASK_TYPE = "unet" # "thres" or "unet" or "gt" or "bone" or "bone_met" or "bone_pred"

def read_image(path):
    x = cv2.imread(path, cv2.IMREAD_COLOR)
    x = cv2.resize(x, (256, 256))
    x = x/255.0
    return x

def read_thres_image(path):
    x = cv2.imread(path, cv2.IMREAD_COLOR)
    x = cv2.resize(x, (256, 256))
    x = x/255.0
    return x

def read_mask(path):
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = np.expand_dims(x, axis=-1)
    return x

def mask_parse(mask):
    mask = np.squeeze(mask)
    mask = [mask, mask, mask]
    mask = np.transpose(mask, (1, 2, 0))
    return mask

if __name__ == "__main__":
    ## Dataset
    data_path = "/home/sobhan/data/dataset"
    data_out_path = "/home/sobhan/data/radiomics_out"
    pyrad_keys = []
    pyrad_results = []
    pyrad_results_ct = []

    train_paths = []
    train_paths.append("/home/sobhan/data/train/responders")
    train_paths.append("/home/sobhan/data/train/non_responders")

    test_paths = []
    test_paths.append("/home/sobhan/data/test/responders")
    test_paths.append("/home/sobhan/data/test/non_responders")

    train_ids = []
    for dest_path in train_paths:
        for id, patient_id in enumerate(sorted(os.listdir(dest_path))):
            train_ids.append(patient_id)

    test_ids = []
    for dest_path in test_paths:
        for id, patient_id in enumerate(sorted(os.listdir(dest_path))):
            test_ids.append(patient_id)

    for patient_id in test_ids:

        path = os.path.join(data_path, patient_id)
        print (path)
        batch_size = 16
        (test_x, test_x_ct, test_y, test_th, test_bones, test_pred) = load_test_data(path)

        images = []
        images_ct = []
        masks = []
        preds = []
        for i, (x, x_ct, y, th, bone, pr) in tqdm(enumerate(zip(test_x, test_x_ct, test_y, test_th, test_bones, test_pred)), total=len(test_x)):
            x = read_image(x)
            x_ct = read_image(x_ct)
            bone = read_mask(bone)
            x_thres = read_thres_image(th)
            y = read_mask(y)
            y_pred = read_mask(pr)
            y_thres = read_mask(th)
            if y_pred.any():
                preds.append(y_pred)
            images.append(x)
            images_ct.append(x_ct)
            if patient_id in train_ids:
                masks.append(mask_parse(y))
            elif MASK_TYPE == "unet":
                masks.append(mask_parse(y_pred))
            elif MASK_TYPE == "gt":
                masks.append(mask_parse(y))
            elif MASK_TYPE =="thres":
                masks.append(mask_parse(y_thres))
            elif MASK_TYPE =="bone":
                masks.append(mask_parse(bone))
            elif MASK_TYPE =="bone_met":
                masks.append(mask_parse(bone) * mask_parse(y_thres))
            elif MASK_TYPE =="bone_pred":
                masks.append(mask_parse(bone) * mask_parse(y_pred))
        print(np.array(images).shape)
        print(np.array(masks).shape)
        images = np.array(images)[:, :, :, 0]
        images_ct = np.array(images_ct)[:, :, :, 0]
        masks = np.array(masks)[:, :, :, 0]

        img = sitk.GetImageFromArray(images)
        img_ct = sitk.GetImageFromArray(images_ct)
        mask = sitk.GetImageFromArray((masks > 0).astype(np.uint8))

        res = []
        res_ct = []

        if FEATURES_TYPE == "all":
            params = {}
            params["resampledPixelSpacing"] = [2.5,2.5,2.5]

            # Feature extractor for PET
            extractor = featureextractor.RadiomicsFeatureExtractor(**params)
            extractor.enableAllFeatures()

            # Feature extractor for CT
            extractor_ct = featureextractor.RadiomicsFeatureExtractor(**params)
            extractor_ct.enableAllFeatures()

            res = extractor.execute(img, mask)
            res_ct = extractor_ct.execute(img_ct, mask)


        else: # for FirstOrder (fo) features only
            try:
                # Feature extractor for PET
                firstOrderFeatures = firstorder.RadiomicsFirstOrder(img, mask)
                firstOrderFeatures.enableAllFeatures()
                res = firstOrderFeatures.execute()

                # Feature extractor for CT
                firstOrderFeatures_ct = firstorder.RadiomicsFirstOrder(img_ct, mask)
                firstOrderFeatures_ct.enableAllFeatures()
                res_ct = firstOrderFeatures_ct.execute()
            except ValueError as e:
                print ("ValueError")
                print (e)

        # radiomics freatures from pet
        keys = []
        vals = []
        for (key, val) in six.iteritems(res):
            # print("\t%s: %s" % (key, val))
            if not ',' in str(val):
                keys.append(key)
                vals.append(val)
        keys.append('patient_id')
        pyrad_keys = keys
        vals.append(int(patient_id))
        pyrad_results.append(vals)

        # radiomics freatures from ct
        keys = []
        vals = []
        for (key, val) in six.iteritems(res_ct):
            # print("\t%s: %s" % (key, val))
            if not ',' in str(val):
                keys.append(key)
                vals.append(val)
        keys.append('patient_id')
        pyrad_keys = keys
        vals.append(int(patient_id))
        pyrad_results_ct.append(vals)

    # Check whether the features are calculated for both modalities successfully
    if pyrad_results and pyrad_results_ct:
        path_out = os.path.join(data_out_path, "pyradiomics_pet.csv")
        path_out_ct = os.path.join(data_out_path, "pyradiomics_ct.csv")

        # Save PET radiomics features
        with open(path_out, 'w') as outfile_pet:
            try:
                writer = csv.writer(outfile_pet, delimiter=';',
                                quoting=csv.QUOTE_MINIMAL)
                writer.writerow(pyrad_keys)
                writer.writerows(pyrad_results)
            except Exception as e:
                print ("Something went wrong while exporting csv PyRadiomics")
                print (e)

        # Save CT radiomics features
        with open(path_out_ct, 'w') as outfile_ct:
            try:
                writer = csv.writer(outfile_ct, delimiter=';',
                                quoting=csv.QUOTE_MINIMAL)
                writer.writerow(pyrad_keys)
                writer.writerows(pyrad_results_ct)
            except Exception as e:
                print ("Something went wrong while exporting csv PyRadiomics")
                print (e)