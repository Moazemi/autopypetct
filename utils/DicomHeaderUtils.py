#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 10:26:59 2017

@author: sobhan
"""

import dicom
import dicom_numpy
from os import listdir
from os.path import isfile, join
import datetime
import math
import numpy as np

default_dicomDir = "/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/name/PET/DICOM/PA1/ST1/SE1/"
default_dicomDirCT = "/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/name/CT/DICOM/PA1/ST1/SE1/"

# lists all files in the given dicom directory
def get_list_of_dicom_files(dicomDir=default_dicomDir):
    return [f for f in listdir(dicomDir) if isfile(join(dicomDir, f))]

# returns the dicome header of a file in the given directory as text
def get_dicom_header(i=0, dicomDir=default_dicomDir):
    dcm_files = get_list_of_dicom_files(dicomDir)
    dcm_file = dicom.read_file(dicomDir + str(dcm_files[i]), force=True)
    return dcm_file

def get_voxel_data(dicomDir=default_dicomDir):
    datasets = [dicom.read_file(dicomDir + f, force=True) for f in get_list_of_dicom_files(dicomDir)]
    try:
        voxel_ndarray, ijk_to_xyz = dicom_numpy.combine_slices(datasets)
    except dicom_numpy.DicomImportException as e:
        print "invalid DICOM data:"
        print e.message
        raise
    return voxel_ndarray, ijk_to_xyz

def get_index_to_voxel_transform_matrix(dicomDir=default_dicomDir):
    _pixels, _affine_mat = get_voxel_data(dicomDir)
    return _affine_mat

def get_patient_name(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    return dcm_file.PatientName.replace('^', ' ')
    
def get_patient_weight(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    return dcm_file.PatientWeight

def get_injected_dose(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    return dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1074].value

def get_half_life(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    return dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1075].value

def get_image_position(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x20, 0x32].value

def get_image_orientation(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x20, 0x37].value

def get_pixel_spacing(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x28, 0x30].value

def get_slice_thickness(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x18, 0x50].value

def get_scan_start_time(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    start_time = dcm_file.RadiopharmaceuticalInformationSequence[0][0x18, 0x1072].value
    return datetime.datetime.strptime(start_time, "%H%M%S.%f")

def get_acquisition_time(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(0, dicomDir)
    acquisition_time = dcm_file[0x08, 0x32].value
    return datetime.datetime.strptime(acquisition_time, "%H%M%S.%f")

def get_instance_number(i, dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(i, dicomDir)
    return dcm_file[0x20, 0x13].value

# calculates and returns the SUV: standardized uptake value
def get_suv_bw(dicomDir=default_dicomDir):
    injected_dose = float(get_injected_dose(dicomDir) / 1000000)
    half_life = get_half_life(dicomDir)
    delta_t = (get_acquisition_time(dicomDir) - get_scan_start_time(dicomDir)).seconds # ??? is it in seconds?
    weight = get_patient_weight(dicomDir)    
    corrected_dose = float (injected_dose * math.exp( - float(math.log(2.0) / half_life * delta_t)))
    print "calculated SUV_BW = %f" % float(weight / corrected_dose)
    return float(weight / corrected_dose)

def get_rescale_intercept(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RescaleIntercept

def get_rescale_slope(dicomDir=default_dicomDir):
    dcm_file = get_dicom_header(dicomDir)
    return dcm_file.RescaleSlope

#for i in range(1):    
#    print "i=%d" % i    
#    print get_instance_number(i, default_dicomDir)
#    print get_image_position(i, default_dicomDir)
#    image_orientation_pet = get_image_orientation(i, default_dicomDir)
#    print image_orientation_pet[:3]
#    pixel_spacing_pet = get_pixel_spacing(i, default_dicomDir)
#    print pixel_spacing_pet[1]
#    print "-----------------------------------------"
#    print get_instance_number(i, default_dicomDirCT)
#    print get_image_position(i, default_dicomDirCT)
#    image_orientation_ct = get_image_orientation(i, default_dicomDirCT)
#    print image_orientation_ct[3:]
#    pixel_spacing_ct = get_pixel_spacing(i, default_dicomDirCT)
#    print pixel_spacing_ct[0]
#    print "*****************************************"

#affine_mat_pet = get_index_to_voxel_transform_matrix(default_dicomDir)
#affine_mat_ct = get_index_to_voxel_transform_matrix(default_dicomDirCT)
#a = np.dot(affine_mat_pet, [10, 20, 0, 1])
#
#print affine_mat_pet
#print affine_mat_ct
#print affine_mat_ct

#print get_injected_dose(dicomDir)    
#print get_scan_start_time(dicomDir)
#print get_acquisition_time(dicomDir)
#print get_suv_bw(dicomDir)
#print get_rescale_intercept(default_dicomDirCT)
#print get_rescale_slope(default_dicomDirCT)
#print get_dicom_header()
#print get_image_position()
