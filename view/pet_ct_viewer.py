#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 10:26:11 2017

@author: sobhan
"""

import os
import numpy as np
#import SimpleITK 
import sys
from PyQt4 import QtGui, QtCore
#from PyQt4.QtCore import *
#from PyQt4.QtGui import *

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
#from __future__ import print_function
from matplotlib.widgets import LassoSelector

import cv2

import dicom

import json

from sklearn.svm import SVC as svc
from sklearn import preprocessing
#from skmultilearn.problem_transform import BinaryRelevance as BR
#from sklearn.naive_bayes import GaussianNB as GB
#from skmultilearn.adapt import MLkNN

#from sklearn.neighbors import KDTree
import scipy
from scipy import ndimage
import scipy.ndimage.filters as filters

from scipy.ndimage import find_objects
import DicomHeaderUtils as dhu
import csv
import math
from skimage.measure import *
#from skimage.feature import peak_local_max
from radiomics import featureextractor
import SimpleITK as sitk 


PathsDicom = []
PathsDicomCT = []
MAX_PATIENTS_COUNT = 10
MAX_SLICES_COUNT = 500
PET_RESOLUTION = 256
CT_RESOLUTION = 512

# pet expansion parameters
PET_HIGHEST_RESOLUTION = 700
PET_EXPAND_OFFSET = 98

WINDOWS = 0


TEST = 1
        
if WINDOWS:
#    PathsDicom.append("C:/Users/Mediso/Downloads/sobhan/python-pet-ct-lesion-detection/dicom/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("C:/Users/Mediso/Downloads/sobhan/python-pet-ct-lesion-detection/dicom/CT/DICOM/PA1/ST1/SE1/")   
#    
#    PathsDicom.append("D:/Sobhan/dicom/patient2/DICOM/PA1/ST1/SE1/")                      
#    PathsDicomCT.append("D:/Sobhan/dicom/patient2/DICOM/PA1/ST1/SE2/")
#    
#    
#    
#    PathsDicom.append("C:/Users/Mediso/Downloads/sobhan/dicom/patien3/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("C:/Users/Mediso/Downloads/sobhan/dicom/patien3/CT/DICOM/PA1/ST1/SE1/")
    
    
    
    
    
#    PathsDicom.append("D:/Sobhan/dicom/patient1/pet/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("D:/Sobhan/dicom/patient1/ct/DICOM/PA1/ST1/SE1/")
    
    PathsDicom.append("D:/Sobhan/dicom/patient14/PET/DICOM/PA1/ST1/SE1/")
    PathsDicomCT.append("D:/Sobhan/dicom/patient14/CT/DICOM/PA1/ST1/SE1/")
    
    
    
#    PathsDicom.append("D:/Sobhan/dicom/patient3/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("D:/Sobhan/dicom/patient3/CT/DICOM/PA1/ST1/SE1/")
    
#    
#    PathsDicom.append("D:/Sobhan/dicom/patient4/pet/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("D:/Sobhan/dicom/patient4/ct/DICOM/PA1/ST1/SE1/")
#    
#    
#    PathsDicom.append("D:/Sobhan/dicom/patient5/pet/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("D:/Sobhan/dicom/patient5/ct/DICOM/PA1/ST1/SE1/")
    
elif TEST:
    
#    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Garmasch_G/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Garmasch_G/CT/DICOM/PA1/ST1/SE1/")

#    PathsDicom.append("/home/sobhan/Pictures/dicom/dicom/patient13/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/dicom/patient13/CT/DICOM/PA1/ST1/SE2/")
#    
#    PathsDicom.append("/home/sobhan/Pictures/dicom/Annette/DICOM PET/DICOM/PA5/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/Annette/DICOM CT/DICOM/PA5/ST1/SE1/")

    PathsDicom.append("/home/sobhan/data/dataset/00/dicom/pet/orig/")
    PathsDicomCT.append("/home/sobhan/data/dataset/00/dicom/ct/orig/")

    
#    PathsDicom.append("/home/sobhan/Documents/PhD/2020/Sobhan/Tables/dicom/Brabender_Uwe/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Documents/PhD/2020/Sobhan/Tables/dicom/Brabender_Uwe/CT/DICOM/PA1/ST1/SE1/")

       

#    PathsDicom.append("/home/moazemi/src/pet-ct/python-pet-ct-lesion-detection/dicom/patient_01_psma/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/moazemi/src/pet-ct/python-pet-ct-lesion-detection/dicom/patient_01_psma/CT/DICOM/PA1/ST1/SE1/")

else:  
    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Garmasch_G/PET/DICOM/PA1/ST1/SE1/")
    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Paul_H/PET/DICOM/PA1/ST1/SE1/")
    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Aslan_H/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Berg_P/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Hagemann_R/PET/DICOM/PA1/ST1/SE1/")
#    PathsDicom.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Richter_C/PET/DICOM/PA1/ST1/SE1/")
    

    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Garmasch_G/CT/DICOM/PA1/ST1/SE1/")
    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/PathologicalFindings/Paul_H/CT/DICOM/PA1/ST1/SE1/")
    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Aslan_H/CT/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Berg_P/CT/DICOM/PA1/ST1/SE1/")
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Hagemann_R/CT/DICOM/PA1/ST1/SE1/")                    
#    PathsDicomCT.append("/home/sobhan/Pictures/dicom/PSMA_Test_01/NoPathologicalFindings/Richter_C/CT/DICOM/PA1/ST1/SE1/")
        
class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        
        if WINDOWS:
            self.WORKING_DIR = 'D:/Sobhan/dicom/outpt/testnov/'
        else:
            self.WORKING_DIR = '/home/sobhan/gt_oct17'
        
        super(Window, self).__init__(parent)
                    
        # by default, the original image will be shown
        self.imageType = 1 # values: 1: original image, 2: CT image
        self.maskState = 1 # values: 1: original CT,  2: bone mask,...
        self.isVerticalViewActive = False
         
        # number of 2D slices (z axis)
        self.sliceCount = np.zeros(MAX_PATIENTS_COUNT)
        self.sliceCountCT = np.zeros(MAX_PATIENTS_COUNT)        
        self.scrollStep = 1
        
        # sampling resolution
        self.sampling_resolution = [2.5,2.5,2.5]
        
        # used to toggle grayscale
        self.showGrayscale = True
        
        # used to control resampling
        self.with_resampling = True
        
        # used to control testing of detect_hotspots
        self.test_detect_hotspots = True
        
        # used to control ct compression
        self.ct_compression = not self.with_resampling
        
        # used to control loading of the previously saved findings
        self.with_loading = False
        
        # export hotspots as csv?
        self.with_csv_export = True
        
        # used to control the usage of pyradiomics library
        self.with_puradiomics = False
        
        # used to control the application of local maxima algorithm
        self.with_local_maxima = True
        
        self.patientIndex = 0

        if self.with_resampling:
            self.zOffset = 0
        else:
            self.zOffset = 5
            
        self.z = 0  

        self.zCT = self.z + self.zOffset        
        
        self.up_wheel = False
        
        self.axCT = plt.subplot(111)
        
        """ tests for lasso selector """            
                
        lasso = LassoSelector(self.axCT, self.onselect)
        
        self.patientNames = []
        
        self.labeled = []
        self.labeled_met = []
        
        self.region_props = []
        self.region_props_met = []
        
        self.affine_mat = []
        
        # used for ml training        
        self.training_data = []
        self.training_labels = []
        self.training_data_multilabel = []
        self.training_labels_multilabel = []
        
        # does all the calculations and visualizations
        self.all_the_magic()
             
    def add_new_patient(self):
        try:
            print "INSIDE add_new_patient, num of patients: %d" % len(PathsDicom)
            i = self.patientIndex
            
            self.progress.setValue(10)
            # loading new pets
            _new_scans = self.load_scan(PathsDicom[i])
            self.pet_scans.append(_new_scans) 
            self.suv_bw.append(dhu.get_suv_bw(PathsDicom[i]))
            _new_imgs = np.copy(self.get_pixels_hu(_new_scans))
            self.pet_imgs.append(_new_imgs)
            self.transformed_pet.append(_new_imgs * self.suv_bw[i])        
            self.patientNames.append(dhu.get_patient_name(PathsDicom[self.patientIndex]))
            num_new_pets = len(self.transformed_pet[self.patientIndex])                
            
            self.progress.setValue(15)
            
            print "INSIDE add_new_patient1"
            
            # loading new cts
            ct_scans = self.load_scan(PathsDicomCT[self.patientIndex])
            self.ct_scans.append(ct_scans)
            num_new_cts = len(ct_scans)   
            
            if num_new_cts < num_new_pets:
                num_slices = num_new_cts
            else:
                num_slices = num_new_pets
            
            print "INSIDE add_new_patient2"
            self.progress.setValue(20)
            
            # update slice count variables
            _new_counts_pet = np.zeros(len(PathsDicom))
            _new_counts_ct = np.zeros(len(PathsDicomCT))
    #        print _new_counts_pet
            for i in range(len(PathsDicom) - 1):
                _new_counts_pet[i] = int(self.sliceCount[i])
            if num_new_cts < num_new_pets:
                _new_counts_pet[self.patientIndex] = int(num_new_cts)
            else:
                _new_counts_pet[self.patientIndex] = int(num_new_pets)
                
            for i in range(len(PathsDicomCT) - 1):
                _new_counts_ct[i] = int(self.sliceCountCT[i])
            if num_new_cts < num_new_pets:
                _new_counts_ct[self.patientIndex] = int(num_new_cts)
            else:
                _new_counts_ct[self.patientIndex] = int(num_new_pets)
                
            self.sliceCount = _new_counts_pet
            self.sliceCountCT = _new_counts_ct
            
            self.max_slice_count = int(max(self.sliceCount))
            
            ct_imgs = self.get_pixels_huCT(ct_scans)
            self.ct_imgs.append(ct_imgs)
            _compressed_cts = np.zeros([MAX_SLICES_COUNT, PET_RESOLUTION, PET_RESOLUTION])
                        
            self.progress.setValue(25)
            
            # compress new cts
            self.cts_before_resampling.append(ct_imgs)
            if self.ct_compression:           
                self.zOffset = 5
                for z in range(self.zOffset, self.zOffset + num_slices):                
                    self.progress.setValue(25 + int(z/10))
                    _compressed_cts[z - self.zOffset , 27:227, 27:227] = np.copy(cv2.resize(ct_imgs[z],(200, 200)))
                self.compressed_cts.append(_compressed_cts)
                self.zOffset = 0
            else:
                self.compressed_cts.append(ct_imgs)
                    
            print "Number of ct slices: %d" % len(self.cts_before_resampling[self.patientIndex])
            
            self.progress.setValue(70)
            print "INSIDE add_new_patient3"
            
            # do resampling?
            if self.with_resampling:
                self.do_resampling(self.patientIndex, self.sampling_resolution)
                self.do_resamplingCT(self.patientIndex, self.sampling_resolution)
            
            print "INSIDE add_new_patient4"
            # create ct masks     
            self.make_ct_masks(self.patientIndex)
            
            print "INSIDE add_new_patient5"
            
            # create suv mask
            self.create_suv_mask(self.patientIndex)
            
            print "INSIDE add_new_patient6"
            
            # find the hotspots
            self.detect_hotspots(self.patientIndex)
            
            self.progress.setValue(95)
    
            # set lesion navigation variables
            self.currentLesionIndex = 0
            z = 0
            if len(self.hotspots[self.patientIndex]) > 0:
                z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            self.z = z
            self.zCT = self.z + self.zOffset
            
            print "INSIDE add_new_patient7"
            
            self.progress.setValue(95)
            
            # map slice numbers to lesion indexes
#            self.make_slice_lesion_mapping()
            
            
            print "INSIDE add_new_patient9"
            
            # refresh lauout
            print "PATIENT %s Added" % dhu.get_patient_name(PathsDicom[self.patientIndex])
            self.comboSelectPatient.addItem(dhu.get_patient_name(PathsDicom[self.patientIndex]))
            if len(self.hotspots[self.patientIndex]) > 0:
                for j in range(len(self.hotspots[self.patientIndex])):
                    self.currentLesionIndex = j
                    self.update_diagnosis_label(True)
            else:
                    self.update_diagnosis_label(False)
            self.currentLesionIndex = 0
            self.comboSelectPatient.setCurrentIndex(self.patientIndex)
            self.progress.setValue(100)
            
            print "INSIDE add_new_patient10"
        
        except Exception as e:
            print "Something went wrong while adding new patient"
            print e.message
            print len(PathsDicom), len(PathsDicomCT)
    
    def map_categories_to_labels(self, category=0):
       # z, x, y, flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagOther, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        flagLiver = flagLung = flagKidney = flagBone = flagBladder = flagProstate = flagLymph = flagSalG = flagSpleen = flagOther = False        
        if int(category) == 1:
            flagLiver = True    
        elif int(category) == 2:
            flagLung = True    
        elif int(category) == 3:
            flagKidney = True
        elif int(category) == 4:
            flagBone = True
        elif int(category) == 5:
            flagBladder = True
        elif int(category) == 6:
            flagProstate = True
        elif int(category) == 7:
            flagLymph = True
        elif int(category) == 8:
            flagSalG = True
        elif int(category) == 9:
            flagSpleen = True
        else:   # Other tissue
            flagOther = True
        return flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
    
    def map_labels_to_categories(self, labels=(False,False,False,False,False,False,False,False,False,True)):
        flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = labels
        print "INSIDE map_labels_to_categories", flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
        category = 0
        if bool(flagLiver):
            category = 1
        elif bool(flagLung):
            category = 2
        elif bool(flagKidney):
            category = 3
        elif bool(flagBone):
            category = 4
        elif bool(flagBladder):
            category = 5
        elif bool(flagProstate):
            category = 6
        elif bool(flagLymph):
            category = 7
        elif bool(flagSalG):
            category = 8
        elif bool(flagSpleen):
            category = 9
        else:
            category = 0
        return category
            
    def refresh_layout(self):              
        # set the layout
        layout = QtGui.QGridLayout()
        layout.addWidget(self.toolbar, 2, 1, 1, 2)
        layout.addWidget(self.toolbarCT, 2, 3, 1, 2)
        layout.addWidget(self.labelZSliceNo, 1, 0, 1, 1)
        layout.addWidget(self.labelScrollSlices, 2, 0, 1, 1)
        layout.addWidget(self.comboScrollStep, 2, 0, 1, 1)
        layout.addWidget(self.canvas, 3, 1, 20, 2)
        layout.addWidget(self.canvasCT, 3, 3, 20, 2)
#        layout.addWidget(self.labelDiagnosisTitle, 2, 3, 1, 1)
        layout.addWidget(self.buttonOpenDicomDir, 3, 0, 1, 1)
        layout.addWidget(self.buttonOpenDicomDirCT, 4, 0, 1, 1)
#        layout.addWidget(self.labelWait, 5, 0, 1, 1)
#        layout.addWidget(self.progress, 6, 0, 1, 1)
        layout.addWidget(self.labelSelectPatient, 5, 0, 1, 1)
        layout.addWidget(self.comboSelectPatient, 6, 0, 1, 1)
        
#        layout.addWidget(self.buttonVerticalImage, 5, 0, 1, 1)
#        layout.addWidget(self.buttonToggleGrayscale, 6, 0, 1, 1)
        layout.addWidget(self.labelSelectCTMask, 7, 0, 1, 1)
        layout.addWidget(self.comboSwitchMasks, 8, 0, 1, 1)
        layout.addWidget(self.buttonToggleSUVOverlay, 9, 0, 1, 1)
#        layout.addWidget(self.textMaxThresCT, 7, 0, 1, 1)
#        layout.addWidget(self.textMinThresCT, 8, 0, 1, 1)
#        layout.addWidget(self.labelImageTypes, 7, 0, 1, 1)
#        layout.addWidget(self.comboImageThresholds, 8, 0, 1, 1)                
#        layout.addWidget(self.labelDiagnosisText, 5, 3, 1, 1)
#        layout.addWidget(self.textDiagnosis, 6, 3, 1, 1)
#        layout.addWidget(self.buttonSubmitDiagnosis, 7, 3, 1, 1)      
        
        layout.addWidget(self.labelDiagnosis, 10, 0, 1, 1)
        layout.addWidget(self.radioYes, 11, 0, 1, 1)
        layout.addWidget(self.radioNo, 12, 0, 1, 1)
        layout.addWidget(self.labelDiagnosis2, 13, 0, 1, 1)
        layout.addLayout(self.diagVLayout, 14, 0, 1, 1)
        layout.addWidget(self.buttonNextLesion, 15, 0, 1, 1)
        layout.addWidget(self.buttonPrevLesion, 16, 0, 1, 1)
#        layout.addWidget(self.buttonEraseSeeds, 17, 0, 1, 1)
        layout.addWidget(self.buttonSaveAllDiagnoses, 17, 0, 1, 1)
        layout.addWidget(self.buttonTrainDataSet, 18, 0, 1, 1)
        
        
        self.resize(2000,1200)
        self.setLayout(layout)
        self.x = 0
        
        # plot the 0th slice
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
    
    def update_slice_containers(self, ct_size):
        self.new_pets = np.zeros([len(PathsDicom), self.max_slice_count, PET_RESOLUTION, PET_RESOLUTION])
        self.new_cts = np.zeros([len(PathsDicomCT), self.max_slice_count, ct_size, ct_size])
        for i in range(len(PathsDicom)):
            for z in range(int(self.sliceCount[i])):
                self.new_pets[i][z] = np.copy(self.transformed_pet[i][z])
        self.transformed_pet = self.new_pets
        
        for i in range(len(PathsDicomCT)):
            for z in range(int(self.sliceCountCT[i])):
                self.new_cts[i][z] = np.copy(self.cts_before_resampling[i][z])
        self.cts_before_resampling = self.new_cts
        
    def detect_hotspots(self, i):
        if self.with_resampling:
            data = np.copy(self.pets_after_resamp[i])
        else:
            data = np.copy(self.transformed_pet[i])
#        data = self.suv_mask[i]
#        data = self.mask_test[i]

         # 1. limit the focus range        
        _min_val = data.min()
        _max_val = data.max()
        _mean_val = data.mean()
        if _min_val < 0: 
            data += np.absolute(_min_val)
            _min_val = data.min()
            _max_val = data.max()
            _mean_val = data.mean()




        print "INSIDE detect_hotspots 1"
        print data.shape
        print _min_val, _max_val, _mean_val
        _threshold =   float(float(_max_val - _min_val) * 75. / 100.) # float(_max_val) # .75
#        _threshold = float(float(_mean_val) / 2.)
#        data[data < _threshold] = 0
        
        
        print "INSIDE detect_hotspots 0"        
        print data.shape
        dt = []
        
        import six
        src_img = sitk.GetImageFromArray(self.transformed_pet[i]) # sitk.GetImageFromArray(data)
        for sli in range(len(data)):
            sl = data[sli]
#            scalerObj = preprocessing.StandardScaler()
#            scaler = scalerObj.fit(sl)
#            sl = scaler.transform(sl)
#            sl = preprocessing.normalize(sl)
#            _min_val = sl.min()
#            _max_val = sl.max()
#            _mean_val = sl.mean()
#            if _min_val < 0: 
#                sl += np.absolute(_min_val)
#                _min_val = sl.min()
#                _max_val = sl.max()
#                _mean_val = sl.mean()
#            _threshold =   float(float(_max_val - _min_val) * 99. / 100.)
            sl = preprocessing.binarize(sl, _threshold)
#            print "SL"
#            print sl
            mask = sitk.GetImageFromArray(sl)
#            pyrad_res = extractor.execute(src_img, mask)
#            for key, value in pyrad_res.iteritems():
#                print "\t", key, ":", value

#            try:
#                firstOrderFeatures = firstorder.RadiomicsFirstOrder(src_img,mask)
#                firstOrderFeatures.enableAllFeatures()  # On the feature class level, all features are disabled by default.
#                firstOrderFeatures.calculateFeatures()
#                print "PYRADIOMICS"
#                print sli
#                for (key,val) in six.iteritems(firstOrderFeatures.featureValues):
#                    
#                    print("\t%s: %s" % (key, val))
#            except ValueError as e:
#                print sli, "ValueError OCURED 0: ", e.message
#                pass
              
            dt.append(sl)
        data = np.array(dt)
        print data.shape
        
        # 2. redefine data
        data = data.astype(np.float64)
        _min_val = data.min()
        _max_val = data.max()
        _mean_val = data.mean()
        print "INSIDE detect_hotspots 2"
        print data.shape
        print _min_val, _max_val, _mean_val
        
        # 3. find local maxima        
        if self.with_local_maxima:
            neighborhood_size = 8
#            threshold = float(1000. * float(_max_val - _min_val)/2400.) #1400 # 20500 # 2398/2400
            _threshold =   float(float(_max_val - _min_val) * 40. / 100.)
            
#            data_max = filters.maximum_filter(data, neighborhood_size)
            data_max = ndimage.maximum_filter(data, size=(5,5,5))
            
            _threshold = data_max.mean() + data_max.std()*6
            print "TEST PEAK"
##            test_max_coords = peak_local_max(data, min_distance=20)
            maxima = (data == data_max)
##            data_min = filters.minimum_filter(data, neighborhood_size)
##            diff = ((data_max - data_min) > _threshold)
##            maxima[diff == 0] = 0  
##            maxima[maxima<_threshold] = 0
            
            # maxima = maxima>_threshold

#            maxima = data_max>_threshold
        

            labeled, num_objects = ndimage.label(maxima)
    #        labeled, num_objects = ndimage.label(data_max>_threshold)
            print "LABELED"
            print labeled.shape, num_objects        
            self.labeled.append(labeled)
    #        print len(self.labeled)
            props = []
            for prop in range(len(regionprops(labeled))):
                props.append(prop)
            self.region_props.append(props)
            objects = find_objects(labeled)
    #        print np.array(objects).shape
    #        print objects
        

        
        if not self.test_detect_hotspots:  
        
    #        labeled, num_objects = ndimage.label(data)
                                
            # 4. find hotpots centers
            _maximum_centers = ndimage.center_of_mass(data, labeled, range(1, num_objects+1))
            print "MAX_CENTERS"
            print _maximum_centers
    #        _maximum_centers = test_max_coords
            
            print "number of lesions: %d, %d" % (len(_maximum_centers), num_objects)
    #        print _maximum_centers
            self.hotspots[i] = _maximum_centers
            print len(self.hotspots[i])
            
            # extract features using pyradiomics
            if self.with_puradiomics:
                from radiomics import firstorder
                extractor = featureextractor.RadiomicsFeaturesExtractor()
                extractor.enableAllFeatures()
                feature_class_names = extractor.getFeatureClassNames()
                feature_names = []        
                print "FEATURE_NAMES"
                for c_name in feature_class_names:            
                    f_names = extractor.getFeatureNames(c_name)
                    feature_names.append(f_names)
                    print c_name
                    print f_names
                print len(feature_names)
        #        src_img = sitk.GetImageFromArray(self.transformed_pet[i]) #sitk.GetImageFromArray(labeled)
                
                data_path = self.WORKING_DIR + '/hotspots_pyradiomics_%d.csv' % i 
                rows = []
                with open(data_path, 'wb') as csvfile:
                    writer = csv.writer(csvfile, delimiter=';',
                                             quoting=csv.QUOTE_MINIMAL)
                    for j in range(num_objects):            
                        src_img = sitk.GetImageFromArray(self.transformed_pet[i][objects[j]])
                        mask = sitk.GetImageFromArray(data[objects[j]])
                        try:
                            print "PYRADIOMICS"
                            pyrad_res = extractor.execute(src_img, mask)
                            keys = []
                            values = []
                            
                            for key, value in pyrad_res.iteritems():
            #                    print "\t", key, ":", value
                                keys.append(key)
                                values.append(value)
                            if j==0:
                                writer.writerow(keys)
                            rows.append(values)
                                
                            firstOrderFeatures = firstorder.RadiomicsFirstOrder(src_img,mask)
                            firstOrderFeatures.enableAllFeatures()  # On the feature class level, all features are disabled by default.
                            firstOrderFeatures.calculateFeatures()                
            #                for (key,val) in six.iteritems(firstOrderFeatures.featureValues):                    
            #                    print("\t%s: %s" % (key, val))
                        except ValueError as e: 
                            print "ValueError OCCURED: ", e.message
                            pass
                    try:
                        writer.writerows(rows)
                    except Exception as e:
                        print "Something went wrong while exporting csv PyRadiomics"
                        print e.message    
            #            
            
            
    #        data = data.astype(np.float64)
    #        _min_val = data.min()
    #        _max_val = data.max()
    #        _mean_val = data.mean()
    #        _threshold =  float(float(_max_val - _min_val) * 99. / 100.)
    #        _max_cents = []
    #        for j in range(len(_maximum_centers)):
    #            z, y, x = _maximum_centers[j]
    #            value = self.transformed_pet[i][int(z)][int(y)][int(x)] 
    #            if value > _threshold:
    #                _max_cents.append((z, y, x))
                    
    #        self.hotspots.append(_max_cents)        
            
    #        print "number of lesions: %d, %d" % (len(_max_cents), num_objects)
    
            
            for j in range(len(_maximum_centers)):
                z, y, x = _maximum_centers[j]
                try:
                    diameter = regionprops(labeled)[j].equivalent_diameter
                    print "OTHER z, y, x, DIAMETER"
                    print z, y, x, diameter
                   
                    
                    name = "no_name"
                    self.hotspots[i][j] = (int(z), int(x), int(y), 0, False, False, diameter, False, name)
    #                self.hotspots[i].append((int(z), int(x), int(y), 0, False, False, diameter, False, name))
                    print "INSIDE detect_hotspots 33"
                    print z, int(z), j
                except:
                    print "INSIDE detect_hotspots 44"
                    print j
                    self.hotspots[i][j] = (0,0,0, 0, False, False, 0., False, "")
    #                self.hotspots[i].append((0, 0, 0, 0, False, False, 0, False, ""))
            
    #        # in case of resampling, the hotspots centers coordinates must be adapted            
    #        if self.with_resampling:        
    #            self.map_hotspot_coordinates(i)
        
        # appending bone metastases to hotspots
        neighborhood_size = 3
        data_bone_max = filters.maximum_filter(self.suv_mask[i], neighborhood_size)
        _labeled, num_bone_objects = ndimage.label(data_bone_max)
        self.labeled_met.append(_labeled)
#        labeled.append(_labeled)
        _maximum_centers = ndimage.center_of_mass(self.suv_mask[i], _labeled, range(1, num_bone_objects + 1))
        print "number of bone metastasis centers: %d, %d" % (len(_maximum_centers), num_bone_objects)
        props = []
        
#        self.hotspots[i].append(_maximum_centers)
        print "INSIDE detect_hotspots JUST BEFORE END"
        
        print len(self.hotspots[i])
        
        for j in range(len(_maximum_centers)):
            z, y, x = _maximum_centers[j]
            print z, y, x 
            if z < self.sliceCountCT[i] and z >=0:
                image_bone = self.bone_mask[i][int(z)]
                if z < .85 * self.sliceCountCT[i]:
                    image_bone = scipy.ndimage.morphology.binary_fill_holes(image_bone)
                value = image_bone[int(y)][int(x)]                
#                print z, x, y, value
#                print "DEBUG VALUE"
#                print value
                if value:
                    try:
                        diameter = regionprops(_labeled)[j].equivalent_diameter
                        self.region_props.append(regionprops(_labeled)[j])                        
                        name = "no_name"
                        self.hotspots[i].append((int(z), int(x), int(y), 4, False, False, diameter, True, name))
#                        self.hotspots[i][j] = (int(z), int(x), int(y), 4, False, False, diameter, True, name)
                        print ((int(z), int(x), int(y), 4, False, False, diameter, True, name))
                    except:
                        print "Something went wrong while adding bone lesion BONEBONE"
                        print j
#                        self.hotspots[i][j] = (0,0,0, 4, False, False, 0., True, "")
#                        self.hotspots[i].append((0,0,0, 4, False, False, 0., True, ""))
                else:
                    try:
                        diameter = regionprops(_labeled)[j].equivalent_diameter
                        self.region_props.append(regionprops(_labeled)[j]) 
                        self.hotspots[i].append((int(z), int(x), int(y), 0, False, False, diameter, True, name))
#                        self.hotspots[i][j] = (int(z), int(x), int(y), 0, False, False, diameter, False, name)
                        print ((int(z), int(x), int(y), 0, False, False, diameter, False, name))
                    except:
                        print "Something went wrong while adding normal lesion"
#                        self.hotspots[i][j] = (0, 0, 0, 0, False, False, 0, False, name)
#                        self.hotspots[i].append((0,0,0, 0, False, False, 0, False, ""))
#        self.region_props_met.append(props)
        
#        self.region_props.append(props)
        self.make_slice_lesion_mapping()
        
        print "INSIDE detect_hotspots END"
        print len(self.hotspots[i])
        print len(self.region_props)
        
        
    def all_the_magic(self):
        self.suv_bw = []        
        
        # loading pets
        self.pet_scans = []
        self.pet_imgs = []
        self.pets_after_resamp = []
        self.transformed_pet = []
        self.suv_mask = [] 
        self.mask_test = []
        self.mask_test_ct = []
        for i in range(len(PathsDicom)):
            self.patientIndex = i
            print "patient No: %d" % i
            self.pet_scans.append(self.load_scan(PathsDicom[i]))
            self.suv_bw.append(dhu.get_suv_bw(PathsDicom[i]))
            self.pet_imgs.append(self.get_pixels_hu(self.pet_scans[i]))            
            self.transformed_pet.append(self.pet_imgs[i] * self.suv_bw[i])
            self.patientNames.append(dhu.get_patient_name(PathsDicom[i]))
            print "Number of pet slices: %d" % len(self.transformed_pet[i])
            self.sliceCount[i] = self.sliceCountCT[i] = int(len(self.pet_scans[i]))
            
        # loading cts
        self.ct_scans = []
        self.ct_imgs = []
        self.cts_before_resampling = []
        self.cts_after_resamp = [] 
        self.compressed_cts = []
        for i in range(len(PathsDicomCT)):
            self.ct_scans.append(self.load_scan(PathsDicomCT[i]))
            self.ct_imgs.append(self.get_pixels_huCT(self.ct_scans[i]))
            self.cts_before_resampling.append(self.ct_imgs[i])
            if self.ct_compression:
                self.compress_CTs(i)
                self.zOffset = 0
            else:
                self.compressed_cts.append(self.ct_imgs[i])
            print "Number of ct slices: %d" % len(self.cts_before_resampling[i])
            if len(self.cts_before_resampling[i]) < len(self.transformed_pet[i]):
                self.sliceCount[i] = self.sliceCountCT[i] = int(len(self.ct_scans[i]))
        
        self.max_slice_count = int(max(self.sliceCount))
        
#        self.update_slice_containers(CT_RESOLUTION)
        
        # do resampling?
        if self.with_resampling:
            for i in range(len(PathsDicom)):
                self.do_resampling(i, self.sampling_resolution)
                self.do_resamplingCT(i, self.sampling_resolution)
        
        # create ct masks    
        self.bone_mask = [] # np.copy(self.cts_before_resampling)
        self.ct_mask = [] # np.copy(self.cts_before_resampling)
        self.soft_tissue_mask = [] # np.copy(self.cts_before_resampling)
        for i in range(len(PathsDicomCT)): 
            self.make_ct_masks(i)     
        
        # create suv mask    
        for i in range(len(PathsDicom)):
            self.create_suv_mask(i)
        
        # ground truth input
        self.local_maxima = []
        self.hotspots = [[]]
        self.labeled = []
        self.labeled_met = []
        if self.with_loading:  # load old data
            self.load_findings()
        else:
            for i in range(len(PathsDicom)):
                self.patientIndex = i
                self.hotspots[i] = []
                self.detect_hotspots(i)
        
        self.currentLesionIndex = 0
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            self.z = z
        else:
            self.z = 0
        self.zCT = self.z + self.zOffset

        # map slice numbers to lesion indexes
        self.slice_lesion_index_mapping = {}
#        if len(self.hotspots[self.patientIndex]) > 0:
#            self.make_slice_lesion_mapping()
        
        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        
        # a figure instance to plot on for CT
        self.figureCT = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvasCT = FigureCanvas(self.figureCT)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbarCT = NavigationToolbar(self.canvasCT, self)
                        
        # add some labels and buttons        
        self.patientIndex = len(PathsDicom) - 1
        self.patientName = self.patientNames[self.patientIndex]
        self.labelZSliceNo = QtGui.QLabel("%s, Slice # %d" % (self.patientName, self.z))        
        self.labelScrollSlices = QtGui.QLabel("                       scrolling step[s]")
        self.comboScrollStep = QtGui.QComboBox()
        self.comboScrollStep.addItems(["1", "5", "10"])
        self.comboScrollStep.currentIndexChanged.connect(self.switchScrollStep)
        self.comboScrollStep.setMaximumWidth(60)
        
        # open dicom dirs
        self.buttonOpenDicomDir = QtGui.QPushButton('Append Dicom PET Directory')
        self.buttonOpenDicomDir.clicked.connect(self.openDicomDir)
        self.buttonOpenDicomDirCT = QtGui.QPushButton('Append Dicom CT Directory')
        self.buttonOpenDicomDirCT.clicked.connect(self.openDicomDirCT)
        self.buttonOpenDicomDirCT.setEnabled(False)
        
        # select patient
        self.labelSelectPatient = QtGui.QLabel("Select patient:")        
        self.comboSelectPatient = QtGui.QComboBox()
        self.comboSelectPatient.addItems(self.patientNames)
        self.comboSelectPatient.currentIndexChanged.connect(self.comboSelectPatientChanged)
        self.comboSelectPatient.setCurrentIndex(len(self.patientNames) - 1)
        self.comboSelectPatient.setMaximumWidth(300)
                                          
        # toggle grayscale
        self.buttonToggleGrayscale = QtGui.QPushButton('Toggle grayscale')
        self.buttonToggleGrayscale.clicked.connect(self.toggleGrayscale)
        self.buttonToggleGrayscale.setMaximumWidth(300)
        
        # switcch ct masks        
        self.labelSelectCTMask = QtGui.QLabel("Select ct mask:")        
        self.comboSwitchMasks = QtGui.QComboBox()
        self.comboSwitchMasks.addItems(["Original CT", "Bone mask", "Soft tissue mask", "Bone metastases"])
        self.comboSwitchMasks.currentIndexChanged.connect(self.switchMasks)
        self.comboSwitchMasks.setMaximumWidth(300)
        
        # toggle suv overlay
        self.showSUVOverlay = True
        self.buttonToggleSUVOverlay = QtGui.QPushButton('Toggle SUV overlay')
        self.buttonToggleSUVOverlay.clicked.connect(self.toggleSUV)
        self.buttonToggleSUVOverlay.setMaximumWidth(300)        
        
        # image type, e. g., original, thresholed,...
        # usig combo boxes to select differetnt image types
        self.labelImageTypes = QtGui.QLabel("Select image type:")
        self.comboImageThresholds = QtGui.QComboBox()
        self.comboImageThresholds.addItems(["Original PET image", "CT image"])
        self.comboImageThresholds.currentIndexChanged.connect(self.comboThresSelectChanged)
        self.comboImageThresholds.setMaximumWidth(300)

        # ground truth input buttons and text
        self.radioYes = QtGui.QRadioButton('Yes')
        self.radioYes.toggled.connect(lambda:self.btnstate(self.radioYes))
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        else:
            flag = False
        self.radioYes.setChecked(flag)
        self.radioNo = QtGui.QRadioButton('No')
        self.radioNo.setChecked(not flag)
        self.radioNo.toggled.connect(lambda:self.btnstate(self.radioNo))
        self.radioGroup1 = QtGui.QButtonGroup()
        self.radioGroup1.addButton(self.radioYes)
        self.radioGroup1.addButton(self.radioNo)
        
        lesion_volume = 0
        
#        self.textbox_hotspot_name = QtGui.QLineEdit()        
#        self.textbox_hotspot_name.setEnabled(True)
#        self.textbox_hotspot_name.setText("e.g., bone 1")
#        self.textbox_hotspot_name.setReadOnly(False)
#        self.textbox_hotspot_name.setToolTip("should be the same as in Mediso")
#        self.textbox_hotspot_name.setFocusPolicy(QtCore.Qt.NoFocus)
        
        
        # text edit to adjust hotspot name
        self.dlgLesionName = QtGui.QDialog()
        self.dlgLesionName.setBaseSize(500,200)
        self.dlgLesionName.setWindowTitle("Adjust current hotspot name")
        self.textLesionName = QtGui.QLineEdit(self.dlgLesionName)
        self.textLesionName.setFixedWidth(100)
        name = "hotspot1"
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.textLesionName.setText(name)
        self.buttonAdjustLesionName = QtGui.QPushButton('Adjust hotspot name')
        self.buttonAdjustLesionName.clicked.connect(self.adjustLesionName)
        self.buttonAdjustLesionName.setMaximumWidth(300)
        self.buttonApplyNewName = QtGui.QPushButton("Apply new name")
        self.buttonApplyNewName.clicked.connect(self.applyNewName)
        self.buttonApplyNewName.setMaximumWidth(300)
                
        self.layoutLesionName = QtGui.QGridLayout()
        self.layoutLesionName.addWidget(self.textLesionName, 0,0,1,1)
        self.layoutLesionName.addWidget(self.buttonApplyNewName, 0,1,1,1)
        self.dlgLesionName.setLayout(self.layoutLesionName)
        
        
#        self.label_name_hotspot = QtGui.QLabel("Name the hotspot:")
        name = "no_name"
        if len(self.hotspots[self.patientIndex]) > 0:
            self.textDiagnosis1 = "%d hotspot(s) are detected for this patient" % len(self.hotspots[self.patientIndex])             
            lesion_volume = self.aproximate_lesion_volume(self.patientIndex, self.currentLesionIndex)
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
#            self.textbox_hotspot_name.setVisible(True)
#            self.label_name_hotspot.setVisible(True)
        else:
            self.textDiagnosis1 = "0 hotspot(s) are detected for this patient"             
#            self.textbox_hotspot_name.setVisible(False)
#            self.label_name_hotspot.setVisible(False)
        
        self.textDiagnosis2 = "\r\nLesion num. %d (named: %s) is currently shown\r\nApproximate lesion volume: \r\n%f [mm3] \r\nIs the annotated spot pathological?" % (self.currentLesionIndex + 1, str(name), lesion_volume)
        self.labelDiagnosis = QtGui.QLabel(self.textDiagnosis1 + self.textDiagnosis2)
        
        
        # label liver, kidneys, bone metastases,...
        self.labelDiagnosis2 = QtGui.QLabel("Does the hotspot represent\r\nany of the following organs?")
        self.diagVLayout = QtGui.QVBoxLayout()
        self.diagLayout0 = QtGui.QHBoxLayout()
        self.diagLayout = QtGui.QHBoxLayout()
        self.diagLayout2 = QtGui.QHBoxLayout()
        self.diagLayout3 = QtGui.QHBoxLayout()
        self.diagLayout4 = QtGui.QHBoxLayout()
        
#        self.diagLayout0.addWidget(self.label_name_hotspot)
#        self.diagLayout0.addWidget(self.textbox_hotspot_name)
        flagLiver = False
        flagLung = False
        flagKidney = False
        flagBone = False
        flagBladder = False
        flagProstate = False
        flagLymph = False
        flagSalG = False
        flagSpleen = False
        flagOther = True
        category, flagManual, flagSizeChanged, diameter, flag, name = 0, False, False, 0., False, ""
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
            print "INSIDE all_the_magic, BEFORE defining radio buttons"
            print z, x, y, category, flagManual, flagSizeChanged, diameter, flag
        
        # liver
        self.checkLiver = QtGui.QRadioButton("Liver")
        self.checkLiver.setChecked(flagLiver)                
        self.checkLiver.toggled.connect(lambda:self.btnstate_check(self.checkLiver))
        self.diagLayout.addWidget(self.checkLiver)
        # liver
        self.checkLung = QtGui.QRadioButton("Lung")
        self.checkLung.setChecked(flagLung)                
        self.checkLung.toggled.connect(lambda:self.btnstate_check(self.checkLung))
        self.diagLayout.addWidget(self.checkLung)
        # kidney
        self.checkKidney = QtGui.QRadioButton("Kidney")
        self.checkKidney.setChecked(flagKidney)        
        self.checkKidney.toggled.connect(lambda:self.btnstate_check(self.checkKidney))
        self.diagLayout.addWidget(self.checkKidney)        
        # bone metastasis
        self.checkBone = QtGui.QRadioButton("Bone")
        self.checkBone.setChecked(flagBone)        
        self.checkBone.toggled.connect(lambda:self.btnstate_check(self.checkBone))
        self.diagLayout2.addWidget(self.checkBone)
        # bladder
        self.checkBladder = QtGui.QRadioButton("Bladder")
        self.checkBladder.setChecked(flagBladder)        
        self.checkBladder.toggled.connect(lambda:self.btnstate_check(self.checkBladder))
        self.diagLayout2.addWidget(self.checkBladder)
        # prostate
        self.checkProstate = QtGui.QRadioButton("Prostate")
        self.checkProstate.setChecked(flagProstate)        
        self.checkProstate.toggled.connect(lambda:self.btnstate_check(self.checkProstate))
        self.diagLayout2.addWidget(self.checkProstate)
        # lymph node
        self.checkLymph = QtGui.QRadioButton("Lymph node")
        self.checkLymph.setChecked(flagLymph)        
        self.checkLymph.toggled.connect(lambda:self.btnstate_check(self.checkLymph))
        self.diagLayout3.addWidget(self.checkLymph)
        # sal. glands
        self.checkSalG = QtGui.QRadioButton("Sal. gland")
        self.checkSalG.setChecked(flagSalG)        
        self.checkSalG.toggled.connect(lambda:self.btnstate_check(self.checkSalG))
        self.diagLayout3.addWidget(self.checkSalG)
        # spleen
        self.checkSpleen = QtGui.QRadioButton("Spleen")
        self.checkSpleen.setChecked(flagSpleen)        
        self.checkSpleen.toggled.connect(lambda:self.btnstate_check(self.checkSpleen))
        self.diagLayout3.addWidget(self.checkSpleen)
        # non of the above!
        self.checkOther = QtGui.QRadioButton("Other tissue")
        self.checkOther.setChecked(flagOther)        
        self.checkOther.toggled.connect(lambda:self.btnstate_check(self.checkOther))
        self.diagLayout4.addWidget(self.checkOther)
        
        self.radioGroup2 = QtGui.QButtonGroup()
        self.radioGroup2.addButton(self.checkLiver)
        self.radioGroup2.addButton(self.checkLung)
        self.radioGroup2.addButton(self.checkKidney)
        self.radioGroup2.addButton(self.checkBone)
        self.radioGroup2.addButton(self.checkBladder)
        self.radioGroup2.addButton(self.checkProstate)
        self.radioGroup2.addButton(self.checkLymph)
        self.radioGroup2.addButton(self.checkSalG)
        self.radioGroup2.addButton(self.checkSpleen)
        self.radioGroup2.addButton(self.checkOther)
        
        # addding theme to the vertical layout
        self.diagVLayout.addLayout(self.diagLayout0)
        self.diagVLayout.addLayout(self.diagLayout)
        self.diagVLayout.addLayout(self.diagLayout2)
        self.diagVLayout.addLayout(self.diagLayout3)
        self.diagVLayout.addLayout(self.diagLayout4)
                
        self.buttonEraseSeeds = QtGui.QPushButton('Erase lesions on this slice')
        self.buttonEraseSeeds.clicked.connect(self.eraseSeeds)
        self.buttonEraseSeeds.setMaximumWidth(300)
        self.buttonSaveAllDiagnoses = QtGui.QPushButton('Save diagnoses for this patient')
        self.buttonSaveAllDiagnoses.clicked.connect(self.save_findings)
        self.buttonSaveAllDiagnoses.setMaximumWidth(300)
        
        
        # buttons and labels to train and predict label for current lesion
        self.buttonTrainDataSet = QtGui.QPushButton('Train the dataset')
        self.buttonTrainDataSet.clicked.connect(self.train_dataset)
        self.buttonPredictDiagnosisLabel = QtGui.QPushButton('Predict diagnosis for this lesion')
        self.buttonPredictDiagnosisLabel.clicked.connect(self.predict_diagnosis_label)
        self.buttonPredictDiagnosisLabel.setEnabled(False)
        self.labelPrediction = QtGui.QLabel()
        
        
        # hotspots navigation buttons
        self.buttonNextLesion = QtGui.QPushButton('Next lesion')
        self.buttonNextLesion.clicked.connect(self.gotoNextLesion)
        self.buttonPrevLesion = QtGui.QPushButton('Previous lesion')
        self.buttonPrevLesion.clicked.connect(self.gotoPrevLesion)
        
        # text edit to adjust lesion sizes
        self.dlgLesionSize = QtGui.QDialog()
        self.dlgLesionSize.setBaseSize(500,200)
        self.dlgLesionSize.setWindowTitle("Adjust current lesion diameter")
        self.textLesionSize = QtGui.QLineEdit(self.dlgLesionSize)
        self.textLesionSize.setFixedWidth(100)
        diameter = 0
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.textLesionSize.setText("%f" % float(diameter))
        self.buttonAdjustLesionSize = QtGui.QPushButton('Adjust lesion diameter')
        self.buttonAdjustLesionSize.clicked.connect(self.adjustLesionDiameter)
        self.buttonAdjustLesionSize.setMaximumWidth(300)
        self.buttonApplyNewDiameter = QtGui.QPushButton("Apply new diameter")
        self.buttonApplyNewDiameter.clicked.connect(self.applyNewDiameter)
        self.buttonApplyNewDiameter.setMaximumWidth(300)
        self.diagVLayout.addWidget(self.buttonAdjustLesionName)
        self.diagVLayout.addWidget(self.buttonAdjustLesionSize)
        self.diagVLayout.addWidget(self.buttonEraseSeeds)
        self.diagVLayout.addWidget(self.buttonPredictDiagnosisLabel)
        self.diagVLayout.addWidget(self.labelPrediction)
        self.labelPrediction.setVisible(False)
        
        self.layoutLesionDiameter = QtGui.QGridLayout()
        self.layoutLesionDiameter.addWidget(self.textLesionSize, 0,0,1,1)
        self.layoutLesionDiameter.addWidget(self.buttonApplyNewDiameter, 0,1,1,1)
        self.dlgLesionSize.setLayout(self.layoutLesionDiameter)
        
        
        # progress bar for loading new patient
        self.progressDlg = QtGui.QDialog()
        self.progressDlg.setWindowTitle("Loading new patient data, please wait ...")
        self.progress = QtGui.QProgressBar(self.progressDlg)
        self.progress.setGeometry(100, 80, 250, 20)
        self.progress.setVisible(False)
        
        # progress bar for saving diagnoses data
        self.progressSaveDlg = QtGui.QDialog()
        self.progressSaveDlg.setWindowTitle("Saving diagnoses data, please wait ...")
        self.progressSave = QtGui.QProgressBar(self.progressSaveDlg)
        self.progressSave.setGeometry(100, 80, 250, 20)
        self.progressSave.setVisible(False)
        
        # initiate diagnosis ui elements
        for i in range(len(PathsDicomCT)):
            self.patientIndex = i
            if len(self.hotspots[i]) > 0:
                for j in range(len(self.hotspots[i])-1):
                    self.currentLesionIndex = j
#                    print "DEBUG LESION"
#                    print j
                    self.update_diagnosis_label(True)
            else:
                    self.update_diagnosis_label(False)
        self.currentLesionIndex = 0
        self.patientIndex = len(PathsDicom) - 1
        # create layout
        self.refresh_layout()
    
    def adjustLesionName(self):
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.textLesionName.setText(str(name))
        self.dlgLesionName.show()
        
    def applyNewName(self):
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.hotspots[self.patientIndex][self.currentLesionIndex] = z, x, y, category, flagManual, flagSizeChanged, diameter, flag, str(self.textLesionName.text())
        self.dlgLesionName.close()
        self.dlgLesionName.hide()
        self.update_diagnosis_label(True)
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
    def adjustLesionDiameter(self):
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.textLesionSize.setText("%f" % float(diameter))
        self.dlgLesionSize.show()
        
    def applyNewDiameter(self):
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.hotspots[self.patientIndex][self.currentLesionIndex] = z, x, y, category, flagManual, True, float(self.textLesionSize.text()), flag, name
        self.dlgLesionSize.close()
        self.dlgLesionSize.hide()
        self.update_diagnosis_label(True)
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
    # controls whether the diagnoses ui elements are shown or not
    def update_diagnosis_label(self, in_flag):
#        print "INSIDE update_diagnosis_label"        
#        print self.currentLesionIndex
#        print self.hotspots[self.patientIndex][self.currentLesionIndex]
        self.textDiagnosis1 = "%d hotspot(s) are detected for this patient\r\n" % len(self.hotspots[self.patientIndex]) 
        if in_flag:            
#            print self.currentLesionIndex
            lesion_volume = self.aproximate_lesion_volume(self.patientIndex, self.currentLesionIndex)
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            
            self.textDiagnosis2 = "\r\nLesion num. %d (named: %s) is currently shown\r\nApproximate lesion volume: \r\n%f [mm3] \r\nIs the annotated spot pathological?" % (self.currentLesionIndex + 1, str(name), lesion_volume)
            self.radioYes.setVisible(True)
            self.radioNo.setVisible(True)
            self.labelDiagnosis2.setVisible(True)
            self.checkLiver.setVisible(True)
            self.checkLung.setVisible(True)
            self.checkKidney.setVisible(True)
            self.checkBone.setVisible(True)
            self.checkBladder.setVisible(True)
            self.checkProstate.setVisible(True)
            self.checkLymph.setVisible(True)
            self.checkSalG.setVisible(True)
            self.checkSpleen.setVisible(True)
            self.checkOther.setVisible(True)
            self.buttonAdjustLesionSize.setVisible(True)
            self.buttonAdjustLesionName.setVisible(True)
            self.buttonEraseSeeds.setVisible(True)
            self.buttonPredictDiagnosisLabel.setVisible(True)
            self.labelPrediction.setVisible(True)
            
        else:
            self.textDiagnosis2 = "\r\n\r\n"        
            self.radioYes.setVisible(False)
            self.radioNo.setVisible(False)
            self.labelDiagnosis2.setVisible(False)
            self.checkLiver.setVisible(False)
            self.checkLung.setVisible(False)
            self.checkKidney.setVisible(False)
            self.checkBone.setVisible(False)
            self.checkBladder.setVisible(False)
            self.checkProstate.setVisible(False)
            self.checkLymph.setVisible(False)
            self.checkSalG.setVisible(False)
            self.checkSpleen.setVisible(False)
            self.checkOther.setVisible(False)
            self.buttonAdjustLesionSize.setVisible(False)
            self.buttonAdjustLesionName.setVisible(False)
            self.buttonEraseSeeds.setVisible(False)
            self.buttonPredictDiagnosisLabel.setVisible(False)
            self.labelPrediction.setVisible(False)
            
        self.labelDiagnosis.setText(self.textDiagnosis1 + self.textDiagnosis2)
        self.update_diagnosis_checkboxes()
    
    def create_suv_mask_test(self, index):
        from skimage import morphology
        from sklearn.cluster import KMeans
        from skimage import measure
        print "INSIDE create_suv_mask 0"
        if self.with_resampling:
            img = np.copy(self.pets_after_resamp[index])
        else:
            img = np.copy(self.transformed_pet[index])
        print "INSIDE create_suv_mask 1"
        row_size= img.shape[0]
        col_size = img.shape[1]
        print "INSIDE create_suv_mask 2"
        mean = np.mean(img)
        std = np.std(img)
        img = img-mean
        img = img/std
        # Find the average pixel value near the lungs
        # to renormalize washed out images
        middle = img[int(col_size/5):int(col_size/5*4),int(row_size/5):int(row_size/5*4)] 
        mean = np.mean(middle)  
        max = np.max(img)
        min = np.min(img)
        print "INSIDE create_suv_mask 3"
        # To improve threshold finding, I'm moving the 
        # underflow and overflow on the pixel spectrum
        img[img==max]=mean
        img[img==min]=mean
        #
        # Using Kmeans to separate foreground (soft tissue / bone) and background (lung/air)
        #
        kmeans = KMeans(n_clusters=2).fit(np.reshape(middle,[np.prod(middle.shape),1]))
        centers = sorted(kmeans.cluster_centers_.flatten())
        threshold = np.mean(centers)
        thresh_img = np.where(img<threshold,1.0,0.0)  # threshold the image
        print "INSIDE create_suv_mask 4"
        # First erode away the finer elements, then dilate to include some of the pixels surrounding the lung.  
        # We don't want to accidentally clip the lung.
    
        eroded = morphology.erosion(thresh_img,np.ones([3,3]))
        dilation = morphology.dilation(eroded,np.ones([8,8]))
        
        
        print "INSIDE create_suv_mask 5"
        labels = measure.label(dilation) # Different labels are displayed in different colors
        label_vals = np.unique(labels)
        regions = measure.regionprops(labels)
        print "INSIDE create_suv_mask 6"
        good_labels = []
        for prop in regions:
            B = prop.bbox
            if B[2]-B[0]<row_size/10*9 and B[3]-B[1]<col_size/10*9 and B[0]>row_size/5 and B[2]<col_size/5*4:
                good_labels.append(prop.label)
        mask = np.ndarray([row_size,col_size],dtype=np.int8)
        mask[:] = 0
    
        #
        #  After just the lungs are left, we do another large dilation
        #  in order to fill in and out the lung mask 
        #
        print "INSIDE create_suv_mask 7"
        for N in good_labels:
            mask = mask + np.where(labels==N,1,0)
        mask = morphology.dilation(mask,np.ones([10,10])) # one last dilation
        self.suv_mask.append(mask)    
        
    def mask_test_(self):      
        import skimage.segmentation as seg          
        import skimage.color as color
        if self.with_resampling:
            image = self.pets_after_resamp[self.patientIndex][int(self.z)]
        else:
            image = self.transformed_pet[self.patientIndex][int(self.z)]
        
        image_slic = seg.slic(image,n_segments=50)
        image = color.label2rgb(image_slic, image, kind='avg')
        
#        image_felzenszwalb = seg.felzenszwalb(image)
#        image = color.label2rgb(image_felzenszwalb, image, kind='avg')
    
    def create_suv_mask(self, index):
        import skimage.segmentation as seg          
        import skimage.color as color
        if self.with_resampling:
            data = np.copy(self.pets_after_resamp[index])
        else:
            data = np.copy(self.transformed_pet[index])
        
#        data_slic = seg.slic(data,n_segments=150, compactness=100, spacing=[2.5,0.5,0.5], multichannel=False)
        
#        data_slic = color.label2rgb(data_slic, data, kind='avg')
        
        mean = data.mean()
        std = data.std()
        data = data-mean
        data = data/std
        
        
        print "DEBUG MASKS"
#        print np.array(data_slic).shape
        _min_val = data.min()
        _max_val = data.max()
        _threshold_up =  float(float(_max_val - _min_val) *4./ 10.)      # 1/13.
        _threshold =  float(float(_max_val - _min_val) *3./ 10.)      # 1/13.
        data[data < _threshold] = 0
#        data[data > _threshold_up] = 0
        self.suv_mask.append(data)
#        self.mask_test.append(data_slic)
        
    def create_suv_mask_old(self, index):
        if self.with_resampling:
            data = np.copy(self.pets_after_resamp[index])
        else:
            data = np.copy(self.transformed_pet[index])
        _min_val = data.min()
        _max_val = data.max()
        if _min_val < 0: 
            data += np.absolute(_min_val)
            _min_val = data.min()
            _max_val = data.max()
        _threshold =  float(float(_max_val - _min_val) *1./ 10.)      # 1/13.
#        _threshold =  float(float(_mean_val))
        data[data < _threshold] = 0            
        self.suv_mask.append(data)
        
    def make_ct_masks(self, index):
        import skimage.segmentation as seg          
        import skimage.color as color
        if self.with_resampling:
            base_data = np.copy(self.cts_after_resamp[index])
        else:
            base_data = np.copy(self.compressed_cts[index])
        print "INSIDE make_ct_masks 1"
        print "patient no. %d" % index
        
        
#        data_slic = seg.slic(base_data,n_segments=512, compactness=100, spacing=[2.5,0.5,0.5], multichannel=False)
        
#        data_slic = color.label2rgb(data_slic, base_data, kind='avg')
#        self.mask_test_ct.append(data_slic)

#        bone_thresh = np.copy(base_data)
        
#        ct_min_thres = np.copy(base_data)
#        ct_max_thres = np.copy(base_data)
#        soft_tissue_min_thres = np.copy(base_data)
#        soft_tissue_max_thres = np.copy(base_data)
        _max, _min, _mean = base_data.max(), base_data.min(), base_data.mean()
        
        print "INSIDE make_ct_masks 2"
        print _max, _min, _mean
        
        if _min < 0:
            base_data += np.absolute(_min)
            _max, _min, _mean = base_data.max(), base_data.min(), base_data.mean()
        
#        bone_thresh_max = np.copy(base_data)
        # apply bone mask
        max_threshold = 4000.
#        min_threshold = 200.
        min_threshold = 1024. + float(2. * float(_max - _min) / 10.)
#        bone_thresh = bone_thresh > min_threshold
#        bone_thresh_max = bone_thresh_max < max_threshold
#        bone_thresh[bone_thresh>max_threshold] = 0
        base_data = base_data > min_threshold 
#        bone_thresh_max = base_data < max_threshold
        self.bone_mask.append(base_data)
        
        
#        image3 = image1 * image2

#        max_threshold = 50 
#        min_threshold = 15 
        # apply liver mask
#        ct_min_thres = ct_min_thres > min_threshold
#        ct_max_thres = ct_max_thres < max_threshold
#        self.ct_mask.append(ct_min_thres * ct_max_thres)
#        self.ct_mask_max = ct_max_thres
        
        # soft tissue mask
        
        if self.with_resampling:
            base_data = np.copy(self.cts_after_resamp[index])
            base_data_max = np.copy(self.cts_after_resamp[index])
        else:
            base_data = np.copy(self.compressed_cts[index])
            base_data_max = np.copy(self.compressed_cts[index])
#            
        max_threshold = -90 #400
        min_threshold = -120   #0
#        soft_tissue_min_thres = soft_tissue_min_thres > min_threshold
#        soft_tissue_max_thres = soft_tissue_max_thres < max_threshold
        base_data = base_data > min_threshold
        base_data_max = base_data_max < max_threshold
#        self.soft_tissue_mask.append(soft_tissue_min_thres * soft_tissue_max_thres)
        self.soft_tissue_mask.append(base_data * base_data_max)
        print "INSIDE make_ct_masks 3"
        
    def compress_CTs(self, index):        
        _compressed_cts = np.zeros([len(self.cts_before_resampling[index]), PET_RESOLUTION, PET_RESOLUTION])
        for z in range(len(self.cts_before_resampling[index]) - 1):                
            _compressed_cts[z, 27:227, 27:227] = np.copy(cv2.resize(self.cts_before_resampling[index][z],(200, 200)))
        self.compressed_cts.append(_compressed_cts)
        
    def openDicomDir(self):
        self.dicomDir = str(QtGui.QFileDialog.getExistingDirectory(self, "Select dicom PET directory"))
        if self.dicomDir + '/' in PathsDicom:
            msg = QtGui.QMessageBox()
            msg.setText("The data from the selected path are already processed, please select another path")
            msg.exec_()
            self.openDicomDir()
        else:
            PathsDicom.append(self.dicomDir + '/')
            self.buttonOpenDicomDir.setEnabled(False)
            self.buttonOpenDicomDirCT.setEnabled(True)
        
    def openDicomDirCT(self):
        self.dicomDir = str(QtGui.QFileDialog.getExistingDirectory(self, "Select dicom CT directory"))
        if self.dicomDir + '/' in PathsDicomCT:
            msg = QtGui.QMessageBox()
            msg.setText("The data from the selected path are already processed, please select another path")
            msg.exec_()
            self.openDicomDirCT()
        else:
            self.progress.setValue(0)
#            self.progress.labelWait.setVisible(True)
            self.progress.setVisible(True)            
            PathsDicomCT.append(self.dicomDir + '/')
            self.patientIndex = len(PathsDicomCT) - 1
            self.progressDlg.show()
            self.add_new_patient()         
#            self.progress.labelWait.setVisible(False)
            self.progress.setVisible(False)
            self.buttonOpenDicomDir.setEnabled(True)
            self.buttonOpenDicomDirCT.setEnabled(False)
            self.progressDlg.close()
            self.plot()
            self.plotCT()
            self.adjustDiagnosis()
            self.adjustDiagnosisCT()
            # sets the key press events policies
            self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
        
    def eraseSeeds(self):
        _z = self.z
        _zCT = self.zCT
        _li = self.currentLesionIndex
        self.make_slice_lesion_mapping()
        print "inside eraseSeeds 1"
        print len(self.hotspots[self.patientIndex])
        
        if (len(self.hotspots[self.patientIndex]) < 2) or (self.slice_lesion_index_mapping[self.z] == None):
            return
        _new_hotspots = []
        
        print "index of the lesion to be erased:"
        print self.slice_lesion_index_mapping
        for zz in range(len(self.hotspots[self.patientIndex])):
            if zz not in (self.slice_lesion_index_mapping[self.z]):
                _new_hotspots.append(self.hotspots[self.patientIndex][zz])
        self.hotspots[self.patientIndex] = sorted(_new_hotspots)

        print "inside eraseSeeds 2"
        print len(self.hotspots[self.patientIndex])
        self.make_slice_lesion_mapping()
        print "inside eraseSeeds 3"
        print len(self.hotspots[self.patientIndex])
        self.currentLesionIndex = _li
        print "inside eraseSeeds 4"
        print self.currentLesionIndex
#        self.z, x, y, flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagOther, flagManual, flagSizeChanged, diameter, flag = self.hotspots[self.patientIndex][self.currentLesionIndex]
#        self.zCT = self.z + self.zOffset
        self.z = _z
        self.zCT = _zCT
        print "inside eraseSeeds 5"
#        print self.currentLesionIndex, self.z, self.zCT
#        self.currentLesionIndex = self.slice_lesion_index_mapping[self.z][0]
        print "inside eraseSeeds 6"
#        print self.currentLesionIndex
        self.labelZSliceNo.setText("%s, Slice # %d" % (self.patientName, self.z))
        self.update_diagnosis_label(False)
        self.plot()
        self.plotCT()
#        self.adjustDiagnosis()
#        self.adjustDiagnosisCT()
        
    def initiate_findings(self):
        _findings = []
        self.findings = []
        for i in range(len(PathsDicom)):
                for j in range(len(self.hotspots[i])):
                    _findings.append(False)
                self.findings.append(_findings)
                
    def make_slice_lesion_mapping(self):
#        print "inside make_slice_lesion_mapping 1"
        if len(self.hotspots[self.patientIndex]) == 0:
            return
        self.hotspots[self.patientIndex] = sorted(self.hotspots[self.patientIndex])
#        print len(self.hotspots[self.patientIndex])
#        print "inside make_slice_lesion_mapping 2"
        self.slice_lesion_index_mapping = {}
#        print self.hotspots[self.patientIndex]
        _zz, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][0]        
        _indices = []
        for i in range(len(self.hotspots[self.patientIndex])):            
            zz, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][i]
#            print "inside make_slice_lesion_mapping 3"
#            print i, _zz, zz                
            if i == 0:
                _indices.append(i)
#            self.slice_lesion_index_mapping[zz] = _indices
            if _zz != zz:
                self.slice_lesion_index_mapping[_zz] = _indices
                _indices = []
                _zz = zz
            if i == len(self.hotspots[self.patientIndex]) - 1:
                    self.slice_lesion_index_mapping[zz] = _indices
            if i != 0:
                _indices.append(i)
        print "inside make_slice_lesion_mapping 4"
        print self.slice_lesion_index_mapping
        
    def btnstate(self,b):
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        
            if b.text() == "Yes":
                if b.isChecked() == True:
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, True, name)
                else:
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, False, name)
    				
            if b.text() == "No":
                if b.isChecked() == True:
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, False, name)
                else:
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, True, name)
    
    
    def btnstate_check(self,b):
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
            print "INSIDE btnstate_check", flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
            if b.text() == "Liver":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagLiver = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagLiver = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Lung":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagLung = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagLung = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Kidney":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagKidney = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagKidney = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Bone":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagBone = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagBone = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Bladder":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagBladder = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagBladder = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Prostate":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagProstate = True                    
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagProstate = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Lymph node":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagLymph = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagLymph = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Sal. gland":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagSalG = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagSalG = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Spleen":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagSpleen = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagSpleen = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                    
            if b.text() == "Other tissue":
                if b.isChecked() == True:
                    print b.text()+" is selected"
                    flagOther = True
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                else:
                    print b.text()+" is deselected"
                    flagOther = False
                    labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                    category = self.map_labels_to_categories(labels)
                    print category
                    self.hotspots[self.patientIndex][self.currentLesionIndex] = (z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                                
            
    def update_diagnosis_checkboxes(self):
        if len(self.hotspots[self.patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
#            print "INSIDE update_diagnosis_checkboxes," , flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
            
            self.radioYes.setChecked(flag)
            self.radioNo.setChecked(not flag)
            self.checkLiver.setChecked(flagLiver)
            self.checkLung.setChecked(flagLung)
            self.checkKidney.setChecked(flagKidney)
            self.checkBone.setChecked(flagBone)
            self.checkBladder.setChecked(flagBladder)
            self.checkProstate.setChecked(flagProstate)
            self.checkOther.setChecked(flagOther)
            self.checkLymph.setChecked(flagLymph)
            self.checkSalG.setChecked(flagSalG)
            self.checkSpleen.setChecked(flagSpleen)
        
    
    def gotoNextLesion(self):
        self.buttonPrevLesion.setEnabled(True)
        
        if self.currentLesionIndex < (len(self.hotspots[self.patientIndex]) - 1):
            self.currentLesionIndex += 1 
        else:
            self.currentLesionIndex = 0
            

           
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        print "INSIDE gotoNextLesion"
        print z, self.currentLesionIndex
        self.z = z
        self.zCT = z + self.zOffset
#        if self.z > self.sliceCount[self.patientIndex] - 1:
#            self.z = 0
#        if self.z < 0:
#            self.z = self.sliceCount[self.patientIndex] - 1
#        if self.zCT > self.sliceCountCT[self.patientIndex] + self.zOffset - 1:
#            self.zCT = self.zOffset
#        if self.zCT < self.zOffset:
#            self.zCT = self.sliceCountCT[self.patientIndex] + self.zOffset - 1
        self.labelZSliceNo.setText("%s, Slice # %d" % (self.patientName, self.z))
        self.update_diagnosis_label(True)
        self.plot()
        self.plotCT()    
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
    
    def gotoPrevLesion(self):
        self.buttonNextLesion.setEnabled(True)

        if self.currentLesionIndex > 0:            
            self.currentLesionIndex -= 1
        else:
            self.currentLesionIndex = len(self.hotspots[self.patientIndex]) - 1        
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        print "INSIDE gotoPrevLesion"
        print z, self.currentLesionIndex
        self.z = z
        self.zCT = z + self.zOffset
#        if self.z > self.sliceCount[self.patientIndex] - 1:
#            self.z = 0
#        if self.z < 0:
#            self.z = self.sliceCount[self.patientIndex] - 1
#        if self.zCT > self.sliceCountCT[self.patientIndex] + self.zOffset - 1:
#            self.zCT = self.zOffset
#        if self.zCT < self.zOffset:
#            self.zCT = self.sliceCountCT[self.patientIndex] + self.zOffset - 1
        self.labelZSliceNo.setText("%s, Slice # %d" % (self.patientName, self.z))
        self.update_diagnosis_label(True)
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
    def setChildrenFocusPolicy (self, policy):
        def recursiveSetChildFocusPolicy (parentQWidget):
            for childQWidget in parentQWidget.findChildren(QtGui.QWidget):
                childQWidget.setFocusPolicy(policy)
                recursiveSetChildFocusPolicy(childQWidget)
        recursiveSetChildFocusPolicy(self)
        
    def keyPressEvent(self, event):        
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
        flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
        if event.key() == QtCore.Qt.Key_Right:
            self.gotoNextLesion()
        elif event.key() == QtCore.Qt.Key_Left:
            self.gotoPrevLesion()
        elif event.key() == QtCore.Qt.Key_Up:
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, True, name)

        elif event.key() == QtCore.Qt.Key_Down:
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, False, name)

        elif event.key() == QtCore.Qt.Key_S:
            self.save_findings()
        
        elif event.key() == QtCore.Qt.Key_L:
            if not flagLiver:
                flagLung = flagLung = flagKidney = flagBone = flagBladder = flagProstate = flagOther = False
                flagLiver = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)                
        
        elif event.key() == QtCore.Qt.Key_G:
            if not flagLung:
                flagLiver = flagLung = flagKidney = flagBone = flagBladder = flagProstate = flagOther = False
                flagLung = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
    
        elif event.key() == QtCore.Qt.Key_K:
            if not flagKidney:
                flagLiver =  flagLung = flagBone = flagBladder = flagProstate = flagOther = False
                flagKidney = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
            
        elif event.key() == QtCore.Qt.Key_B:
            if not flagBone:
                flagLiver = flagLung = flagKidney  = flagBladder = flagProstate = flagOther = False
                flagBone = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                
        elif event.key() == QtCore.Qt.Key_R:
            if not flagBladder:
                flagLiver = flagLung = flagLung = flagKidney = flagBone = flagProstate = flagOther = False
                flagBladder = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
            
        elif event.key() == QtCore.Qt.Key_P:
            if not flagProstate:
                flagLiver = flagLung = flagLung = flagKidney = flagBone = flagBladder = flagOther = False
                flagProstate = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
                
        elif event.key() == QtCore.Qt.Key_O:
            if not flagOther:
                flagLiver = flagLung = flagLung = flagKidney = flagBone = flagBladder = flagProstate = False
                flagOther = True
                labels = flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther
                category = self.map_labels_to_categories(labels)
            self.hotspots[self.patientIndex][self.currentLesionIndex] = (int(z), x, y, category, flagManual, flagSizeChanged, diameter, flag, name)
        
        else:
            QtGui.QWidget.keyPressEvent(self, event)

        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
    def save_resampled_pet_slices(self):
        print "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
        print "SAVING RESAMPLED PET SLICES"
        for z, pet_slice in enumerate(self.pets_after_resamp[self.patientIndex]):
            pet_slice /= 255.0
            
            img = np.copy(pet_slice)
            vmin = np.min(img)
            vmax = np.max(img)
            if vmin < 0:
                img += abs(vmin)
                vmin += abs(vmin)
                vmax += abs(vmin)
            thres = 0.4 * vmax
#            Zm = np.ma.masked_where(img > thres, img)
#            Zm = img > thres
            Zm = np.where(img > thres, 1, 0)
            plt.imsave("/home/sobhan/src/r_scripts/pet1/%d.png" % z, img, cmap="gray")
            plt.imsave("/home/sobhan/src/r_scripts/pet_mask1/%d.png" % z, Zm, cmap="gray")
            
        
    def do_resampling(self, i, sampling_resolution):
        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "before resampling pets"

        print "patient No: %d" % i
        img_after_resamp, spacing = self.resample(self.pet_imgs[i], self.pet_scans[i], sampling_resolution)
        print img_after_resamp.shape
        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "ater resampling pets"

        tmpImg = np.zeros([len(img_after_resamp), CT_RESOLUTION, CT_RESOLUTION])
        for z in range(len(img_after_resamp)):
            tmpImg[z,:,:] = np.copy(cv2.resize(img_after_resamp[z], (PET_HIGHEST_RESOLUTION, PET_HIGHEST_RESOLUTION)))[PET_EXPAND_OFFSET:610,PET_EXPAND_OFFSET:610]
        print tmpImg.shape
        self.pets_after_resamp.append(tmpImg)
        print len(self.pets_after_resamp[i])
        
        # set slice counters
        self.sliceCount[i] = self.sliceCountCT[i] = int(len(img_after_resamp))
        self.save_resampled_pet_slices()
        
    def do_resamplingCT(self, i, sampling_resolution):
        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "before resampling cts"
        img_after_resamp, spacing = self.resample(self.ct_imgs[i], self.ct_scans[i], sampling_resolution)
        print img_after_resamp.shape

        print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        print "ater resampling cts"

        tmpImg = np.zeros([len(img_after_resamp), CT_RESOLUTION, CT_RESOLUTION])        
        for z in range(len(img_after_resamp)):
            tmpImg[z,:,:] = np.copy(cv2.resize(img_after_resamp[z], (CT_RESOLUTION, CT_RESOLUTION)))
        self.cts_after_resamp.append(tmpImg)
        print len(self.cts_after_resamp[i])    
        
        # set slice counters
        if len(img_after_resamp) < len(self.pets_after_resamp[i]):
            self.sliceCount[i] = self.sliceCountCT[i] = int(len(img_after_resamp))    
        
    # maps coordinates of hotspot centers after resapling
    def map_hotspot_coordinates(self, i):        
        for j in range(len(self.hotspots[i])):
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[i][j]
            _x = (float(float(x) * float(float(PET_HIGHEST_RESOLUTION)/float(PET_RESOLUTION))) - float(PET_EXPAND_OFFSET))
            _y = (float(float(y) * float(float(PET_HIGHEST_RESOLUTION)/float(PET_RESOLUTION))) - float(PET_EXPAND_OFFSET))                
            _z = (float(z) * float(self.sliceCount[i])/float(400))
            if _x > CT_RESOLUTION - 1: 
                _x = CT_RESOLUTION - 1
            if _y > CT_RESOLUTION - 1:
                _y = CT_RESOLUTION - 1
            self.hotspots[i][j] = (int(_z), _x, _y, category, flagManual, flagSizeChanged, diameter, flag, name)
        
    def wheelEvent(self,event):
        self.make_slice_lesion_mapping()
#        print "INSIDE wheelEvent 1"
        if self.isVerticalViewActive:
            return        
        self.x =self.x + event.delta()/120
#        print "OLD z, zCT: %d, %d" % (self.z, self.zCT)
#        print "INSIDE wheelEvent 2"
        if event.delta() > 0:
            self.up_wheel = True
            self.z += self.scrollStep       
            self.zCT += self.scrollStep    
        elif event.delta() < 0:
            self.up_wheel = False
            self.z -= self.scrollStep
            self.zCT -= self.scrollStep
            
#        print "INSIDE wheelEvent 3"
        if self.z > self.sliceCount[self.patientIndex] - 1:
            self.z = 0
        if self.z < 0:
            self.z = self.sliceCount[self.patientIndex] - 1
        if self.zCT > self.sliceCountCT[self.patientIndex] + self.zOffset - 1:
            self.zCT = self.zOffset
        if self.zCT < self.zOffset:
            self.zCT = self.sliceCountCT[self.patientIndex] + self.zOffset - 1
#        print "NEW z, zCT: %d, %d" % (self.z, self.zCT)
#        print "INSIDE wheelEvent 4"
        
        # refresh diagnosis radio buttons
#        print "INSIDE wheelEvent 5"

        self.labelZSliceNo.setText("%s, Slice # %d" % (self.patientName, self.z))            
        self.plot()
        self.plotCT() 
        if self.slice_lesion_index_mapping != {}:   
#            print "INSIDE wheelEvent 6"
            try:
                if len(self.slice_lesion_index_mapping[self.z]) > 0:
                    self.currentLesionIndex = self.slice_lesion_index_mapping[self.z][0] 
                    if self.currentLesionIndex < 0:
                        self.currentLesionIndex = len(self.hotspots[self.patientIndex]) 
                    if self.currentLesionIndex > len(self.hotspots[self.patientIndex]):
                        self.currentLesionIndex = 0
                    self.update_diagnosis_label(True)          
                    self.adjustDiagnosis()
                    self.adjustDiagnosisCT()       
    #            print self.currentLesionIndex
    #            if self.currentLesionIndex > 0:
#                    print "INSIDE wheelEvent 7"
                    
                else:
#                    print "INSIDE wheelEvent 8"
                    self.update_diagnosis_label(False)
            except Exception as e:
                    print "INSIDE wheelEvent 9"
                    print "something went wrong"
                    print e.message
                    self.update_diagnosis_label(False)
        else:
#            print "INSIDE wheelEvent 10"
            self.update_diagnosis_label(False)
#            print self.slice_lesion_index_mapping[self.z][0]
#         print "INSIDE wheelEvent 6"

            

    def adjustDiagnosis(self):        
#        b = self.findings[self.patientIndex][self.currentLesionIndex]
        flagLiver = False
        flagLung = False
        flagKidney = False
        flagBone = False
        flagBladder = False
        flagProstate = False
        flagLymph = False
        flagSalG = False
        flagSpleen = False
        flagOther = True
        if len(self.hotspots[self.patientIndex]) > 0:
#            print "INSIDE adjustDiagnosis"
#            print self.hotspots[self.patientIndex][self.currentLesionIndex]
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
            self.drawSeedCircles(self.hotspots[self.patientIndex][self.currentLesionIndex])        
        else:
            flag = False
        
        self.radioYes.setChecked(flag)
        self.radioNo.setChecked(not flag)
        self.btnstate(self.radioYes)
        self.checkLiver.setChecked(flagLiver)
        self.checkLung.setChecked(flagLung)
        self.checkKidney.setChecked(flagKidney)
        self.checkBone.setChecked(flagBone)
        self.checkBladder.setChecked(flagBladder)
        self.checkProstate.setChecked(flagProstate)
        self.checkOther.setChecked(flagOther)
#        self.btnstate_check(self.checkOther)
        
    def adjustDiagnosisCT(self):      
        pass
        if len(self.hotspots[self.patientIndex]) > 0:
            self.drawSeedCirclesCT(self.hotspots[self.patientIndex][self.currentLesionIndex])
    
    def showImage(self, imageType):
        self.isVerticalViewActive = False
        self.maskState = imageType
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
        
    def switchMasks(self, comboIndex):
        self.showImage(comboIndex + 1)
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
        self.update_diagnosis_label(True)
    
    def switchScrollStep(self, comboIndex):
        if comboIndex == 0:
            self.scrollStep = 1
        elif comboIndex == 1:
            self.scrollStep = 5
        elif comboIndex == 2:
            self.scrollStep = 10
        self.plot()
        self.plotCT()
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
        
    def toggleGrayscale(self):
        if self.showGrayscale == True:
            self.showGrayscale = False
        else:
            self.showGrayscale = True
        self.plot()
        self.plotCT()
        self.drawSeedCircles(self.allSeedPoints[self.patientIndex][self.z])
        self.drawSeedCirclesCT(self.allSeedPointsCT[self.patientIndex][self.zCT])
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
    
    def toggleSUV(self):
        if self.showSUVOverlay == True:
            self.showSUVOverlay = False
        else:
            self.showSUVOverlay = True
        self.plot()
        self.plotCT()
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
    
            
    def comboThresSelectChanged(self, comboIndex):
        self.showImage(comboIndex + 1)
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
        
    def comboSelectPatientChanged(self, comboIndex):
        self.patientIndex = comboIndex
        self.patientName = self.patientNames[comboIndex]
        self.labelZSliceNo.setText("%s, Slice # %d" % (self.patientName, self.z))
        self.make_slice_lesion_mapping()
        self.update_diagnosis_label(True)
        self.plot()
        self.plotCT()        
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
    
    def changeIndex(self):
        index = self.comboImageThresholds.currentIndex()
        if index < self.comboImageThresholds.count() - 1:
            self.comboImageThresholds.setCurrentIndex(index + 1)
        else:
            self.comboImageThresholds.setCurrentIndex(0)
        # sets the key press events policies
        self.setChildrenFocusPolicy(QtCore.Qt.NoFocus)
            
    def load_scan(self, path):
        slices = [dicom.read_file(path + '/' + s, force=True) for s in os.listdir(path)]
        slices.sort(key = lambda x: int(x.InstanceNumber))
        try:
            slice_thickness = np.abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2])
        except:
            slice_thickness = np.abs(slices[0].SliceLocation - slices[1].SliceLocation)
            
        for s in slices:
            s.SliceThickness = slice_thickness
        
        print "INSIDE load_scan"
        print "loaded %d slices" % len(slices)
        return slices
    
    def get_pixels_hu(self, scans):
        image = np.stack([s.pixel_array for s in scans])
        # Convert to int16 (from sometimes int16), 
        # should be possible as values should always be low enough (<32k)
        image = image.astype(np.int16)
    
        # Set outside-of-scan pixels to 1
        # The intercept is usually -1024, so air is approximately 0
#        image[image == -2000] = 0
#        
#        # Convert to Hounsfield units (HU)
#        intercept = scans[0].RescaleIntercept
#        slope = scans[0].RescaleSlope
#        
#        if slope != 1:
#            image = slope * image.astype(np.float64)
#            image = image.astype(np.int16)
#            
#        image += np.int16(intercept)
        
        return np.array(image, dtype=np.int16)
    
    def get_pixels_huCT(self, scans):
        image = np.stack([s.pixel_array for s in scans])
        # Convert to int16 (from sometimes int16), 
        # should be possible as values should always be low enough (<32k)
        image = image.astype(np.int16)
    
        # Set outside-of-scan pixels to 1
        # The intercept is usually -1024, so air is approximately 0
        image[image == -2000] = 0
        
        # Convert to Hounsfield units (HU)
        intercept = scans[0].RescaleIntercept
        slope = scans[0].RescaleSlope
        
        if slope != 1:
            image = slope * image.astype(np.float64)
            image = image.astype(np.int16)
            
        image += np.int16(intercept)
        
        print "INSIDE get_pixels_huCT"
        print image.shape
        
        return np.array(image, dtype=np.int16)


    def resample(self, image, scan, new_spacing=[1,1,1]):
        # Determine current pixel spacing
        spacing = map(float, ([scan[0].SliceThickness] + scan[0].PixelSpacing))
        spacing = np.array(list(spacing))
    
        resize_factor = spacing / new_spacing
        new_real_shape = image.shape * resize_factor
        new_shape = np.round(new_real_shape)
        real_resize_factor = new_shape / image.shape
        new_spacing = spacing / real_resize_factor
        
        image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
        
        return image, new_spacing

    def plot_test(self):      
        from skimage.color import rgb2gray
        from skimage.filters import sobel
        import skimage.segmentation as seg          
        import skimage.color as color
        from skimage.segmentation import mark_boundaries
        from skimage.util import img_as_float
        
        if self.with_resampling:
            image = self.pets_after_resamp[self.patientIndex][int(self.z)]
        else:
            image = self.transformed_pet[self.patientIndex][int(self.z)]
        
        image = img_as_float(image[::2, ::2])
        image_slic = seg.slic(image,n_segments=10, compactness=.01, sigma=1)
#        image_slic = color.label2rgb(image_slic, image, kind='avg')
        
        
        gradient = sobel(rgb2gray(image))
#        image_quick = seg.felzenszwalb(image, scale=100, sigma=0.5, min_size=150)
#        image_quick = color.label2rgb(image_quick, image, kind='avg')
        
#        image_felzenszwalb = seg.felzenszwalb(image)
#        image = color.label2rgb(image_felzenszwalb, image, kind='avg')
        
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.hold(False)

#        ax.imshow(image, cmap=plt.cm.gray)
        ax.imshow(mark_boundaries(image, image_slic))

        self.ax = ax 
        # refresh canvas
        self.canvas.draw()
        # init mouse annotation
        self.initMouseAnnotate()
    
        
    def plot(self):          
        if self.with_resampling:
            image = self.pets_after_resamp[self.patientIndex][int(self.z)]
        else:
            image = self.transformed_pet[self.patientIndex][int(self.z)]

#        image = self.mask_test[self.patientIndex][int(self.z)]
        
        # test labeled
#        image = self.labeled[self.patientIndex][int(self.z)] == self.currentLesionIndex
#        print image
        image = np.asarray(image)
        print "DEBUG PLOT"
        print image.shape
#        print image
        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.hold(False)

        ax.imshow(image, cmap=plt.cm.gray)

        self.ax = ax 
        # refresh canvas
        self.canvas.draw()
        # init mouse annotation
        self.initMouseAnnotate()
        
    def plotCT(self):          # CT image     
        import skimage.segmentation as seg          
        import skimage.color as color
        
        
        
        """ tests for lasso selector """
#        from tkinter import *
#
#        from matplotlib.widgets import LassoSelector
#        import matplotlib.image as mpimg
#        from pylab import *
#        from matplotlib import path
#        from tkinter.filedialog import askopenfilename
#        import tkinter as tk
#        from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg    
        
#        root = tk.Tk()        
            
        self.zCT = int(self.zCT)
        self.z = int(self.z)
        if self.ct_compression:
            image = self.compressed_cts[self.patientIndex][self.zCT]
        else:
            image = self.cts_after_resamp[self.patientIndex][self.zCT]
        
        
#        image_mask_test_ct = self.mask_test_ct[self.patientIndex][int(self.zCT)]

#        """ tests for region label prediction"""
#        from skimage.measure import label as sklabel
#        labeled_ct, num_regions = sklabel(image, return_num=True)
#        print "INSIDE plotCT"
#        print num_regions
#        props = regionprops(labeled_ct)
#        for i in range(0, num_regions):
#            if props[i].area > 1000:
#                try:
#                    print props[i].area, props[i].max_intensity, props[i].mean_intensity, props[i].min_intensity
#                except AttributeError as e:
#                    print e.message, "AttributeError"
##            for prop in props[i]:
##                print prop, props[0].prop
            
        

        # test skimage segmentation functions
#        image_slic = seg.slic(image,n_segments=200)        
#        image = color.label2rgb(image_slic, image, kind='avg')
#        image_felzenszwalb = seg.felzenszwalb(image)
#        image = color.label2rgb(image_felzenszwalb, image, kind='avg')
        
        image_bone = self.bone_mask[self.patientIndex][self.zCT]
        if self.zCT < .85 * self.sliceCountCT[self.patientIndex]:
            image_bone = scipy.ndimage.morphology.binary_fill_holes(image_bone)
#        image_ct_mask = self.ct_mask[self.patientIndex][self.zCT]
#        image_ct_mask_max = self.ct_mask_max[self.zCT]
        image_soft_tissue_mask = self.soft_tissue_mask[self.patientIndex][self.zCT]
#        if self.with_resampling:
        image_bone_metastases = self.suv_mask[self.patientIndex][int(self.z)] * image_bone
        

        image_suv = self.suv_mask[self.patientIndex][int(self.z)]
                               
        # create an axis
        ax = self.figureCT.add_subplot(111)

        # discards the old graph
        ax.hold(False)

        # plot data
#        if self.showGrayscale:
#            ax.imshow(image, cmap=plt.cm.gray)
#        else:
#            ax.imshow(image)
        if self.maskState == 1:
            ax.imshow(image, cmap=plt.cm.gray)
        elif self.maskState == 2:
            ax.imshow(image_bone, cmap=plt.cm.gray)
        elif self.maskState == 3:
            ax.imshow(image_soft_tissue_mask, cmap=plt.cm.gray)            
#        elif self.maskState == 3:
#            ax.imshow(image_ct_mask, cmap=plt.cm.gray)            
        elif self.maskState == 4:            
            ax.imshow(image_bone_metastases, cmap=plt.cm.gray)
#            ax.imshow(image_mask_test_ct, cmap=plt.cm.gray)
            
#        elif self.maskState == 5:
#            image  = image_ct_mask * image_suv
#            ax.imshow(image, cmap=plt.cm.gray)

        if self.showSUVOverlay:
            ax.hold(True)
            ax.imshow(image_suv, cmap=plt.cm.viridis, alpha=.4, interpolation='bilinear')

        
        self.axCT = ax 
        # refresh canvas
        self.canvasCT.draw()
        
#        selector = SelectFromCollection(ax, image)

        # Creating two frames
#        topFrame = tk.Frame()
#        bottomFrame = tk.Frame()
#        topFrame.pack(side="top")
#        bottomFrame.pack(side="bottom")
#        
#        canvas = FigureCanvasTkAgg(self.figureCT, bottomFrame)
#        canvas.show()
#        canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
                
#        lasso = LassoSelector(self.axCT, onselect)
        
#        root.title("Ett nytt test")
#        root.mainloop()

#        data = [Datum(*xy) for xy in np.asarray(image)]
#        lasso = LassoManager(self.axCT, data)

#        lasso = LassoManager(self.axCT, onselect=self.onselect, axIdx=0)
        
        # init mouse annotation
#        self.initMouseAnnotateCT()        
  
    def onselect(self, verts):

            print "INSIDE onselect"
            print (verts)
            
    def aproximate_lesion_volume(self, patientIndex, lesionIndex):
#        print "INSIDE aproximate_lesion_volume"
        print "current lesion:", lesionIndex
        if len(self.hotspots[patientIndex]) > 0:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[patientIndex][lesionIndex]        
#            if (not flagManual) and (not flagSizeChanged):
#                diameter = regionprops(self.labeled[patientIndex])[lesionIndex].equivalent_diameter                    
#                if (not flag):
#                    if (lesionIndex < len(regionprops(self.labeled[patientIndex]))):
#    #                    print len(regionprops(self.labeled[patientIndex]))
#    #                    print flag
#                        print lesionIndex
#    #                    print len(self.labeled)
#    #                    print patientIndex
#                        diameter = regionprops(self.labeled[patientIndex])[lesionIndex].equivalent_diameter    
#                    else: #(lesionIndex >= len(regionprops(self.labeled[patientIndex]))):
#                        lesionIndex = lesionIndex - len(regionprops(self.labeled[patientIndex]))
#    #                    lesionIndex = lesionIndex - len(self.region_props_met[patientIndex])
#    #                    print "DEBUG LESION INDEX"
#    #                    print len(regionprops(self.labeled[patientIndex]))
#    #                    print flag
#                        print lesionIndex
#    #                    print len(self.labeled)
#    #                    print patientIndex
#                        diameter = regionprops(self.labeled_met[patientIndex])[lesionIndex].equivalent_diameter    
#                else: 
#                    if (lesionIndex >= len(regionprops(self.labeled[patientIndex]))):
#    #                   (flag) and (lesionIndex >= len(self.region_props_met[patientIndex])):
#                        lesionIndex = lesionIndex - len(regionprops(self.labeled[patientIndex]))
#    #                    lesionIndex = lesionIndex - len(self.region_props_met[patientIndex])
#                        print lesionIndex
#    #                    print len(self.labeled_met)
#    #                    diameter = self.region_props_met[patientIndex][lesionIndex].equivalent_diameter
#                        diameter = regionprops(self.labeled_met[patientIndex])[lesionIndex].equivalent_diameter    
#                    else: # and (lesionIndex < len(regionprops(self.labeled[patientIndex]))):
#    #                if (flag) and (lesionIndex < len(self.region_props_met[patientIndex])):
#                        print lesionIndex
#    #                    print len(self.labeled_met)
#    #                    print len(self.hotspots[patientIndex])
#    #                    diameter = self.region_props_met[patientIndex][lesionIndex].equivalent_diameter
#                        diameter = regionprops(self.labeled[patientIndex])[lesionIndex].equivalent_diameter    
            try:            
                pixel_spacing = dhu.get_pixel_spacing(0, PathsDicom[patientIndex])
            except Exception as e:
                print "Something went wrong while importing pixe_spacing for patient No. %d" % patientIndex
                print e.message
                # setting it to a default value            
                pixel_spacing = (2.65, 2.65)
            return float(float(4. / 3.) * math.pi * pow(float(.5 * diameter * float(pixel_spacing[0])), 3))
        
    def load_findings(self):
        for i in range(len(PathsDicom)):
            data_path = self.WORKING_DIR + '/data_%d.json' % i
            with open(data_path, 'r') as f:
                self.hotspots.append(json.load(f))
        
    def save_findings(self):
        # Writing JSON data
        self.WORKING_DIR = str(QtGui.QFileDialog.getExistingDirectory(self, "Select diagnoses output directory"))
        self.progressSaveDlg.show()
        self.progressSave.setVisible(True)
        self.progressSave.setValue(10)
        dirs_path = self.WORKING_DIR + '/metadata.txt'
        with open(dirs_path, 'w') as fd:
            for i in range(len(PathsDicom)):
                patient_num = "Patient No. %d: \r\n" % i
                fd.write(patient_num)
                fd.write(self.patientNames[i] + "\r\n")
                fd.write("Data paths: \r\n")
                data_path = self.WORKING_DIR + '/data_%d.json' % i            
                with open(data_path, 'w') as f:
                    json.dump(self.hotspots[i], f)
                    f.close()            
                fd.write(PathsDicom[i] + "\r\n")
                fd.write(PathsDicomCT[i] + "\r\n")
                fd.write("--------------------------------------- \r\n")     
        self.progressSave.setValue(35)
        if self.with_csv_export:
            self.export_as_csv()
        self.progressSave.setValue(100)
        self.progressSaveDlg.close()

    def export_as_csv(self):
        print "INSIDE export_as_csv 1"
        i = self.patientIndex
        self.progressSave.setValue(40 + i * 5)                                        
        try:
            pixel_spacing = dhu.get_pixel_spacing(0, PathsDicom[i])
            slice_thickness = float(dhu.get_slice_thickness(0, PathsDicom[i]))
        except Exception as e:
            print "Something went wrong while importing dicom header info for patient No. %d" % i
            print e.message
            # setting it to a default value            
            pixel_spacing = (2.65, 2.65)
            slice_thickness = 2.5

        data_path = self.WORKING_DIR + '/hotspots_centers_%d.csv' % i 
        print "INSIDE export_as_csv exporting data for patient No. %d" % i
        with open(data_path, 'wb') as csvfile:
            first_row = ['PatientName', 'PatientID', 'PatientBirthDate', 'PatientSize', 'PatientWeight', 'PatientSex', 'PixelSpacing', 'SpacingBetweenSlices']
            _pixel_spacing = '%f\\%f' % (float(pixel_spacing[0]), float(pixel_spacing[1]))
            second_row = ['patient1', 'id1', '19.febr.57', 1.830000043, 92, 'M', _pixel_spacing, slice_thickness]
            third_row = ['']
        
            fieldnames = ['x(mm)', 'y(mm)', 'z(mm)', 'ValuePET', 'ValueCT', 'flagLiver', 'flagLung', 'flagKidney', 'flagBone', 'flagBladder', 'flagProstate', 'flagLymph', 'flagSalG', 'flagSpleen', 'flagOther', 'flagManual', 'flagSizeChanged', 'LesionDiameter', 'flagPathological', 'hotspotName']
            writer = csv.writer(csvfile, delimiter=';',
                                     quoting=csv.QUOTE_MINIMAL)
            
            writer.writerow(first_row)
            writer.writerow(second_row)
            writer.writerow(third_row)
            writer.writerow(fieldnames)                

            try:
                writer.writerows(self.extract_data())
            except Exception as e:
                print "Something went wrong while exporting csv YYY"
                print e.message        
    
    def extract_data(self):
        i = self.patientIndex
        try:
            affine_mat = dhu.get_index_to_voxel_transform_matrix(PathsDicom[i])
        except Exception as e:
            print "Something went wrong while importing dicom header info for patient No. %d" % i
            print e.message
            # setting it to a default value
            affine_mat = np.array([[  2.65335989,   0.00000000,   0.00000000,  -3.39729004],
                                     [  0.00000000,   2.65335989,   0.00000000,  -4.90472992],
                                     [  0.00000000,   0.00000000,   2.50000000,  -1.00950000],
                                     [  0.00000000,   0.00000000,   0.00000000,   1.00000000]])            
        data = []
        training_data = []
        training_labels = []
        training_data_multilabel = []
        training_labels_multilabel = []
        for j in range(len(self.hotspots[i])):
                value = 0
                value_ct = 0
                try:
                    z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[i][j]
                    flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
                    # apply affine transform from index coordinates to mm coordinates
                    _x, _y, _z, w = np.dot(affine_mat, [x, y, z, 1])
                    try:
                        if self.with_resampling:                                
                            value = np.asarray(self.pets_after_resamp[i][int(z)])[int(y), int(x)]
                            print "INSIDE extract_data 2 exporting data for lesion No. %d" % j
                        else:
                            value = np.asarray(self.transformed_pet[i][int(z)])[int(y), int(x)]
                            print "INSIDE extract_data 3 exporting data for lesion No. %d" % j
                        
                        if self.ct_compression:
                            value_ct = np.asarray(self.compressed_cts[i][int(z)+ self.zOffset])[int(y), int(x)]
                        else:
                            value_ct = np.asarray(self.cts_after_resamp[i][int(z)+ self.zOffset])[int(y), int(x)]
                        
                    except Exception as e:
                        print "Something went wrong while extracting dianosis data"
                        print e.message
                        value = 0
                        print "INSIDE extract_data 4 exporting data for lesion No. %d" % j
                                
                    data.append([_x, _y, _z, value, value_ct, int(flagLiver), int(flagLung), int(flagKidney), int(flagBone), int(flagBladder), int(flagProstate), int(flagLymph), int(flagSalG), int(flagSpleen), int(flagOther), int(flagManual), int(flagSizeChanged), diameter, int(flag), str(name)])
                    training_data.append([_x, _y, _z, value, value_ct, int(flagLiver), int(flagLung), int(flagKidney), int(flagBone), int(flagBladder), int(flagProstate), int(flagOther), int(flagManual), int(flagSizeChanged), diameter])                    
                    training_labels.append(int(flag))
                    training_data_multilabel.append([value, value_ct, int(flagManual), int(flagSizeChanged), diameter])
                    training_labels_multilabel.append([int(flagLiver), int(flagLung), int(flagKidney), int(flagBone), int(flagBladder), int(flagProstate), int(flagOther), int(flag)])
                except Exception as e:
                    print "Something went wrong while exporting csv XXX"
                    print e.message
                    print "INSIDE extract_data 5 exporting data for lesion No. %d" % j
        # accumulate training data
        self.training_data = np.array(training_data)
        self.training_labels = np.array(training_labels)
        self.training_data_multilabel = np.array(training_data_multilabel)
        self.training_labels_multilabel = np.array(training_labels_multilabel)
        self.affine_mat = affine_mat
        return data
    
    def train_dataset(self):
        self.labelPrediction.setVisible(False)
        self.extract_data()
        try:
            # single label svm
            clf = svc()
            clf.fit(self.training_data, self.training_labels)
            self.clf = clf
            # multilabel binary relevance        
            ###
#            print self.training_data_multilabel.shape, self.training_data_multilabel
#            print self.training_labels_multilabel.shape, self.training_labels_multilabel
#            from sklearn.cross_validation import train_test_split
#            X_train, X_test, y_train, y_test = train_test_split(self.training_data_multilabel, self.training_labels_multilabel, test_size=0.33)
            ###
            from sklearn.neighbors import KNeighborsClassifier
            
            clf_br =  KNeighborsClassifier() #BR(GB()) # MLkNN(k=3)
            clf_br.fit(self.training_data_multilabel, self.training_labels_multilabel)
#            clf_br.fit(X_train, y_train)
#            print clf_br.predict(X_test)
            self.clf_br = clf_br
            print "INSIDE train_dataset"
            self.buttonPredictDiagnosisLabel.setEnabled(True)        
        except Exception as e:
            print "Something went wron while training"
            print e.message
    
    def predict_diagnosis_label(self):        
        print "INSIDE predict_diagnosis_label 1"        
#        clf = svc()
#        clf.fit(self.training_data, self.training_labels)
        # now we predict the label for the current lesion
        value = value_ct = 0
        i = self.patientIndex
        j = self.currentLesionIndex
        test_data = []
        try:
            z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = self.hotspots[self.patientIndex][self.currentLesionIndex]
            flagLiver, flagLung, flagKidney, flagBone, flagBladder, flagProstate, flagLymph, flagSalG, flagSpleen, flagOther = self.map_categories_to_labels(category)
            _x, _y, _z, w = np.dot(self.affine_mat, [x, y, z, 1])
            try:
                if self.with_resampling:                                
                    value = np.asarray(self.pets_after_resamp[i][int(z)])[int(y), int(x)]
                    print "INSIDE predict_diagnosis_label 2 exporting data for lesion No. %d" % j
                else:
                    value = np.asarray(self.transformed_pet[i][int(z)])[int(y), int(x)]
                    print "INSIDE predict_diagnosis_label 3 exporting data for lesion No. %d" % j
                
                if self.ct_compression:
                    value_ct = np.asarray(self.compressed_cts[i][int(z)+ self.zOffset])[int(y), int(x)]
                else:
                    value_ct = np.asarray(self.cts_after_resamp[i][int(z)+ self.zOffset])[int(y), int(x)]
                
            except Exception as e:
                print "Something went wrong while exporting csv"
                print e.message
                value = 0
                print "INSIDE predict_diagnosis_label 4 exporting data for lesion No. %d" % j
        except Exception as e:
                    print "Something went wrong while extracting data for lesion number %d" % self.currentLesionIndex 
                    print e.message           
        test_data = np.array([[_x, _y, _z, value, value_ct, int(flagLiver), int(flagLung), int(flagKidney), int(flagBone), int(flagBladder), int(flagProstate), int(flagOther), int(flagManual), int(flagSizeChanged), diameter]])
        test_data_multi = np.array([[value, value_ct, int(flagManual), int(flagSizeChanged), diameter]])
        print "the predicted lables:"
        try:
            print "svm:", self.clf.predict(test_data)
            prediction = self.clf_br.predict(test_data_multi)
            l, g, k, b, r, p, o, f  = prediction[0]
            print "Predicted labels:", l, g, k, b, r, p, o, f
            predicted_label = "Other tissue"
            if l:
                predicted_label = "Liver"
            elif g:
                predicted_label = "Lung"
            elif k:
                predicted_label = "Kidney"
            elif b:
                predicted_label = "Bone"
            elif r:
                predicted_label = "Bladder"
            elif p:
                predicted_label = "Prostate"
                
            print "BR:", prediction.shape
            print "BR:", prediction[0].shape
            print "BR:", prediction
            from sklearn.metrics import accuracy_score
            acc = accuracy_score(np.array([[int(flagLiver), int(flagLung), int(flagKidney), int(flagBone), int(flagBladder), int(flagProstate), int(flagOther), int(flag)]]), prediction)
            print "Accuracy:", acc
            
            self.labelPrediction.setText("Predicted label for this lesion: %s \r\nPathological: %r\r\nPrediction accuracy: %f" % (predicted_label, bool(self.clf.predict(test_data)), acc))
            self.labelPrediction.setVisible(True)
        except Exception as e:
            print "Something went wrong while label prediction"
            print e.message
        
    
    def drawSeedCircles(self, seedPoint):   
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = seedPoint
        print "INSILE DRAW CIRCLE"
        print z, x, y, diameter
        if (not flagManual) :
            try:            
                pixel_spacing = dhu.get_pixel_spacing(0, PathsDicom[self.patientIndex])
            except Exception as e:
                print "Something went wrong while importing pixe_spacing for patient No. %d" % self.patientIndex
                print e.message
                # setting it to a default value            
                pixel_spacing = (2.65, 2.65)
            diameter *= pixel_spacing[0]
        circle = plt.Circle((x, y), .5 * diameter, fc='none', ec='r')        
        self.ax.add_patch(circle)
        circle.figure.canvas.draw()
            
    def drawSeedCirclesCT(self, seedPoint):
        z, x, y, category, flagManual, flagSizeChanged, diameter, flag, name = seedPoint
        if (not flagManual) :
            try:            
                pixel_spacing = dhu.get_pixel_spacing(0, PathsDicom[self.patientIndex])
            except Exception as e:
                print "Something went wrong while importing pixe_spacing for patient No. %d" % self.patientIndex
                print e.message
                # setting it to a default value            
                pixel_spacing = (2.65, 2.65)
            diameter *= pixel_spacing[0]
        circle = plt.Circle((x, y), .5 * diameter, fc='none', ec='r')        
        self.axCT.add_patch(circle)
        circle.figure.canvas.draw()
        
        
    # mouse annotations
    def initMouseAnnotate(self):
        self.circle = plt.Circle((0, 0), 0., fc='none', ec='r')
        self.ax.add_patch(self.circle)        
        self.ax.figure.canvas.mpl_connect('button_press_event', self.on_press)
        self.ax.figure.canvas.mpl_connect('button_release_event', self.on_release)
        self.ax.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)    
        self.is_pressed = 0         
    
#    def rectMouseAnnotate(self):        
#        self.circle = plt.Circle((0, 0), 5.5, fc='none', ec='r')
#        self.ax.add_patch(self.circle)        
#        self.ax.figure.canvas.mpl_connect('button_press_event', self.on_press)
#        self.ax.figure.canvas.mpl_connect('button_release_event', self.on_release)
#        self.ax.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)    
#        self.is_pressed = 0  

    def on_motion(self, event):
        if self.is_pressed:
            self.x1 = event.xdata
            self.y1 = event.ydata             
            self.circle.center = (self.x0 + 0.5 * (self.x1 - self.x0), self.y0 + 0.5 * (self.y1 - self.y0))
            self.circle.set_radius(math.sqrt(((self.x1 - self.x0) ** 2 + (self.y1 - self.y0) ** 2 )))
            self.circle.figure.canvas.draw()

    def on_press(self, event):
        self.is_pressed = 1
        self.x0 = event.xdata
        self.y0 = event.ydata

    def on_release(self, event):
        self.is_pressed = 0       
        x, y = self.circle.center
        _z = self.z
        self.hotspots[self.patientIndex].append((int(self.z), int(x), int(y), 0, True, False, float(2. * self.circle.radius), True, "no_name"))
        self.adjustLesionName()
        _lesion_index = self.currentLesionIndex                        
        self.make_slice_lesion_mapping()
        if self.up_wheel:
            self.currentLesionIndex = _lesion_index + 1
        else:
            self.currentLesionIndex = _lesion_index 
        self.z = _z
        self.zCT = self.z + self.zOffset
        self.update_diagnosis_label(True)
        
        self.plot()
        self.plotCT()
        self.adjustDiagnosis()
        self.adjustDiagnosisCT()
#        self.initMouseAnnotate()


        
    # mouse annotations for CT
    def initMouseAnnotateCT(self):
        self.circleCT = plt.Circle((0, 0), 5.5, fc='none', ec='r')
        self.axCT.add_patch(self.circleCT)        
        self.axCT.figure.canvas.mpl_connect('button_press_event', self.on_pressCT)
        self.axCT.figure.canvas.mpl_connect('button_release_event', self.on_releaseCT)
        self.axCT.figure.canvas.mpl_connect('motion_notify_event', self.on_motionCT)    
        self.is_pressedCT = 0 
        
    def rectMouseAnnotateCT(self):        
        self.circleCT = plt.Circle((0, 0), 5.5, fc='none', ec='r')
        self.axCT.add_patch(self.circleCT)        
        self.axCT.figure.canvas.mpl_connect('button_press_event', self.on_pressCT)
        self.axCT.figure.canvas.mpl_connect('button_release_event', self.on_releaseCT)
        self.axCT.figure.canvas.mpl_connect('motion_notify_event', self.on_motionCT)    
        self.is_pressedCT = 0  

    def on_motionCT(self, event):
        if self.is_pressedCT:
            self.x1CT = event.xdata
            self.y1CT = event.ydata             
            self.circleCT.center = (self.x0CT + 0.5 * (self.x1CT - self.x0CT), self.y0CT + 0.5 * (self.y1CT - self.y0CT))
            self.circleCT.set_radius(math.sqrt(((self.x1CT - self.x0CT) ** 2 + (self.y1CT - self.y0CT) ** 2 )))
            self.circleCT.figure.canvas.draw()
        
    def on_pressCT(self, event):
        self.is_pressedCT = 1
        self.x0CT = event.xdata
        self.y0CT = event.ydata

    def on_releaseCT(self, event):
        self.is_pressedCT = 0      
        x = self.x0CT + 0.5 * self.rectCT.get_width()
        y = self.y0CT + 0.5 * self.rectCT.get_height()  
        print "inside on_releaseCT"
        print self.patientIndex
        print len(self.allSeedPointsCT[self.patientIndex][self.zCT])
        self.allSeedPointsCT[self.patientIndex][self.zCT] += ([(x, y, self.rectCT.get_width(), self.rectCT.get_height())])
        print x, y, self.rectCT.get_width(), self.rectCT.get_height()
        print len(self.allSeedPointsCT[self.patientIndex][self.zCT])
#        b, strDiagnosis = self.findingsCT[self.patientIndex][self.zCT]
#        self.findingsCT[self.patientIndex][self.zCT] = True, strDiagnosis        
        self.initMouseAnnotateCT()      
				
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()    
    main.setWindowFlags(main.windowFlags() |
                          QtCore.Qt.WindowSystemMenuHint |
                          QtCore.Qt.WindowMinMaxButtonsHint)
    main.show()
    sys.exit(app.exec_())    
